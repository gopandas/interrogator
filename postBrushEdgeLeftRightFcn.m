function postBrushEdgeLeftRightFcn(~, event_data, rwObj)

    nlines = length(event_data.Axes.Children);
    brushdata = cell(nlines, 1);
    for ii = 1:nlines
        brushdata{ii} = event_data.Axes.Children(ii).BrushHandles.Children(1).VertexData;
%         rwObj.BrushedData =  brushdata(ii);
        fprintf('Line %i\n', ii)
        fprintf('X: %f Y: %f Z: %f\n', brushdata{ii})
    end

% event_data.Axes.Position

% findall(gca, 'Children')     
% event_data.Axes.CurrentPoint
%     
%     event_data.Axes.Children.YData;
%     rwObj.ResultsNewRefLevel.eNonLin
%     
    
end
