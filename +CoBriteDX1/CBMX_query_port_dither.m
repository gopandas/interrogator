function dither=CBMX_query_port_dither(ser_obj,Port)
% D. Stahl 22.11.11
% queries dither state of ports
% Port address consisting of a 3 element vector, 0 is wildcard
% if wildcard is used, first 3 columns of response are Port address
% response 1: dither on, 0: dither off, -1: not supported
% requires FW version 2.0.0. or higher

dither=CoBriteDX1.CBMX_query_prototype(ser_obj,Port,'dit','%d');

