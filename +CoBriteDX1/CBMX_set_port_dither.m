function [ok response]=CBMX_set_port_dither(ser_obj,Port,dither)
% D. Stahl 21.11.11
% sets Laserport dither
% response 1: dither on, 0: dither off, -1: not supported
% requires FW version 2.0.0. or higher
% Port address consisting of a 3 element vector, 0 is wildcard
[ok response]=CoBriteDX1.CBMX_set_prototype(ser_obj,Port,'dit',dither,'%d');
