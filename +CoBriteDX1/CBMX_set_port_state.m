function [ok response]=CBMX_set_port_state(ser_obj,Port,state)
% D. Stahl 21.11.11
% sets Laserport output on/off state; 1: on 0: off
% Port address consisting of a 3 element vector, 0 is wildcard
[ok response]=CoBriteDX1.CBMX_set_prototype(ser_obj,Port,'stat',state,'%d');
