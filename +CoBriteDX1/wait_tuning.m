function wait_tuning(ser_obj,Port,max_time)
% D. Stahl 12.11.11
% waits until laser is settled or max_time [s] is elapsed
% Port address consisting of a 3 element vector, 0 is wildcard
timer_start=now;
fprintf('checking tuning state ... ');
while ((now-timer_start)*24*3600)<=max_time
    if all(~CoBriteDX1.CBMX_query_tuning_state(ser_obj,Port))
        break;
    end
    pause(0.25);
   fprintf('.');
end
assert (((now-timer_start)*24*3600)<=max_time, 'Max waiting time exceeded!');
