function FTF=CBMX_query_port_FTF(ser_obj,Port)
% D. Stahl 11.11.11
% queries FTF value of ports
% Port address consisting of a 3 element vector, 0 is wildcard
% if wildcard is used, first 3 columns of response are Port address

FTF=CoBriteDX1.CBMX_query_prototype(ser_obj,Port,'off','%f');
