function [response echo_out]=CBMX_query_prototype(ser_obj,Port,command,res_format)
% D. Stahl 11.11.11
% Prototype query that can be used to execute arbitrary queries
% Port address consisting of a 3 element vector C-S-P, 0 is wildcard
% if wildcard is used, response first 3 columns are used Port address, 4th column is
% actual value
% command [String] command to be issued, i.e. 'frequ'
% res_format [String] defines format of response, i.e.
% %f,%f,%f,%f,%d,%d,%d

verbose=0; %outputs result to command line
timeout=30; %timeout for response query in seconds
response=[]; %default response
if ser_obj.BytesAvailable
    fread(ser_obj,ser_obj.BytesAvailable); %clear receive buffer
end
sendline=sprintf([command,'? %d,%d,%d;'],Port);
fprintf(ser_obj,sendline);
tic;
while ser_obj.BytesAvailable==0 && toc<timeout
    pause(0.1);
end
pause(0.05);
if any(Port==0) % change format if wildcard is used
    res_format=['%d,%d,%d,' res_format];
end
if ser_obj.BytesAvailable
    data=char(fread(ser_obj,ser_obj.BytesAvailable))';
    raw_res=regexp(data,'\n','split'); %read buffer and break it into lines
    idx=1;
    for i=1:length(raw_res)
        if ~isempty(strfind(raw_res{i},'ERR'))
            error(raw_res{i});
            response=[];
        else
            if ~isempty(strfind(raw_res{i},sendline(1:end-1))) % echo_out on?
                echo_out=raw_res{i};
            else              
                vals = sscanf(raw_res{i},res_format);
                if ~isempty(vals)
                    response(idx,:)=vals;
                end
                idx=idx+1;                
            end
            if verbose, fprintf(strcat(command,' : ',res_format),response); end            
        end
    end
if ~exist('echo_out'), echo_out=[]; end    
echo_out;
else
    error('no data received');
end

