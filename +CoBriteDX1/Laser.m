classdef Laser <interfaces.ILaser
    %LASER Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Access=private)
        com_port
        ser_obj=false
    end
    
    properties (Transient, SetAccess = protected)
        timer_handle = false
    end
    
    properties (Constant)
        max_time=45; %max time to wait after tuning [s]
        ptime=5; % pause time used after setting a value for power
        
        %Notes on USB com ports: if there are communication errors, change
        %the com port number to a random number to avoid system conflicts,
        %connect the laser directly to the mother board, don't connect
        %through a USB hub or front panel. Windows com port settings:
        %Baud rate: 112500,
        %data bits read and write to minimum: 64 bits,
        %latency minimum: 1 ms
        Port=[1 1 1]; % do not use wildcards here
        wvError=0.01;
        fError=0.001;
        pwrError=0.5e-3;
    end
    
    methods
        function obj = Laser(port)
            %laser outputs [1527.60488 1567.13256] when queried
            obj.WavelengthMin = 1528e-9;
            obj.WavelengthMax = 1565e-9;
            obj.WavelengthStep = 1e-12;
            %in dBm, the laser gives [6 16] dBm when queried
            %FTF limits setting actual: -12.000GHz to 12.000GHz
            obj.FTFlim = 12; % in GHz
            obj.Powers = 1e-3.*(ceil(10^(6/10)):1e-1:round(10*10^(16/10))*0.1); %in W
            obj.com_port = port;
        end
    end
    
    methods (Access=protected)
        %these can be called only by this instance and its children(classes
        %that inherit from Laser object which inherits from ILaser)
        function Initialize(obj) %public methods cause other people will call these
            tic;
            fprintf('Initializing CoBriteDX1...\n');
%             delete(instrfind); %first clean up before doing anything
%             %             clear port;
            bdRate = 115200;
            obj.ser_obj=serial(obj.com_port,'BaudRate', bdRate);
            fopen(obj.ser_obj);
            fprintf(obj.ser_obj,';');
            % flush of Laser chassis buffer by sending command termination signal
            pause(0.1);
            while obj.ser_obj.BytesAvailable % flush Rx buffer
                temp=fscanf(obj.ser_obj,'%c',obj.ser_obj.BytesAvailable); %#ok<NASGU>
            end
            disp('Turning the Laser ON');
            CoBriteDX1.CBMX_set_port_state(obj.ser_obj,obj.Port,1);
            CoBriteDX1.wait_tuning(obj.ser_obj,obj.Port,obj.max_time);
            CoBriteDX1.CBMX_set_port_dither(obj.ser_obj,obj.Port,1);
            pause(0.5);
            assert (logical(CoBriteDX1.CBMX_query_port_dither(obj.ser_obj,obj.Port)),...
                'Error: Dither on');
            toc;
        end
        
        function ShutDown(obj)
            %switch the port off
            tic;
            disp('Turning the Laser OFF');
            CoBriteDX1.CBMX_set_port_state(obj.ser_obj,obj.Port,0);
            CoBriteDX1.wait_tuning(obj.ser_obj,obj.Port,obj.max_time);
            fclose(obj.ser_obj); %close the laser port
            clear port;
            toc;
        end
        
        function wvRead = SetWavelength(obj, wavelength)
            fprintf('\n');
            fprintf('Setting Wavelength ...\n');
            wavelengthNM = wavelength * 1e9;
            f = 1e-9*algorithms.LambdaToF(wavelength); %f [GHz]
            fprintf('Target: lambda = %4.3f nm and f = %2.3f GHz\n', 1e9*wavelength, f);

            F = CoBriteDX1.CBMX_query_port_FTF(obj.ser_obj,obj.Port);
            while length(F)~=1;  % while loops help fix the communication error with the laser, should be put into the query script
                F = CoBriteDX1.CBMX_query_port_FTF(obj.ser_obj,obj.Port); pause(0.1); fprintf ('Repeating query (FTF)\n');
            end
            f0 = 1e3*CoBriteDX1.CBMX_query_port_frequ(obj.ser_obj,obj.Port);
            while length(f0)~=1; 
                f0 = 1e3*CoBriteDX1.CBMX_query_port_frequ(obj.ser_obj,obj.Port); pause(0.1); fprintf ('Repeating query (FTF)\n');
            end
            deltaf = f-f0;
            fprintf('PreTuning: FTF = %6.3f GHz f = %6.3f GHz\n', F, f0);
            fprintf('delta f = %6.3f GHz\n', deltaf);
            if (abs(deltaf)<obj.FTFlim)
                fprintf('Fine tuning ...\n');
                CoBriteDX1.CBMX_set_port_FTF(obj.ser_obj,obj.Port, deltaf);
            else
                fprintf('Rough Tuning ...\n');
                f0new = f - 12; % f0new is in GHz
                f0newThz = f0new*1e-3;
                portConf = CoBriteDX1.CBMX_query_port_config(obj.ser_obj,obj.Port);
                % making sure that portConf is long enough in the while loop    
                while length(portConf)~=6
                        portConf = CoBriteDX1.CBMX_query_port_config(obj.ser_obj,obj.Port);
                        fprintf('ReQuerying Laser... \n');
                        pause(0.1);
                end
                CoBriteDX1.CBMX_set_port_config(obj.ser_obj,obj.Port,...
                    f0newThz, 12, portConf(3), portConf(4), portConf(6));
%                 CoBriteDX1.CBMX_set_port_frequ(obj.ser_obj,obj.Port, f0newThz);
%                 CoBriteDX1.wait_tuning(obj.ser_obj,obj.Port,obj.max_time);
%                 CoBriteDX1.CBMX_set_port_FTF(obj.ser_obj,obj.Port, 12);
            end
            CoBriteDX1.wait_tuning(obj.ser_obj,obj.Port,obj.max_time);
            F = CoBriteDX1.CBMX_query_port_FTF(obj.ser_obj,obj.Port);
            while length(F)~=1; 
                F = CoBriteDX1.CBMX_query_port_FTF(obj.ser_obj,obj.Port); pause(0.1); fprintf ('Repeating query (FTF)\n');
            end
            f0_post = 1e3*CoBriteDX1.CBMX_query_port_frequ(obj.ser_obj,obj.Port);
            while length(f0_post)~=1; 
                f0_post = 1e3*CoBriteDX1.CBMX_query_port_frequ(obj.ser_obj,obj.Port); pause(0.1);  fprintf ('Repeating query (frequ)\n');
            end
            wvTarget = 1e9*algorithms.FToLambda(1e9*f0+1e9*deltaf);
            wvRead = 1e9*algorithms.FToLambda(1e9*f0_post+1e9*F);
            %             wvRead=CoBriteDX1.CBMX_query_port_wav(obj.ser_obj,obj.Port);
            fprintf('PostTuning: FTF = %6.3f GHz f = %6.3f GHz\n', F, f0_post);
            fprintf('\nTarget lambda = %4.3f nm and actual lambda = %4.3f nm \n',...
                wvTarget, wvRead);
            fprintf('Wavelength error: %3.3f pm', 1e3*abs(wvTarget-wvRead));
            while abs(wvTarget-wvRead)>obj.wvError
                fprintf('Port %d-%d-%d: Error! Wavelength error exceeded',obj.Port);
                obj.SetWavelength(wavelength);
            f0_post = 1e3*CoBriteDX1.CBMX_query_port_frequ(obj.ser_obj,obj.Port);
            while length(f0_post)~=1; 
                f0_post = 1e3*CoBriteDX1.CBMX_query_port_frequ(obj.ser_obj,obj.Port); pause(0.1);  fprintf ('Repeating query (frequ)\n');
            end
            wvRead = 1e9*algorithms.FToLambda(1e9*f0_post+1e9*F);
            end
        end
        
        function wavelength = GetWavelength(obj)
            f0 = 1e3*CoBriteDX1.CBMX_query_port_frequ(obj.ser_obj,obj.Port); % in THz
            while length(f0)~=1; 
                f0 = 1e3*CoBriteDX1.CBMX_query_port_frequ(obj.ser_obj,obj.Port); pause(0.1); fprintf ('Repeating query (FTF)\n');
            end
            df0 = CoBriteDX1.CBMX_query_port_FTF(obj.ser_obj,obj.Port); % in GHz
            while length(df0)~=1; 
                df0 = CoBriteDX1.CBMX_query_port_FTF(obj.ser_obj,obj.Port); pause(0.1); fprintf ('Repeating query (FTF)\n');
            end
            f = f0+df0;
            wavelength = algorithms.FToLambda(1e9*f);
            fprintf('Current: FTF = %2.3f GHz f = %2.3f GHz\n', df0, f0);
            fprintf('Current wavelength is %4.3f nm\n', 1e9*wavelength);
        end
        
        function SetPower(obj, power)
            fprintf('Setting Power ...\n');
            power_mW = 1e3*power;
            powerDBm = 10*log10(power_mW);
            lim=CoBriteDX1.CBMX_query_port_power_limit(obj.ser_obj,obj.Port);
            assert(powerDBm>lim(1)||powerDBm<lim(2), 'Power out of bounds');
            CoBriteDX1.CBMX_set_port_power(obj.ser_obj,obj.Port, powerDBm);
            CoBriteDX1.wait_tuning(obj.ser_obj,obj.Port,obj.max_time);
            pause(obj.ptime);
            pwrRead=CoBriteDX1.CBMX_query_port_power(obj.ser_obj,obj.Port);
            fprintf('Power setting error = %2.6f \n', abs(10^(powerDBm/10)-10^(pwrRead/10)));
            assert(abs(10^(powerDBm/10)-10^(pwrRead/10))<1e3*obj.pwrError||pwrRead==-100, ...
                'Port %d-%d-%d: Error! Power setting/reading not working',obj.Port);
            fprintf('Power set to %2.3f mW \n', 10^(pwrRead/10));
        end
        
        function power = GetPower(obj)
            powerdBm = CoBriteDX1.CBMX_query_port_power(obj.ser_obj,obj.Port);
            power = 1e-3*10^(powerdBm/10);
            fprintf('Current power is %4.3f mW\n', 1e3*power);
        end

        function ok = SetLowNoise(obj, sw)
            %turn dither off
            if sw==true
                disp('Turning Low Noise Mode on...');
                x = inputdlg('Timer setup: set Low-noise mode timeout [mins]:', 'Timeout', 1, {'5'});
                if ~isempty(x); 
                    obj.timer_handle = GUI.TimerGUI(str2double(x{:}));
                end
            else
                disp('Turning Low Noise Mode off...');
            end
            curr=CoBriteDX1.CBMX_query_port_dither(obj.ser_obj,obj.Port);
            if curr==-1
                disp('Dither is not supported for this laser!\n');
            else
                if sw; ditTarget=0; else ditTarget=1; end; %1 for OFF, 0 for ON
                CoBriteDX1.CBMX_set_port_dither(obj.ser_obj,obj.Port,ditTarget);
                pause(0.5);
                %                 CoBriteDX1.wait_tuning(obj.ser_obj,obj.Port,obj.max_time);
                %                 pause(obj.ptime);
                ditRead=CoBriteDX1.CBMX_query_port_dither(obj.ser_obj,obj.Port);
                assert(ditTarget==ditRead,...
                    'Port %d-%d-%d: Warning! Dither status setting/reading not working',obj.Port );
                ok = true;
                if sw==true
                    disp('ON');
                else
                    disp('OFF');
                    if isvalid(obj.timer_handle)
                        delete(obj.timer_handle);
                    end
                end
            end
        end
    end
    
end
