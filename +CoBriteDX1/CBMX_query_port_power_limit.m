function power_limit=CBMX_query_port_power_limit(ser_obj,Port)
% D. Stahl 21.11.11
% queries Laserport power limits
% Port address consisting of a 3 element vector C-S-P, 0 is wildcard
% if wildcard is used, first 3 columns of response are used Port address, 4th column is
% actual value

power_limit=CoBriteDX1.CBMX_query_prototype(ser_obj,Port,'pow:lim','%f,%f');
