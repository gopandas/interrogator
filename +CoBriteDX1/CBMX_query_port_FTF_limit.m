function FTF_limit=CBMX_query_port_FTF_limit(ser_obj,Port)
% D. Stahl 21.11.11
% queries Laserport FTF offset limits
% Port address consisting of a 3 element vector C-S-P, 0 is wildcard
% if wildcard is used, first 3 columns of response are used Port address, 4th column is
% actual value
% response is a two element vector

temp=CoBriteDX1.CBMX_query_prototype(ser_obj,Port,'off:lim','%f'); %as value is symetrical, only 1 value is returned
FTF_limit=[-temp temp];
