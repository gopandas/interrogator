function frequency_limit=CBMX_query_port_frequ_limit(ser_obj,Port)
% D. Stahl 21.11.11
% queries Laserport frequency limits
% Port address consisting of a 3 element vector C-S-P, 0 is wildcard
% if wildcard is used, first 3 columns of response are used Port address, 4th column is
% actual value
% response is a two element vector

frequency_limit=CoBriteDX1.CBMX_query_prototype(ser_obj,Port,'freq:lim','%f,%f');
