function [ok response]=CBMX_set_port_power(ser_obj,Port,Power)
% D. Stahl 12.11.11
% sets Laserport power in dBm
% Port address consisting of a 3 element vector, 0 is wildcard
[ok response]=CoBriteDX1.CBMX_set_prototype(ser_obj,Port,'pow',Power,'%2.2f');
