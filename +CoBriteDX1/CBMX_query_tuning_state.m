function state=CBMX_query_tuning_state(ser_obj,Port)
% D. Stahl 11.11.11
% queries tuning state of ports
% Port address consisting of a 3 element vector, 0 is wildcard
% if wildcard is used, first 3 columns of response are Port address
% response 1: tuning occuring, 0: settled

state=CoBriteDX1.CBMX_query_prototype(ser_obj,Port,'busy','%d');
