function [ok response]=CBMX_set_prototype(ser_obj,Port,command,parameter,param_format)
% D. Stahl 21.11.11
% Prototype set that can be used to execute arbitrary set commands
% Port address consisting of a 3 element vector C-S-P, 0 is wildcard
% command [String] command to be issued, i.e. 'frequ'
% error [String] in case error occured
% ok will be 1 if command was executed

verbose=1; %output result to command line
timeout=2; %timeout for confirmation response
if ser_obj.BytesAvailable
    fread(ser_obj,ser_obj.BytesAvailable); %clear receive buffer
end
if isempty(Port)
    sendline=sprintf([command,' ',param_format,';'],parameter);
    fprintf(ser_obj,sendline);
else
     sendline=sprintf([command,' %d,%d,%d,',param_format,';'],Port,parameter);
    fprintf(ser_obj,sendline);
end
tic;
while ser_obj.BytesAvailable==0 || toc<timeout
    pause(0.1);
end
pause(0.05);
if ser_obj.BytesAvailable
    response=sprintf('%c',fread(ser_obj,ser_obj.BytesAvailable));
    if ~isempty(strfind(response,'ERR'))
        error(response);
        response=[];
        ok=0;
    else
        responseSemicolon = strfind(response,';');
        if length(responseSemicolon)~=1; 
            fprintf('Response has more semicolons, fixing... ')
            responseSemicolon = responseSemicolon(end); 
            fprintf('fixed\n')
        end 
        
        if responseSemicolon || strfind(response,sendline); ok=1; end
        if verbose; disp(response); end
    end
else
    response=[];
    error(response);
    ok=0;
end