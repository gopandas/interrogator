function [ok response]=CBMX_set_port_frequ(ser_obj,Port,frequency)
% D. Stahl 27.10.11
% sets Laserport frequency
% Port address consisting of a 3 element vector, 0 is wildcard
[ok response]=CoBriteDX1.CBMX_set_prototype(ser_obj,Port,'freq',frequency,'%3.4f');
