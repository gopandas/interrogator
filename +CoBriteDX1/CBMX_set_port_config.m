function [ok response]=CBMX_set_port_config(ser_obj,Port,Frequency,FTF,Power,OnOff,dither)
% D. Stahl 27.10.11
% sets Laserport state
% Port address consisting of a 3 element vector, 0 is wildcard
[ok response]=CoBriteDX1.CBMX_set_prototype(ser_obj,Port,'conf',[Frequency FTF Power OnOff dither],'%3.4f,%2.3f,%2.2f,%d,%d');
