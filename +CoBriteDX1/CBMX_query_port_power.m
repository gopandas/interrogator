function Power=CBMX_query_port_power(ser_obj,Port)
% D. Stahl 27.10.11
% queries Laserport power
% Port address consisting of a 3 element vector, 0 is wildcard
% if wildcard is used, first 3 columns are used Port address, 4th column is
% actual value

Power=CoBriteDX1.CBMX_query_prototype(ser_obj,Port,'pow','%f');

