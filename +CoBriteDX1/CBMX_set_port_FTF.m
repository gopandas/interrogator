function [ok response]=CBMX_set_port_FTF(ser_obj,Port,FTF)
% D. Stahl 27.10.11
% sets Laserport FTF offset
% Port address consisting of a 3 element vector, 0 is wildcard
[ok response]=CoBriteDX1.CBMX_set_prototype(ser_obj,Port,'off',FTF,'%2.3f');
