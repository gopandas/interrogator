function [ok response]=CBMX_set_port_wavelength(ser_obj,Port,wavelength)
% D. Stahl 21.11.11
% sets Laserport wavelength
% Port address consisting of a 3 element vector, 0 is wildcard
[ok response]=CoBriteDX1.CBMX_set_prototype(ser_obj,Port,'wav',wavelength,'%4.3f');
