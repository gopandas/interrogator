function frequ=CBMX_query_port_frequ(ser_obj,Port)
% D. Stahl 27.10.11
% queries Laserport frequ
% Port address consisting of a 3 element vector C-S-P, 0 is wildcard
% if wildcard is used, first 3 columns of response are used Port address, 4th column is
% actual value

frequ=CoBriteDX1.CBMX_query_prototype(ser_obj,Port,'freq','%f');

%% old code
% if ser_obj.BytesAvailable
%     fread(ser_obj,ser_obj.BytesAvailable); %clear receive buffer
% end
% fprintf(ser_obj,'freq? %d,%d,%d;',Port);
% pause(0.05);
% if ser_obj.BytesAvailable
%     i=1;
%     while ser_obj.BytesAvailable
%         response=fscanf(ser_obj,'%s',ser_obj.BytesAvailable);
%         if ~isempty(strfind(response,'ERR'))
%             error(response);
%             Power=[];
%         else
%             if any(Port==0)
%                 Power(i,:)=sscanf(response,'%d,%d,%d,%f');
%                 i=i+1;
%             else
%                 Power=sscanf(response,'%f');
%             end
%         end
%     end
% else
%     error('no data received');
% end
