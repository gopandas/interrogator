function mons=CBMX_query_monitor_values(ser_obj,Port)
% D. Stahl 11.11.11
% queries monitor values of ports
% Port address consisting of a 3 element vector, 0 is wildcard
% if wildcard is used, first 3 columns of response are Port address
% response columns: LaserDiode chip temp/LaserDiode base temp/LaserDiode
% diode current/TEC current

mons=CoBriteDX1.CBMX_query_prototype(ser_obj,Port,'mon','%f,%f,%f,%f');
