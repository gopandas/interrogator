classdef LaserFactory<interfaces.ILaserFactory
    %   LaserFactory Creates laser objects for CoBriteDX1 laser
    %   Detailed explanation goes here
    
    properties (Constant)
        Name = 'CoBriteDX1';
    end
    
    methods 
        function laser = Create(~)
            ports = CoBriteDX1.getAvailableComPort();
            [idx, ok] = listdlg('PromptString', 'Select COM Port',...
                'SelectionMode', 'single', 'ListSize', [100 120],...
                'ListString', ports);
            if ~ok
                idx = 1;
                laser = [];
                return;
            end
            laser = CoBriteDX1.Laser(ports(idx));
        end
    end
    
end
