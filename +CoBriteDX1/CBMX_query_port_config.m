function config=CBMX_query_port_config(ser_obj,Port)
% D. Stahl 28.10.11
% queries Laserport setting
% Port address consisting of a 3 element vector, wildcard is NOT allowed!
% if wildcard is used, first 3 columns are Port address
%columns are : Frequency,FTF,Power,On/Off;Busy state,Dither state
if any(Port==0)
    error('No Port wildcards allowed!');
else
    config=CoBriteDX1.CBMX_query_prototype(ser_obj,Port,'conf','%f,%f,%f,%f,%d,%d,%d');
end
