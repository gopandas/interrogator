classdef (Abstract) ISpectrum < handle
    %FINESPECTRUM A calibrated laser spectrum which interacts with data
    
    properties (SetObservable)
        LambdaMaxSensitivity
        % wavelength at the point of maximum spectrum slope
        VMaxSensitivity
        % Voltage at the wavelength with maximum spectrum slope at zero electric field 
        OperatingLambda
        % Lambda at which this spectrum operates, can be changed externally
        % for manual intervention
        OperatingV
        % Voltage at the operating wavelength point with no electric field
        % captured from the DAQ at the moment of ajusting laser wavelength
        Calibration
        % calibration object, contains E and V calibration factors
    end
    
    properties (SetAccess=protected, SetObservable)
        Spectrum
        % Contains data in the form of an array of (lambda, power)
        Results
        % Results object, contains all the data
    end
    
    properties (Dependent)
        LambdaMax
        LambdaMin
    end
    
    methods (Abstract)
        Calibrate(obj, appliedVoltagefile, appliedVoltage)
        % Calibrates this spectrum object with the passed voltage
        voltages = ProcessData(obj, powerLevels)
        % Performs a direct conversion using the calibration factor of the
        % passed data
    end
    
    methods
        function lambda = get.LambdaMax(obj)
            lambda = max(obj.Spectrum(1, :));
        end
        function lambda = get.LambdaMin(obj)
            lambda = min(obj.Spectrum(1, :));
        end
    end
    
end

