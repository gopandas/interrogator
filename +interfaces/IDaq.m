classdef (Abstract) IDaq < handle
    %IDAQ Base DAQ Interface
    
    properties (Transient, SetObservable, SetAccess = private)
        Voltage = 0
        % Current voltage on DAQ
        State = interfaces.DAQState.Off
    end
    
    methods
        function delete(obj)
            % Ensures that the daq is shut down during destruction
            try
                obj.RequestShutdown();
            catch ME
                warning('Error shutting down daq: %s', ME.message)
            end
        end
        
        function [ok, V] = RequestVoltage(obj)
            % Requests a single sample from the DAQ
            obj.EnsureOn();
            V = obj.GetVoltage();
            obj.Voltage = V;
            ok = true;
        end
        
        function [ok, V] = RequestVoltageMean(obj, t)
            % Requests an averaged sample from the daq over time t
            obj.EnsureOn();
            V = obj.GetVoltageMean;
            obj.Voltage = V;
            ok = true;
        end
        
        function ok = RequestShutdown(obj)
            if obj.State == interfaces.DAQState.On
                obj.ShutDown
                obj.State = interfaces.DAQState.Off;
            end
            
            ok = true;
        end
    end
    
    methods (Abstract, Access = protected)
        Initialize(obj)
        % Initializes the daq
        ShutDown(obj)
        % Shuts down the daq
        V = GetVoltage(obj)
        % Gets a single sample from the DAQ
        V = GetVoltageMean(obj, t)
        % Gets an averaged sample from the daq over time t
    end
    
    methods (Access = private)
        function EnsureOn(obj)
            % Ensures that the daq has been initialized
            if obj.State == interfaces.DAQState.Off
                obj.Initialize()
                obj.State = interfaces.DAQState.On;
            end
        end
    end
    
end

