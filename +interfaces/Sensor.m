classdef Sensor
    %SENSOR object
    
    properties (SetAccess = private)
        Name
        % Name of the sensor used in measurements
        EShift
        % E-field shift rate  for the specific sensor in pm/(MV/m) 
    end
            
    methods
        function obj = Sensor(name, eShift)
            obj.Name = name;
            obj.EShift = eShift;
        end     
    end
    
end

