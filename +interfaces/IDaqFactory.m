classdef IDaqFactory < handle
    %IDAQFACTORY Creates DAQ objects
    %   Detailed explanation goes here
    
    properties (Abstract, Constant)
        Name
        % Name of this daq type
    end
    
    methods (Abstract)
        daq = Create(obj)
        % Creates a new daq object
    end
    
end

