classdef (Abstract) ILaser < handle
    %ILASER Interface to a laser
    %   All lasers must implement these methods
    
    % Read-only properties
    properties (Transient, SetAccess = protected)
        WavelengthMin = false
        % Minimum wavelength that this laser can tune to, in meters
        WavelengthMax = false
        % Maximum wavelength that this laser can tune to, in meters
        WavelengthStep = false
        % Minimum step size for the laser
        FTFlim = false;
        % Fine tuning +- limits in Hz, value is on the order of GHz
        Grid
        % Grid spacing in GHz
        Powers  = []
        % Collection of all powers this laser may be set to
    end
    
    properties (Transient, SetObservable, SetAccess = private)
        Wavelength = 0
        % Current operating wavelength of laser
        Power = 0
        % Current operating power of laser
        State = interfaces.LaserState.Off
        % Current state of the laser
    end
    
    methods (Abstract, Access=protected)
        Initialize(obj)
        % Initializes the laser
        ShutDown(obj)
        % Shuts down the laser
        SetWavelength(obj, wavelength)
        % Sets the wavelength of the laser
        %   The implementing class should perform all actions and waiting
        %   necessary to set the laser to a certain wavelength. If
        %   wavelength is false, then 
        GetWavelength (obj)
        % Reads the wavelength of the laser
        SetPower(obj, power)
        % Sets the power of the laser
        GetPower(obj)
        % Sets the power of the laser
        %   The implementing class should perform all actions and waiting
        %   necessary to set the laser to a certain power
        SetLowNoise(obj, sw)
        % Turn low noise mode ON/OFF, sw =0 for OFF, sw=1 for ON
    end
    
    methods
        function delete(obj)
            % Ensures that the laser is shut down during destruction
            try
                obj.RequestShutdown();
            catch ME
                warning('Error shutting down laser: %s', ME.message)
            end
        end
                
        function [ok, wvRead] = RequestWavelength(obj, wavelength)
            % Requests that the laser turn on and set itself to the
            % requested wavelength in meters
            obj.EnsureOn();
            if obj.State == interfaces.LaserState.LN
                obj.SetLowNoise(false);
                obj.State = interfaces.LaserState.On;
            end
            if wavelength>=obj.WavelengthMin&& wavelength<=obj.WavelengthMax
                wvRead = obj.SetWavelength(wavelength);
            else
                warndlg('Wavelength value out of range!');
            end
            obj.UpdateWavelength;
            obj.UpdatePower;
            ok = true;
        end
        
        function ok = UpdateWavelength(obj)
            % Asks the laser its wavelength
            obj.EnsureOn();
            obj.Wavelength = obj.GetWavelength;
            ok = true;
        end

        
        function ok = RequestPower(obj, power)
            % Requests that the laser turn on and set itself to the
            % requested power level
            obj.EnsureOn();
            if obj.State == interfaces.LaserState.LN
                obj.SetLowNoise(false);
                obj.State = interfaces.LaserState.On;
            end
            if power<obj.Powers(1)
                power = obj.Powers(1);
            elseif power>obj.Powers(end)
                power = obj.Powers(end);
            end
            obj.SetPower(power);
            obj.UpdateWavelength;
            obj.UpdatePower;
            ok = true;
        end

        function ok = UpdatePower(obj)
            % Asks the laser its power
            obj.EnsureOn();
            obj.Power = obj.GetPower;
            ok = true;
        end

        function ok = RequestLNSwitch(obj, sw)
            % Requests that low noise mode be switched on (sw=0) or off (sw=1)
            if ~obj.SetLowNoise(sw); 
                ok = false; return; 
            end
            %update Laser State
            if (sw==true)&&(obj.State == interfaces.LaserState.On)
                obj.State = interfaces.LaserState.LN;
            elseif (sw==false)&&(obj.State == interfaces.LaserState.LN)
                    obj.State = interfaces.LaserState.On;
            else
                disp('Unknown Laser State');
            end
            ok = true;
        end
        
        function ok = RequestShutdown(obj)
            % Requests that the laser shut down, 
            % but first turn off low noise
            if obj.State == interfaces.LaserState.LN
                obj.SetLowNoise(false);
                obj.State = interfaces.LaserState.On;
            end
            if obj.State == interfaces.LaserState.On
                obj.ShutDown();
                obj.State = interfaces.LaserState.Off;
            end
            ok = true;
        end
    end
    
    methods (Access = private)
        function EnsureOn(obj)
            % Ensures that the laser has had its initialization method
            % called
            if obj.State == interfaces.LaserState.Off
                obj.Initialize();
                obj.State = interfaces.LaserState.On;
            end
        end
    end
    
end

