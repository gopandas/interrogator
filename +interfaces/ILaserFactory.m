classdef (Abstract) ILaserFactory < handle
    %ILASERFACTORY Creates laser objects
    %   Detailed explanation goes here
    
    properties (Abstract, Constant)
        Name
        % Name of this laser type
    end
    
    methods (Abstract)
        laser = Create(obj)
        % Creates a new laser object
    end
    
end

