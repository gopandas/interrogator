classdef (Abstract) IAlgorithm < handle
    %IALGORITHM Interface to main processing algorithm
    
    properties (SetAccess = private)
        Laser
        lambdaRes
        vres
        VMaxres
        
    end
    
    methods(Abstract)
        resonances = GetRoughSpectrum(obj)
        % Gets a rough spectrum from the laser/daq
        %   Returns interface.Resonance objects
        spectrum = GetFineSpectrum(obj, resonance)
        % Gets a fine spectrum from the laser/daq
        %   Returns an instance of interface.ISpectrum
    end
    
end

