classdef Resonance
    %RESONANCE Section of a rough spectrum
    
    properties (SetAccess = private)
        Spectrum;
        % Power levels for this spectrum in the form of an array of
        % (lambda, power)
    end
            
    properties
        WavelengthMin;
        WavelengthMax;
    end
    
    methods
        function obj = Resonance(spectrum)
            obj.Spectrum = spectrum;
            obj.WavelengthMin = spectrum(1, 1);
            obj.WavelengthMax = spectrum(1, end);
        end
        
        function lambda = get.WavelengthMin(obj)
            lambda = obj.WavelengthMin;
        end
            
        function obj = set.WavelengthMin(obj, lambda)
            obj.WavelengthMin = lambda;
        end
                
        function lambda = get.WavelengthMax(obj)
            lambda = obj.WavelengthMax;
        end
        
        function obj = set.WavelengthMax(obj, lambda)
            obj.WavelengthMax = lambda;
        end
                
    end
    
end

