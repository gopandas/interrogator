function postBrushFcn(~, event_data, rwObj)

    refLevel = mean(event_data.Axes.Children.YData);
    if rwObj.referenceLevelButton.Value
        if ~isempty(strfind (event_data.Axes.YLabel.String, 'GV')); multiplier=1e9; end
        if ~isempty(strfind (event_data.Axes.YLabel.String, 'MV')); multiplier=1e6; end
        if ~isempty(strfind (event_data.Axes.YLabel.String, 'kV')); multiplier=1e3; end
        if ~isempty(strfind (event_data.Axes.YLabel.String, 'mV')); multiplier=1e-3; end
        if ~isempty(strfind (event_data.Axes.YLabel.String, 'uV')); multiplier=1e-6; end
        if ~isempty(strfind (event_data.Axes.YLabel.String, 'nV')); multiplier=1e-9; end
        if strcmp(event_data.Axes.Title.String, rwObj.subplotTitles{1})
            disp(sprintf( 'vLin with multiplier %2.3f' , multiplier));
            rwObj.referenceLevelShift(1) = ...
                rwObj.referenceLevelShift(1) + multiplier*refLevel; end
        if strcmp(event_data.Axes.Title.String, rwObj.subplotTitles{2})
            disp(sprintf( 'vNonLin with multiplier %2.3f' , multiplier));
            rwObj.referenceLevelShift(2) = ...
                rwObj.referenceLevelShift(2) + multiplier*refLevel; end
        if strcmp(event_data.Axes.Title.String, rwObj.subplotTitles{3})
            disp(sprintf( 'eLin with multiplier %2.3f' , multiplier));
            rwObj.referenceLevelShift(3) = ...
                rwObj.referenceLevelShift(3) + multiplier*refLevel; end
        if strcmp(event_data.Axes.Title.String, rwObj.subplotTitles{4})
            disp(sprintf( 'eNonLin with multiplier %2.3f' , multiplier));
            rwObj.referenceLevelShift(4) = ...
                rwObj.referenceLevelShift(4) + multiplier*refLevel; end
    end
end
