Nonlinear_run_this calls

	DBmToMW
		Input: laser power in dBm
		Output: Laser power in mW
	LambdaToF 
		Input: wavelength
		Output:	frequency
	num2eng
		Input: number
		Output:string
	plotLabeld
		Input: time
		Input: Voltage
		Output: no output, just a graph

	plotScaled
		I: time
		I: Voltage
		Output: no output, just a graph
		(matlab named plot labeld)

	ReadLecroyBinaryWaveform

		I: Lecroy binary file
		needs to be in the same folder as the binary waveforms
	
	SlotTheGridForFineTuning

	AnalyzeSpectrum:

		Takes a spectrum (wavelengths and voltages) and returns the mid point (point of highest sensitivity)
		width of the resonance, and length of max sensitivity?
		
		I: wavelength 				(double)
		I: Voltage				(double)
		I: smoothing number			(double)
		O: MaxSensR = [lambdaMax indMax];	(double double)
			  			
					
		O: widthR = lambdaMinus10PercPlus - lambdaMinus10PercMinus; 
			
			Just a number
			
		O: MaxSensL = [lambdaMin, indMin];	(double double)

		
		Outputs a figure of a resonance edge, shows the most sensitive wavelength (graphically and numerically)

	findPeaktoPeak

		Finds the voltage peak to peak along with the offset value
	
		I: input voltage values (vector of doubles)
		I: input wavelength values (vector of doubles)
		I: smooth factor (int)
		O: average voltage peak to peak value (double)
		O: offset voltage (double)

	findcorrespondingX

		I: 
		O: Vector of numbers

	findResonance:

		I: wavelength spectrum (vector of doubles)
		I: voltage spectrum (vector of doubles)	
		O: Resonance, a structure of resonances.
			or
		Returns an error because no resonances were detected
		

	FToLambda:	