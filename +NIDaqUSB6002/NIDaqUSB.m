classdef NIDaqUSB <interfaces.IDaq
    %Daq Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Access=private)
        s=false
        ch0=false
        devs=false
    end
    
    properties (Constant)
        analogInputChannelName = 'ai0';
        sampleRate=1000;
        averagingDuration = 0.5;
    end
    
    methods (Access = protected)
        function Initialize(obj) %public methods cause other people will call these                 
            disp(sprintf('Initializing NI DAQ USB 6002'));
            % Finding the DAQ Identifier on the system
                disp(sprintf(['Initializing DAQ...']));
                obj.devs = daq.getDevices; %To use this method one must have 
                %Data Acquisition Toolbox installed
                for kk=1:length(obj.devs)
                    if strcmp(obj.devs(kk).Model,'USB-6002'); USBNo = kk; end
                end
                assert(exist('USBNo', 'var') == 1, 'No NI USB-6002 device found');
            % Opening the Analog input channel
                obj.s = daq.createSession('ni');
                obj.s;
                obj.s.Rate = obj.sampleRate;
                obj.s.DurationInSeconds = obj.averagingDuration;
                obj.ch0 = obj.s.addAnalogInputChannel(obj.devs(USBNo).ID, obj.analogInputChannelName, 'Voltage');
                obj.ch0.TerminalConfig = 'SingleEnded';
                disp(sprintf(['Voltage on DAQ = %2.3f mV'], 1e3.*GetVoltage(obj)));
        end
        
        function ShutDown(obj)
            release(obj.s);
        end
        
        function V = GetVoltage(obj)
            V =  inputSingleScan(obj.s); %Single scan of voltage on the DAQ
        end

        function V = GetVoltageMean(obj, t)
            switch nargin
                case 2
                    obj.s.DurationInSeconds = t;
                otherwise
                    obj.s.DurationInSeconds = obj.averagingDuration;
            end
            data =  startForeground(obj.s); 
            V = mean(data); %Mean value of the voltage on the DAQ in period t
        end
    end
end
