classdef NIDaqUSBFactory < interfaces.IDaqFactory
    %   LaserFactory Creates laser objects for CoBriteDX1 laser
    %   Detailed explanation goes here
    
    properties (Constant)
        Name = 'NI-DAQ-USB-6002';
    end
    
    methods 
        function daq = Create(~)
            daq = NIDaqUSB6002.NIDaqUSB();
        end
    end
    
end
