classdef Spectrum < interfaces.ISpectrum
    %SPECTRUM Dummy spectrum class
    
    properties (Access = private)
        laser
        daq
    end
    
    methods
        function obj = Spectrum(laser, daq, spectrum)
            
            addlistener(obj, 'OperatingLambda', 'PostSet', @obj.onOpLambdaChanged);
            
            obj.laser = laser;
            obj.daq = daq;
            obj.Spectrum = spectrum;
            obj.OperatingLambda = mean(spectrum(1,:));
        end
        
        function Calibrate(obj, appliedVoltageFile, appliedVoltage)
            % Calibrates this spectrum object with the passed voltage
            obj.CalibrationFactor = 100000;
        end
        function voltages = ProcessData(obj, powerLevels)
            % Performs a direct conversion using the calibration factor of the
            % passed data
            voltages = powerLevels * obj.CalibrationFactor;
        end
    end
    
    methods (Access = private)
        function onOpLambdaChanged(obj, ~, ~)
            obj.laser.RequestWavelength(obj.OperatingLambda);
        end
    end
    
end

