classdef Algorithm < interfaces.IAlgorithm
    %ALGORITHM Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Constant)
        RoughPoints = 30
        FinePoints = 30
    end
    
    properties (Access = private)
        laser
        daq
    end
    
    methods
        function obj = Algorithm(laser, daq)
            obj.laser = laser;
            obj.daq = daq;
        end
        
        function resonances = GetRoughSpectrum(obj)
            % Gets a rough spectrum from the laser/daq
            %   Returns interface.SpectrumSection objects
            wavelengths = linspace(obj.laser.WavelengthMin, obj.laser.WavelengthMax, obj.RoughPoints);
            voltages = zeros(1, length(wavelengths));
            i = 1;
            for lambda = wavelengths
                obj.laser.RequestWavelength(lambda);
                [~, v] = obj.daq.RequestVoltageMean(0.05);
                voltages(i) = v;
                i = i + 1;
            end
            
            w = load('+dummy/WidebandSpectrum.mat');
            [~,locs] = findpeaks(w.VSpectrumWB);
            
            i = 1;
            resonances = {};
            for jj = locs
                lambda = w.lambdaGrid(floor((jj - i)/2) + i);
                spectrum = [w.lambdaGrid(i:jj); w.VSpectrumWB(i:jj)];
                resonances{end + 1} = interfaces.Resonance(lambda, spectrum);
                i = jj;
            end
        end
        
        function spectrum = GetFineSpectrum(obj, resonance)
            wavelengths = linspace(resonance.WavelengthMin, resonance.WavelengthMax, obj.FinePoints);
            voltages = zeros(1, length(wavelengths));
            i = 1;
            for lambda = wavelengths
                obj.laser.RequestWavelength(lambda);
                [~, v] = obj.daq.RequestVoltageMean(0.05);
                voltages(i) = v;
                i = i + 1;
            end
            
            w = load('+dummy/SpectrumResonanceAt1550.105nm.mat');
            spectrum = dummy.Spectrum(obj.laser, obj.daq, [w.lambdaGridNB; w.VSpectrumNB]);
        end
    end
    
end

