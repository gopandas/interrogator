classdef LaserFactory < interfaces.ILaserFactory
    %DUMMYLASERFACTORY Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Constant)
        Name = 'Dummy'
    end
    
    methods
        function laser = Create(~)
            laser = dummy.Laser();
        end
    end
    
end

