classdef Laser < interfaces.ILaser
    %LASER Dummy laser class
    
    properties
        NeedsShutdown = false
    end
    
    methods
        function obj = Laser()
            obj.WavelengthMin = 1527.60588e-9;
            obj.WavelengthMax = 1566.60488e-9;
            obj.WavelengthStep = 2e-12;
            obj.Powers = [0, 10e-3];
        end
        function delete(obj)
           if obj.NeedsShutdown
               warning('Warning: Dummy laser never had RequestShutdown called before destruction');
           end
        end
    end
    
    methods (Access=protected)
        function Initialize(obj)
            obj.NeedsShutdown = true;
        end
        function ShutDown(obj)
            obj.NeedsShutdown = false;
        end
        function SetWavelength(~, ~)
        end
        function SetPower(~, ~)
        end
    end
    
end

