classdef DAQFactory < interfaces.IDaqFactory
    %DAQFACTORY Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Constant)
        Name = 'Dummy DAQ'
        % Name of this daq type
    end
    
    methods
        function daq = Create(~)
            % Creates a new daq object
            daq = dummy.DAQ();
        end
    end
    
end

