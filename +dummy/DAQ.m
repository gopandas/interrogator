classdef DAQ < interfaces.IDaq
    %DAQ Summary of this class goes here
    %   Detailed explanation goes here
    
    methods (Access = protected)
        function Initialize(~)
            % Initializes the daq
        end
        function ShutDown(~)
            % Shuts down the daq
        end
        function V = GetVoltage(~)
            % Gets a single sample from the DAQ
            V = rand();
        end
        function V = GetVoltageMean(~, t)
            % Gets an averaged sample from the daq over time t
            pause(t);
            V = mean(rand(1, t * 1000));
        end
    end
    
end

