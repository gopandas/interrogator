function output_txt = DataTipFunction(obj,event_obj, rwObj)
% Display the position of the data cursor
% obj          Currently not used (empty)
% event_obj    Handle to event object
% output_txt   Data cursor text string (string or cell array of strings).

    pos = get(event_obj,'Position');
    output_txt = {['t: ',num2str(pos(1),4)],...
        ['V: ',num2str(pos(2),4)]};
    obj.TipHandle.Interpreter = 'tex';
    obj.TipHandle.FontSize = 7;

    % If there is a Z-coordinate in the position, display it as well
    if length(pos) > 2
        output_txt{end+1} = ['Z: ',num2str(pos(3),4)];
    end
end
