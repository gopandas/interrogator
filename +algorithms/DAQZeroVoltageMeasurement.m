clear all; close all;


    % Initializing DAQ
        % Parameters
            analogInputChannelName = 'ai0';
        % Finding the DAQ Identifier on the system
            devs = daq.getDevices;
            for kk=1:length(devs)
                if strcmp(devs(kk).Model,'USB-6002')
                    disp([sprintf('NI-DAQ USB-6002 is device #%i ', kk)... 
                       'and its ID is ' devs(kk).ID]);
                   USBNo = kk; 
                end
            end    
        % Opening the Analog input channel
            s = daq.createSession('ni');
            s;
            s.Rate = 50000;
            s.DurationInSeconds = 3;
            ch0 = s.addAnalogInputChannel(devs(USBNo).ID, analogInputChannelName, 'Voltage');
            ch0.TerminalConfig = 'SingleEnded';
            s.Channels;

            
        data =  s.startForeground; 
        VDAQ0 = mean(data); %Voltage on the DAQ when the laser is OFF
        disp(sprintf('Interrogator Voltage = %3.1f mV', 1e3.*data));

        clear s devs ch0 ai0
save('DAQZeroVoltageMeasurement.mat');
