function [fGridSlotted fGridSlottedFineTuning]= SlotTheGridForFineTuning (fGrid, laserSettings)

        fFineLim = 1e9.*laserSettings.Properties.FTF.lim;
        fFineSpan = fFineLim(2)-fFineLim(1);
        fSpan = fGrid(1)- fGrid(end);
        numberOfFineSlots = ceil(fSpan/fFineSpan);
        % create a subgrid of fine tuning slot borders
            fFineTuningSlotsGridBorders =  fliplr(fGrid(end):(fFineSpan):...
                fGrid(1)) + fFineLim(2);
            fFineTuningSlotsGridBorders = [fFineTuningSlotsGridBorders ...
                fFineTuningSlotsGridBorders(end)-fFineSpan];
            fFineTuningSlotsGridCenters =  ...
                fFineTuningSlotsGridBorders(1:end-1) - fFineLim(2);
            tuningAllRough = 0;
        %creating rough and fine tuning grid slots
            fGridDifference = diff(fGrid);
            if min(abs(fGridDifference))>fFineSpan; tuningAllRough = 1; 
                %Tuning all frequencies with rough tuning method
            else
                nn=1;
                %assigning frequencies into fine tuning slots
                for kk=1:length(fGrid)
                    slotNo(kk) = min(find(fFineTuningSlotsGridBorders < fGrid(kk)))-1;
                end
                fIndices=NaN(numberOfFineSlots,numberOfFineSlots);
                fGridSlottedFineTuning = NaN(numberOfFineSlots,numberOfFineSlots);
                for kk=1:numberOfFineSlots
                    temp=find(slotNo==kk);
                    if size(fIndices(kk,:),2)>=1;
                        fIndices(kk,1:length(temp))=temp;
                        fGridSlotted(kk)=fGrid(fIndices(kk,1));
                        for jj=1:length(temp)
                            fGridSlottedFineTuning(nn,jj) = fGrid(fIndices(kk,jj))-fGrid(fIndices(kk,1));
                        end
                        nn=nn+1;
                    end
                end
            end
