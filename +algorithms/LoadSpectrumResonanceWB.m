function resonance = LoadSpectrumResonanceWB(lambdaStep, laserSettings, daqSessionObj)
    % Loads a resonance value by either capturing a new spectrum or loading
    % the last spectrum
    
    filename = 'SpectrumWideband.mat';
    load('SpectrumWideband.mat')
    function [lambdaGridWB, VSpectrumWB] = getspectrum()
        if exist(filename, 'file') == 2
            choice = questdlg('Would you like to load previous spectrum, saved one or create a new one?',...
                'Spectrum Selection',...
                'Load Previous', 'Load Saved', 'Create New', 'Load Previous');
            switch choice
                case 'Load Previous'
                    vars = load(filename);
                    lambdaGridWB = vars.lambdaInt;
                    VSpectrumWB = vars.vInt;
                    return
                case 'Load Saved'
                    filename1 = uigetfile;
                    vars = load(filename1);
                    lambdaGridWB = vars.lambdaInt;
                    VSpectrumWB = vars.vInt;
                    return
            end
        end

        lambdaGridWB = 1e-9*laserSettings.Properties.wav.lim(1):lambdaStep:1e-9*laserSettings.Properties.wav.lim(2);
        t1=clock; t1= t1([4 5 6]);
        VSpectrumWB = algorithms.CaptureSpectrum (lambdaGridWB, laserSettings, daqSessionObj); 
        t2=clock; t2= t2([4 5 6]); 
        dt = t2-t1;
        if dt(2)<0; dt(2)=60+dt(2); dt(1)=dt(1)-1; end; 
        if dt(3)<0, dt(3)=60+dt(3); end;
        disp(sprintf('Time elapsed %0.0i:%0.0i:%0.0i (hrs:min:sec)', dt));

    end
    [lambdaInt, vInt] = getspectrum();
    beep;
        h0= figure('PaperUnits', 'inches');pos = get (h0, 'PaperPosition');
        w = 3.5;set (h0, 'PaperPosition', [pos(1) pos(2) w pos(4)/pos(3)*w] );
        plot(1e9.*lambdaInt, 1e3.*vInt);
        xlabel('Wavelength [nm]', 'FontSize', 12);ylabel('V [mV]', 'FontSize', 12);
        axis tight; grid on
        proxy = 1e3.*vInt; margin1=0.1; margin2=0.1; 
        ylim([min(proxy)-margin1.*(max(proxy) - min(proxy)) max(proxy)+margin2.*(max(proxy) - min(proxy))]);

%         saveas(h0, 'SpectrumWideband.eps','epsc2');
        saveas(h0, 'SpectrumWideband.png');  
        save(filename, 'lambdaInt', 'vInt');
    beep;
    resonance = algorithms.FindResonance(lambdaInt, vInt);
end
