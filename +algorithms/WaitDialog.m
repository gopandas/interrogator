function WaitDialog(message, timeout)
    % Opens a dialog which will display a countdown. This function returns
    % when the dialog is closed or when the timeout in seconds expires. The first
    % argument is the message to display in the dialog. The second argument
    % is the time to wait before closing the dialog in seconds.
    
    sz = [200 50];
    screensz = get(0, 'ScreenSize');
    xpos = ceil((screensz(3) - sz(1))/2);
    ypos = ceil((screensz(4) - sz(2))/2);
    
    f = figure('units','pixels',...
        'position',[xpos, ypos, sz(1), sz(2)],...
        'toolbar','none',...
        'menu','none');
    
    uicontrol('Style', 'text',...
        'String', message,...
        'Position', [0 sz(2)/2 sz(1) sz(2)/2]);
    
    timeText = uicontrol('Style', 'text',...
        'String', num2str(timeout),...
        'FontSize', 14,...
        'Position', [0 0 sz(1) sz(2)/2]);
    
    function onTick(obj, ev)
        timeout = timeout - 1;
        set(timeText, 'String', num2str(timeout));
        if (timeout < 1)
            uiresume(f);
        end
    end
    
    t = timer('Period', 1,...
        'ExecutionMode', 'fixedDelay',...
        'StartDelay', 1,...
        'TimerFcn', @onTick);
    start(t);
    uiwait(f);
    stop(t);
    
    if ishandle(f)
        close(f);
    end
end
