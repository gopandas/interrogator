function laserStatus=LaserStatus(ser_obj,Port)
    laserStatus=CBMX_query_port_config(ser_obj,Port);
    fprintf('Frequency: %2.4fTHz\n',laserStatus(1));
    fprintf('FTF: actual: %2.3fGHz\n',laserStatus(2));
    fprintf('Power: %2.2fdBm\n',laserStatus(3));
    fprintf('Output state : %d, Tuning state : %d, Dither state: %d\n',laserStatus(4:end));
