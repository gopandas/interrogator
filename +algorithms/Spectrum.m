classdef Spectrum < interfaces.ISpectrum
    
    properties (SetObservable)
        RightLeft % variable which has ones for points that belong on the right spectrum edge and zeros for left edge
    end
    
    properties (Access = private)
      %   operatingLambda; %wavelength at max sens spot
     %    voltageMaxSensSpectrum; %voltage at max sens spectrum
      %   calibration; %calibration factor
 
         lasObj;
         daqObj;
         resultsWindowObj;
    end
        
    methods 
  
        function obj = Spectrum(lasObj, daqObj, spectrum)

            addlistener(obj, 'OperatingLambda', 'PostSet', @obj.onOpLambdaChanged);

            obj.Spectrum = spectrum;
            obj.lasObj = lasObj;
            obj.daqObj = daqObj;

            [maxR, ~, ~] = algorithms.AnalyzeSpectrum(obj.Spectrum);
            obj.LambdaMaxSensitivity = maxR(1);
            obj.VMaxSensitivity = ...
                algorithms.MapThisLambda(obj.LambdaMaxSensitivity, obj.Spectrum);
            if lasObj.State == interfaces.LaserState.Off
                obj.OperatingLambda = obj.LambdaMaxSensitivity;
            else
                obj.OperatingLambda = obj.lasObj.Wavelength;
            end                
        end

        
        function calObj = Calibrate(obj, tCal, vCal, sGain, appliedVoltage, sensor)            
            oldPointer = obj.startWatchCursor();
            EShiftSI = sensor.EShift*1e-12*1e-6; %E shift converted to m/(V/m)         
            
            [~, ~, acdcFlag] = algorithms.findPeakToPeak(tCal, vCal);
            
            if acdcFlag 
                disp('AC coupled');
%                 vCal = 1.24.*vCal + obj.OperatingV;  factor 1.24 is due
%                 to low corner cutoff at 120Hz when TIA is in the AC
%                 coupling mode, so the 60 Hz signal is seen with 25%
%                 attenuation
                vCal = vCal + obj.OperatingV;                  
            else
                disp('DC coupled');
            end

          % Find linear calibration coefficient for Voltage
            [vCalSCOSPP, vCalSCOS0, acdcFlag] = algorithms.findPeakToPeak(tCal, vCal);
            disp('Make sure these two have similar values:');
            disp(sprintf('Calculated OFfset: %3.3f mV, operating voltage: %3.3f',...
                1e3*vCalSCOS0, 1e3*obj.OperatingV));
            calFactorV = appliedVoltage/vCalSCOSPP;
            fprintf('V Calibration ratio is %2.3f V/mV\n', 1e-3*calFactorV);
            
          % Find linear calibration coefficient for E field
            lambdaCal = algorithms.MapThisV (vCal, obj.Spectrum);
            [lambdaCalPP, lambdaCal0] = algorithms.findPeakToPeak(tCal, lambdaCal, 2);
            calFactorE = (lambdaCalPP/EShiftSI)/vCalSCOSPP;
            fprintf('E Calibration ratio is %2.3f (kV/m)/mV\n', 1e-6*calFactorE);
            calObj = algorithms.Calibration(calFactorV, calFactorE, sensor,... 
                tCal, vCal, sGain, obj.OperatingV, acdcFlag, vCalSCOSPP, vCalSCOS0,... 
                lambdaCalPP, lambdaCal0);
            obj.endWatchCursor(oldPointer);
        end

        
        function resultsObj = ProcessData(obj, t, v, sGain, fileN, sensor, rightEdgeMapping)            
            switch nargin
                case 6
                    rightEdgeMapping = ones(length(v),1); 
                    % 1 for right edge
                    % 0 for left edge
            end
            
            oldPointer = obj.startWatchCursor();
            EShiftSI = sensor.EShift*1e-12*1e-6; %E shift converted to m/(V/m)
            
            [~, ~, acdcFlag] = algorithms.findPeakToPeak(t, v);
            

            %Linear V
                disp(sprintf('operating voltage is %3.3f mV', 1e3.*obj.OperatingV));
                vShifted = v - obj.OperatingV;    
                vLin = vShifted.*obj.Calibration.CalibrationFactorV;
                
            %Linear E
                eLin = vShifted.*obj.Calibration.CalibrationFactorE;
                
            %NonLinear E
                if acdcFlag
                    vshifted = v+obj.OperatingV;
                    lambdaMapped = algorithms.MapThisV (vshifted, ...
                        obj.Spectrum, rightEdgeMapping);
                else
                    lambdaMapped = algorithms.MapThisV (v, obj.Spectrum, rightEdgeMapping);
                end
                lambdaMappedShift = lambdaMapped - obj.OperatingLambda;
                eNonLin = lambdaMappedShift./EShiftSI;
            
            %NonLinear V
                EVConversionFactor = obj.Calibration.CalibrationFactorV/obj.Calibration.CalibrationFactorE;
                vNonLin = eNonLin.*EVConversionFactor;
                
            [~, fileName, ~] = fileparts(fileN);
            resultsObj = algorithms.Results(fileName, t, v, sGain, ...
                obj.OperatingV, obj.OperatingLambda, vLin, vNonLin, eLin, eNonLin);
            % Saving all the calibrated data
                newFolder = '.\dataFiles\';
                if ~exist(newFolder); mkdir(newFolder); end
                save([newFolder fileName datestr(now, 'yy-mm-dd_HH-MM-SS') '_calibrated.mat'],...
                    't', 'eLin', 'vLin', 'eNonLin', 'vNonLin');
            obj.endWatchCursor(oldPointer);
        end
    end
    
    methods (Access = private)
        function onOpLambdaChanged(obj, ~, ~)
            if obj.OperatingLambda == obj.LambdaMaxSensitivity;
                obj.OperatingV = obj.VMaxSensitivity;
            else
                if obj.OperatingLambda>=obj.LambdaMin && obj.OperatingLambda==obj.LambdaMax
                    obj.OperatingV = ...
                    algorithms.MapThisLambda(obj.OperatingLambda, obj.Spectrum);
                else
                    [daqOK, obj.OperatingV] = obj.daqObj.RequestVoltage;
                    assert(daqOK, 'warning: voltage could not be read on the DAQ');
                end
            end
        end

        function oldPointer = startWatchCursor(~)
            fHandle = gcf;
            oldPointer = get(fHandle, 'Pointer');
            set(fHandle, 'Pointer', 'watch');
        end

        function endWatchCursor(~, oldPointer)
            fHandle = gcf;
            set(fHandle, 'Pointer', oldPointer);
        end
        
    end
end
