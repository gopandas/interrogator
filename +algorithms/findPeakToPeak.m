function [peak_to_peak, offset, ACDC] = findPeakToPeak(x0, y0, units)
% findPeakToPeak - find peak to peak value of the y0(x0) data set at the dominant
% frequency in terms of x0
% units - is a designator of units of y0, 1 for Volts (voltage) and 2 for m (wavelength)

% CONSTANTS 
noiseTolerance = 100e-3;

switch nargin 
    case 2
        units = 1; 
        % 1 for Volts (voltage), and 
        % 2 for m (wavelength)
end

oldPointer = startWatchCursor();

% 1. Determining peak-to-peak in frequency domain (best for harmonic signals)
        L=length(x0); %Number of samples
		fs=L/(max(x0)-min(x0)); %Sample rate
		fn=fs/2; %Nyquist frequency
		Y_f=fft(y0)/L;
		frequency = (0:floor(L/2))./(L/2).*fn;
        Y_fourier = 2*abs(Y_f(1:size(frequency,2)));
        %find most dominant peaks
        nPeaks = 50;
        [pks1, loc1, w1, p1]=findpeaks(Y_fourier, 'NPeaks', nPeaks, 'SORTSTR', 'descend');
        %keep only tall peaks:  peaks taller than 1% of the tallest peak
        nPeaks = sum(pks1>0.01*pks1(1));
        [pks1, loc1, w1, p1]=findpeaks(Y_fourier, 'NPeaks', nPeaks, 'SORTSTR', 'descend');
        
        freq=frequency(loc1(1)); amp1=pks1(1);        
        
        peak_to_peak1 = 2*pks1(1);
        offset1 = Y_fourier(1)/2;
        
% 2. Determining peak-to-peak in time domain (best for transient signals)
        nPeaks = 50;
        [pks2, loc2, w2, p2]=findpeaks(y0, 'NPeaks', nPeaks, 'SORTSTR', 'descend');
        %keep only wide peaks: with widths greater than 1% of the widest peak
        %reason for this is to discard the spikes in signal due to RF noise
        %and arcing
            pks2 = pks2(w2>0.01*max(w2)); loc2 = loc2(w2>0.01*max(w2));
            p2 = p2(w2>0.01*max(w2)); w2 = w2(w2>0.01*max(w2));
        
        [pks2minus, loc2minus, w2minus, p2minus]=findpeaks(-y0, 'NPeaks', nPeaks, 'SORTSTR', 'descend');
        %keep only wide peaks: with widths greater than 1% of the widest peak
            pks2minus = pks2minus(w2minus>0.01*max(w2minus)); 
            loc2minus = loc2minus(w2minus>0.01*max(w2minus));
            p2minus = p2minus(w2minus>0.01*max(w2minus)); 
            w2minus = w2minus(w2minus>0.01*max(w2minus));

        peak_to_peak2 = max(pks2) + max(pks2minus);
        offset2 = mean(y0);
        
% 3. Determining DC offset in time domain by averaging 
        y0Prime = diff(y0); y0Second = diff(y0Prime);
        [~, loc3] = findpeaks(y0Second, 'NPeaks', 1, 'SORTSTR', 'descend');
        offset3 = mean(y0(1:loc3));
        
% conclusion
    isHarm = isHarmonic(pks1, pks2, offset1);
    if isHarm 
        disp('Calibration signal is harmonic');
        peak_to_peak = peak_to_peak1;
        offset = offset1;
        ACDC = isACCoupled (offset, noiseTolerance);
    else
        disp('Calibration signal is transient');
        peak_to_peak = peak_to_peak2;
        offsetVector = [offset1 offset2 offset3];
        offsetAvg = mean(offsetVector); offsetStd = std(offsetVector);
        in = find((offsetVector-repmat(offsetAvg, size(offsetVector)))<=...
            repmat(2*offsetStd, size(offsetVector)));
        if length(in)==3 
            offset = offset1;
        else
            offset = mean(offsetVector(in));
        end
        ACDC = isACCoupled (offset, noiseTolerance);
    end        

% summary
    switch units
        case 1
            if isHarm
                disp(sprintf('Peak to peak = %3.2f mV, frequency %3.2f Hz, Offset = %3.2f mV',... 
                    1e3*peak_to_peak, freq, 1e3*offset));
            else
                disp(sprintf('Peak to peak = %3.2f mV, Offset = %3.2f mV',... 
                    1e3*peak_to_peak, 1e3*offset));
            end
        case 2
            if isHarm
                disp(sprintf('Peak to peak = %3.2f pm, frequency %3.2f Hz, Offset = %3.2f nm',... 
                    1e12*peak_to_peak, freq, 1e9*offset));
            else
                disp(sprintf('Peak to peak = %3.2f pm, Offset = %3.2f nm',... 
                    1e12*peak_to_peak, 1e9*offset));
            end
    end
endWatchCursor(oldPointer);
end

function harmonic = isHarmonic (pks1, pks2, offset)
    peakTime = pks2(1)-offset;
    peakFreq = pks1(1);
    
    peakDifRel = (peakTime - peakFreq)/peakFreq;
    
    if peakDifRel < 1
        % dominant peak calculated using FFT should be similar to peak calculated
        % in time domain for periodic signals, 
        % the relative difference between the two is required to be less than 100%
        harmonic = true;
    else
        harmonic = false;
    end
    
end

function ACCoupled = isACCoupled (offset, noiseTolerance)
    ACCoupled = abs(offset)<noiseTolerance;
end

function oldPointer = startWatchCursor()
    oldPointer = get(gcf, 'Pointer');
    set(gcf, 'Pointer', 'watch');
end

function endWatchCursor(oldPointer)
    set(gcf, 'Pointer', oldPointer);
end

