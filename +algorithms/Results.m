classdef Results
% RESULTS - results object

    properties (SetAccess = private)
        filename; % filename with the measured data
        t; % time vector
        v; % measured voltage data
        sGain; % small signal gain
        operatingV; % the DC offset at the operating wavelength 
        operatingLambda; % operating wavelength at which the measurements were taken
        vLin, vNonLin, eLin, eNonLin; % all types of calibration results
    end
        
    methods 
  
        function obj = Results(filename, t, v, sGain, vOperating, lambdaOperating, ...
                vLin, vNonLin, eLin, eNonLin)
            obj.filename = filename;
            obj.t = t; obj.v = v; 
            obj.sGain = sGain;
            obj.operatingV = vOperating;
            obj.operatingLambda = lambdaOperating;
            obj.vLin = vLin; obj.vNonLin = vNonLin; 
            obj.eLin = eLin; obj.eNonLin = eNonLin; 
        end
    
    end
end