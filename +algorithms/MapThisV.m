function lambdaMapped = MapThisV (v, spectrum, rightleft)
% MAPTHIS maps the points in the vector vMap voltage 
% to corresponding wavelength vector using 
% the spectrum lambda, spectrumVoltage

% by default the first point is found on the right side of the resonance
switch nargin 
    case 2
        rightleft = ones(length(v),1); 
        %right edge for a value 1
        %left edge for value of 0
end

oldPointer = startWatchCursor();

lambda = spectrum(1,:);
spectrumVoltage = spectrum(2,:);



% find the splitting point between left and right edge
indMid = find (spectrumVoltage==min(spectrumVoltage));
assert (length(indMid)==1, 'multiple minimum points in the spectrum found');

% find the maxima on the left and right and cut them off
indLeft = max( find (spectrumVoltage(1:indMid-1)>...
     spectrumVoltage(indMid) + 0.9*(max(spectrumVoltage(1:indMid-1)) - spectrumVoltage(indMid)) ) );
indRight = indMid -1 +... 
    min(find (spectrumVoltage(indMid:end)>...
    spectrumVoltage(indMid) + 0.9*(max(spectrumVoltage(indMid:end)) - spectrumVoltage(indMid) ) ));

% forming the cropped left and right edge of the spectrum 
% only non-monotonous parts on the ends of the resonance are cut off, not in the middle
spectrumLeft = spectrumVoltage(indLeft:indMid-1);
lambdaLeft = lambda(indLeft:indMid-1);
spectrumRight = spectrumVoltage(indMid:indRight);
lambdaRight = lambda(indMid:indRight);

smtValues = 1:2:300; 


% initialize the lambdaMapped value to all-zeros
lambdaMapped = zeros(size(v)); 

% find the blocks of zeros and ones in the rightleft vector
blocks = diff(rightleft); 
blocksIndices = find(blocks~=0); 
blocksIndices = [1 (blocksIndices'+1); blocksIndices' length(v)];
    % blocksIndices columns are the end-indices of each interchanging 1 and 0-block

    
h_w = waitbar(0, 'wait...');
set(h_w, 'pointer', 'watch');
oldPointer = startWatchCursor();
tic
i0 = size(blocksIndices, 2);

nonmonotonic = 1; smt = 1;

    for i=1:i0
        vBlock = v(blocksIndices(1, i):blocksIndices(2, i));
        if rightleft(blocksIndices(1,i))
            % look on the right edge
            
                %Smooth out Right spectra until it is monotonic
                nonmonotonic = 1; 
                k=1;
                while nonmonotonic 
                    spectrumRight = smooth(spectrumRight, smtValues(k));
                    nonmonotonic = ~all(diff(spectrumRight)>0);
                    if nonmonotonic 
                        k=k+1;
                        spectrumRight = smooth(spectrumRight, smtValues(k));
                    end
                end

            dv = diff(spectrumRight);
            if all(dv>0)||all(dv<0)
                % map the block
                    % first, all the values that exceed spectrum values are capped
                    vBlock(vBlock>max(spectrumRight)) = max(spectrumRight);
                    vBlock(vBlock<min(spectrumRight)) = min(spectrumRight);
                lambdaMapped(blocksIndices(1, i):blocksIndices(2, i)) = ...
                interp1(spectrumRight, lambdaRight, vBlock);
            else
%                 warndlg('Mapping non-monotonic vector');
            end
        else
            % look on the left edge
            
                %Smooth out Left spectra until it is monotonic
                nonmonotonic = 1; 
                k=1;
                while nonmonotonic 
                    spectrumLeft = smooth(spectrumLeft, smtValues(k));
                    nonmonotonic = ~all(diff(spectrumLeft)<0);
                    if nonmonotonic 
                        k=k+1;
                        spectrumLeft = smooth(spectrumLeft, smtValues(k));
                    end
                end
                
            % if non-monotonic, smooth with increasing smoothing coefficient until
            % monotonic
            dv = diff(spectrumLeft); smt = 0;
            spectrumLeftNew = spectrumLeft;
            while ~(all(dv>0)||all(dv<0))
                % find out first if the non-monotonicity is only in the first
                % third of the edge, in that case crop from the left
                dvFirstThird = diff(spectrumLeftNew(1:floor(length(spectrumLeftNew)/3)));
                dvRest = diff(spectrumLeftNew(floor(length(spectrumLeftNew)/3)+1:end));
                isMonotonicFirstThird = all(dvFirstThird>0)||all(dvFirstThird<0);
                isMonotonicRest = all(dvRest>0)||all(dvRest<0);
                if ~isMonotonicFirstThird&&isMonotonicRest
                    spectrumLeftNew = spectrumLeftNew(2:end);
                else 
                    smt = smt + 1;
                    spectrumLeftNew = smooth (spectrumLeftNew, smt);
                end
                dv = diff(spectrumLeftNew);
            end
                % map the block
                    % first, all the values that exceed spectrum values are capped
                    vBlock(vBlock>max(spectrumLeftNew)) = max(spectrumLeftNew);
                    vBlock(vBlock<min(spectrumLeftNew)) = min(spectrumLeftNew);
                lambdaMapped(blocksIndices(1, i):blocksIndices(2, i)) = ...
                interp1(spectrumLeftNew, lambdaLeft, vBlock);
        end
        waitbar(i/i0, h_w); drawnow;
    % things can possibly go wrong here on the non-monotonous parts of the spectrum
    end

toc
close (h_w);
endWatchCursor(oldPointer);
end

function oldPointer = startWatchCursor()
    oldPointer = get(gcf, 'Pointer');
    set(gcf, 'Pointer', 'watch');
end

function endWatchCursor(oldPointer)
    set(gcf, 'Pointer', oldPointer);
end



