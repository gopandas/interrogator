function vMapped = MapThisLambda (lambda0, spectrum)
% MAPTHIS maps the points in the vector lambda 
% to corresponding voltage vector using 
% the spectrum lambda, spectrumVoltage


lambda = spectrum(1,:);
spectrumVoltage = spectrum(2,:);
% by default the first point is found on the right side of the resonance


% find the splitting point between left and right edge
indMid = find (min(spectrumVoltage)==spectrumVoltage);
assert (length(indMid)==1, 'multiple minimum points in the spectrum found');

% initialize the lambdaMapped value to all-zeros
vMapped = zeros(size(lambda0)); 

% check the segment for monotonicity before interpolation
dlambda = diff(lambda);
if all(dlambda>0)||all(dlambda<0)
    % map the lambda0
    vMapped = interp1(lambda, spectrumVoltage, lambda0);
else
    warndlg('Mapping non-monotonic vector');
    return;
end

end




