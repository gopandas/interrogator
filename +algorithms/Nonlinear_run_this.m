clear all; close all; %#ok<CLSCR>


%% Initialization 
    %parameters
    SCOSpolarity = 1; %1 (Default) means that SCOS positive voltage is measuring positive field
    Eshift = 50; %Shift in pm/MV/m
    
        % Spectrum scan settings
            widebandSpectrumNoOfPoints = 3;
   
    % Initializing the laser
        global max_time
        max_time=45; %max time to wait after tuning [s]
        laserPowerdBm = 16; laserPowermW = DBmToMW (laserPowerdBm); 

        x = inputdlg('COM',...
             'Enter Laser COM port', [1 50]);
        laserCOMPort=['COM' char(x)];
        laserCOMPort(laserCOMPort==' ')=[];
        [ser_obj, Port, port] = LaserON (laserCOMPort);



        
    % Initializing DAQ
        % Parameters
            analogInputChannelName = 'ai0';
        % Finding the DAQ Identifier on the system
            devs = daq.getDevices;
            for kk=1:length(devs)
                if strcmp(devs(kk).Model,'USB-6002')
                    disp([sprintf('NI-DAQ USB-6002 is device #%i ', kk)... 
                       'and its ID is ' devs(kk).ID]);
                   USBNo = kk; 
                end
            end    
        % Opening the Analog input channel
            s = daq.createSession('ni');
            s;
            s.Rate = 50000;
            s.DurationInSeconds = 0.1;
            ch0 = s.addAnalogInputChannel(devs(USBNo).ID, analogInputChannelName, 'Voltage');
            ch0.TerminalConfig = 'SingleEnded';
            s.Channels;

        
        
%% Finding the operating wavelength

    % Turning on the laser  
        data =  s.startForeground; 
        VDAQ0 = mean(data); %Voltage on the DAQ when the laser is OFF
        disp(sprintf('Interrogator Voltage = %3.1f mV', 1e3.*VDAQ0));

        disp('Turning on the Laser');
        CBMX_set_port_state(ser_obj,Port,1); %always switch Port on for next steps
        tic; wait_tuning(ser_obj,Port,max_time); toc;
        CBMX_set_port_power(ser_obj,Port,laserPowerdBm);
        wait_tuning(ser_obj,Port,max_time);
        pause(12); % leave in for power tuning

        data = s.inputSingleScan;
        disp(sprintf('Interrogator Voltage = %3.1f mV', 1e3.*mean(data)));

        LaserStatus(ser_obj,Port) ;
        
        port.FTF.lim=CBMX_query_port_FTF_limit(ser_obj,Port);
        fprintf('FTF limits setting actual: %2.3fGHz to %2.3fGHz\n',port.FTF.lim);

    % Capturing the Wideband Spectrum
        lambdaStep = 1.5e-9;
        laserSettings = struct ('serialObject', ser_obj, 'Port', Port, 'Properties', port);
        resSelect = LoadSpectrumResonanceWB(lambdaStep, laserSettings, s);




    % Capturing the Narrow Resonance Spectrum
        lambdaStep = .05e-9;
        %Query Laser Config
        port.conf.read=CBMX_query_port_config(ser_obj,Port);
        if port.conf.read(3)<port.power.lim(1); port.conf.read(3)=port.power.lim(1);end
        if port.conf.read(3)>port.power.lim(2); port.conf.read(3)=port.power.lim(2);end
        laserSettings = struct ('serialObject', ser_obj, 'Port', Port, 'Properties', port);
        [lambdaRes, vResRaw]=LoadSpectrumResonanceNB(resSelect, lambdaStep, laserSettings, s);
        load handel.mat;
        sound(y, Fs);

        % smooth out the spectrum
        smt=3;
        vRes=smooth(vResRaw, smt)';
    % Keeping only the part of the spectrum with Monotonically rising/falling edges
        [indCropA, indCropB, indBot] = MakeMonotonous (lambdaRes, vRes);       
        
    % Analyze Spectrum to find the point of highest sensitivity
        smt=10;
        [MaxSensR, widthR, MaxSensL] = AnalyzeSpectrum (lambdaRes, vRes, smt);
        lambdaMaxSens = MaxSensR(1); indMaxSens = MaxSensR(2);
        vMaxSensSpectrum = vRes(indMaxSens);
        
    % Set the laser wavelength to highest sensitivity point
        CBMX_set_port_frequ(ser_obj,Port,1e-12.*LambdaToF(lambdaMaxSens));
        pause(1);
        wait_tuning(ser_obj,Port,max_time);
        fprintf('Laser set to wavelength: %4.3f nm\n', 1e9.*lambdaMaxSens);

    % read from DAQ the voltage at operating point when no field is applied
        data = s.startForeground;
        VMaxSens0=mean(data);
        fprintf('Voltage at the operating wavelength: %4.1f mV\n', 1e3.*VMaxSens0);

    % Operating point statistics
        fprintf('Based on estimated E shift of : %2.1f pm/MV/m\n', Eshift);
        maxMeasurableField1 = 1e12.*(lambdaRes(end)-lambdaMaxSens)/Eshift;
        fprintf('Maximum anticipated positive Field : %2.1f MV/m\n', maxMeasurableField1);
        maxMeasurableField2 = 1e12.*(lambdaMaxSens-lambdaRes(indBot))/Eshift;
        fprintf('Maximum anticipated negative Field : %2.1f MV/m\n', maxMeasurableField2);
%% closing 
        LaserOFF(ser_obj, Port);
        clear s ch0 devs
        
        % save workspace to be loaded in the 
        save('Workspace1.mat');
