function [ser_obj, Port, port] =  LaserON (com_port)


        delete(instrfind); %first clean up before doing anything
    % Parameters
        %look up serial port number in Device Manaager of Windows
         ser_obj=serial(com_port,'BaudRate',115200);
        % use the following line to access chassis via Ethernet/Telnet and comment
        % out the command for serial connection
        % ser_obj = tcpip(IP_address, 10001); % open telnet connection via Ethernet
        ptime=2; % pause time used after setting a value
        % enter here ports to test
        Port=[1 1 1]; % do not use wildcards here
        % note: Echo needs to be off to make code function


    % Opening communication port
        clear port;
        fopen(ser_obj); % open serial port
        fprintf(ser_obj,';'); % flush of Laser chassis buffer by sending command termination signal

    % Checking the temperature
        disp('testing monitor values');
        port.wav.mon=CBMX_query_monitor_values(ser_obj,Port);
        fprintf('LaserDiode chip temp: %2.2f�C, LaserDiode base temp %2.2f�C, LaserDiode current %3.1fmA, TEC current %3.1fmA\n',port.wav.mon);
    % Reading laser Power limits
        port.power.lim=CBMX_query_port_power_limit(ser_obj,Port);
    % Reading laser Frequency limits
        port.frequency.lim=CBMX_query_port_frequ_limit(ser_obj,Port);
        lambdaCBand = 299792458./(fliplr(port.frequency.lim).*1e12);
    % Reading laser Wavelength limits
        port.wav.lim=CBMX_query_port_wav_limit(ser_obj,Port);
