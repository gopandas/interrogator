function lambda = FToLambda (f)
lambda = 299792458./f;
if length(f)>1; disp('Warning frequency should be a scalar, not vector');end

