function wait_tuning(ser_obj,Port,max_time)
% N. Stan
% waits until laser is busy or max_time [s] is elapsed

timer_start=now;
fprintf('laser busy ...');
while ((now-timer_start)*24*3600)<=max_time
    if all(~CBMX_query_tuning_state(ser_obj,Port))
        break;
    end
    pause(1);
   fprintf('.');
end
if ((now-timer_start)*24*3600)>=max_time
    error('tuning time exceeded!');
else
    disp('  settled!');
end