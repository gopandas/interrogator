function [MaxSensR, widthR, MaxSensL] = AnalyzeSpectrum (spectrum)

lambda = spectrum(1, :);
Vs = spectrum(2, :);
smt = 1;


% slope
    VsPrime = diff(Vs)./diff(lambda); 
    VsPrimes = smooth(VsPrime, smt);
    lambdaPrime = (lambda(2:end)+lambda(1:end-1))./2;

        %cropping the edges off because of discontinuities
            cropPercentage = 0.05;
            ind1 = ceil(length(lambdaPrime)*cropPercentage); lambda1 = lambdaPrime(ind1);
            ind2 = floor(length(lambdaPrime)*(1-cropPercentage)); lambda2 = lambdaPrime(ind2);

            VPrimeCropped = [zeros(ind1-1,1); VsPrimes(ind1:ind2); zeros((length(VsPrimes)-ind2),1)];



%     h0= figure('PaperUnits', 'inches');pos = get (h0, 'PaperPosition');
%     w = 3.5;set (h0, 'PaperPosition', [pos(1) pos(2) w pos(4)/pos(3)*w] );
%     plot (lambdaPrime, VPrimeCropped);
%     grid on
%     proxy = VPrimeCropped;margin1=0.1; margin2=0.1; 
%     ylim([min(proxy)-margin1.*(max(proxy) - min(proxy)) max(proxy)+margin2.*(max(proxy) - min(proxy))]);
% 
%         %crop lines
%         line ([lambda1 lambda1], [min(VsPrimes) max(VsPrimes)], 'Color', 'black');
%         line ([lambda2 lambda2], [min(VsPrimes) max(VsPrimes)], 'Color', 'black');

    indMax = find (VPrimeCropped == max(VPrimeCropped)); lambdaMax = lambdaPrime(indMax);
    %line ([lambdaMax lambdaMax], [min(VsPrimes) max(VsPrimes)], 'Color', 'red');

    indPlus10Perc = max(find (VPrimeCropped>0.9*max(VPrimeCropped))); lambdaMinus10PercPlus = lambdaPrime(indPlus10Perc);
    %line ([lambdaMinus10PercPlus lambdaMinus10PercPlus], [min(VsPrimes) max(VsPrimes)], 'Color', 'yellow');

    indMinus10Perc = min(find (VPrimeCropped>0.9*max(VPrimeCropped))); lambdaMinus10PercMinus = lambdaPrime(indMinus10Perc);
    %line ([lambdaMinus10PercMinus lambdaMinus10PercMinus], [min(VsPrimes) max(VsPrimes)], 'Color', 'yellow');
    
    indMin = find (VPrimeCropped == min(VPrimeCropped(VPrimeCropped~=0))); lambdaMin = lambdaPrime(indMin);
%     line ([lambdaMin lambdaMin], [min(VsPrimes) max(VsPrimes)], 'Color', 'blue');
% 
%     xlabel('Wavelength [nm]', 'FontSize', 12);ylabel('Slope [V/nm]', 'FontSize', 12);
%     
% %     saveas(h0, 'Slope_sensitivity_points.eps','epsc2');
%     saveas(h0, 'Slope_sensitivity_points.png');

    MaxSensR = [lambdaMax VsPrimes(indMax)];  
    widthR = lambdaMinus10PercPlus - lambdaMinus10PercMinus;
    MaxSensL = [lambdaMin, VsPrimes(indMin)];
    
    fprintf('Maximum sensitivity at lambda = %4.3f nm\n', 1e9*lambdaMax);
    fprintf('90%% sensitivity width = %4.3f pm\n', 1e12*widthR);
    fprintf('Minimum sensitivity at lambda = %4.3f nm\n', 1e9*lambdaMin);
