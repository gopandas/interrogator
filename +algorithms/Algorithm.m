classdef Algorithm < interfaces.IAlgorithm
    properties (Constant)
         RoughPoints = 30;  %30
         %RoughPoints needs to be at least 20 to see all resonances properly in 1530-1575nm range
         %recommended 25
         %Pure Photonics laser has a wider wavelength range so the
         %RoughPoints is better increased to 35
         max_time=45; %max time to wait after tuning [s]
    end
   properties
        laserObj;
        daqObj;
        sensorObj;
   end
   methods     
       
        function obj = Algorithm(laserObj, daqObj, sensorObj)
            obj.laserObj = laserObj;
            obj.daqObj = daqObj;
            obj.sensorObj = sensorObj;
        end
     
       
        function resonances = GetRoughSpectrum(obj)
            %returns an array of spectrums

            %Calculating the wavelengths
            if ((obj.laserObj.WavelengthMax - obj.laserObj.WavelengthMin) / obj.RoughPoints) >= obj.laserObj.WavelengthStep
                wavelengths = linspace(obj.laserObj.WavelengthMin, obj.laserObj.WavelengthMax, obj.RoughPoints);
            else
                wavelengths = (obj.laserObj.WavelengthMin):obj.laserObj.WavelengthStep:(obj.laserObj.WavelengthMax);
            end
            
            voltages = zeros(1, length(wavelengths));
            i = 1; time1 = tic;
            clear timeCumulative;
            timeCumulative = zeros(obj.RoughPoints,1);
            if exist('defaultSettings.mat', 'file'); 
                load('defaultSettings.mat', 'WavelengthScanDuration'); 
                tRemaining = round((obj.RoughPoints-i)*WavelengthScanDuration);
                h_w = waitbar ((i)/(obj.RoughPoints),{'Fine scan...',... 
                    sprintf('Scan %i of %i. Time remaining: %3.2f min',...
                    i, obj.RoughPoints, tRemaining/60)});
            else
                h_w = waitbar (0,{'Fine scan... ',...
                    sprintf('Scan %i of %i, Estimating time...', i, obj.RoughPoints)});
            end
            set (h_w, 'pointer', 'watch');            
            for lambda = wavelengths
                timeI = tic;                
                obj.laserObj.RequestWavelength(lambda);
                [~, v] = obj.daqObj.RequestVoltageMean(0.05);
                voltages(i) = v;
                timeCumulative(i) = toc(timeI);
                tRemaining = round((obj.RoughPoints-i)*mean(timeCumulative(1:i)));
                i = i + 1;
                if i == 23
                   disp(''); 
                end
                if ~h_w.isvalid; resonances=[]; return; end
                if strcmp(h_w.BeingDeleted, 'off')
                    if tRemaining>60
                        waitbar((i)/(obj.RoughPoints), h_w, ...
                        {'Rough scan... ', sprintf('Scan %i of %i. Time remaining: %3.2f min',...
                        i, obj.RoughPoints, tRemaining/60)});
                    else
                        waitbar((i)/(obj.RoughPoints), h_w, ...
                        {'Rough scan... ', sprintf('Scan %i of %i. Time remaining: %i s',...
                        i, obj.RoughPoints, tRemaining)});
                    end
                else
                    return;
                end
            end
            close(h_w); toc(time1)
            [~,locs] = findpeaks(voltages);

            i = 1;
            resonances = {};
            for jj = locs
                spectrum = [wavelengths(i:jj); voltages(i:jj)];
                resonances{end + 1} = interfaces.Resonance(spectrum);
                i = jj;
            end
            
            if isempty(obj.sensorObj)
                sensorName = 'NoName';
            else
                sensorName = obj.sensorObj.Name;
            end
            save(['WBSpectrum' datestr(now, 'yy-mm-dd_HH-MM-SS') '_' sensorName '.mat'], 'resonances');
            
            WavelengthScanDuration = mean(timeCumulative);
            if exist('defaultSettings.mat', 'file')==2
                save('defaultSettings.mat', 'WavelengthScanDuration', '-append');
            else
                save('defaultSettings.mat', 'WavelengthScanDuration');
            end

        end 


        function spectrum = GetFineSpectrum(obj, resonance, scanResolution)
            % Capturing the Narrow Resonance Spectrum
            
            %Calculating the wavelengths
            if scanResolution<(1e12*obj.laserObj.WavelengthStep) 
                scanResolution = 1e12*obj.laserObj.WavelengthStep;
                disp('resolution too small for the laser, setting to minimum');
            end
            
            wavelengths = resonance.WavelengthMin:(1e-12*scanResolution):resonance.WavelengthMax;
            FinePoints = length(wavelengths);
            
            voltages = zeros(1, length(wavelengths));
            i = 1; time1 = tic;
            clear timeCumulative;
            timeCumulative = zeros(FinePoints,1);
            if exist('defaultSettings.mat', 'file'); 
                load('defaultSettings.mat', 'WavelengthScanDuration'); 
                tRemaining = round((FinePoints-i)*WavelengthScanDuration);
                h_w = waitbar ((i)/(FinePoints),{'Fine scan...',... 
                    sprintf('Scan %i of %i. Time remaining: %3.2f min',...
                    i, FinePoints, tRemaining/60)});
            else
                h_w = waitbar (0,...
                    {'Fine scan... ', sprintf('Scan %i of %i, Estimating time...', i, FinePoints)});
            end
            set (h_w, 'pointer', 'watch');
            wvRead = zeros (1, length(wavelengths));
            for i = 1:length(wavelengths)
                timeI = tic;
                [~, wvRead(i)] = obj.laserObj.RequestWavelength(wavelengths(i));
                [~, v] = obj.daqObj.RequestVoltageMean(0.5);
                voltages(i) = v;
                timeCumulative(i) = toc(timeI);
                tRemaining = round((FinePoints-i)*mean(timeCumulative(1:i)));
                i = i + 1;
                if ~h_w.isvalid;  spectrum=[]; return; end
                if strcmp(h_w.BeingDeleted, 'off')
                    if tRemaining>60
                        waitbar((i)/(FinePoints), h_w, ...
                        {'Fine scan...', sprintf('Scan %i of %i. Time remaining: %3.2f min',...
                        i, FinePoints, tRemaining/60)});
                    else
                        waitbar((i)/(FinePoints), h_w, ...
                        {'Fine scan... ', sprintf('Scan %i of %i. Time remaining: %i s',...
                        i, FinePoints, tRemaining)});
                    end
                end
                beep;
            end
            close(h_w); toc(time1)
            if isempty(obj.sensorObj)
                sensorName = 'NoName';
            else
                sensorName = obj.sensorObj.Name;
            end
            % Get rid of the duplicate values
            k=1;
            wvs(k)= wvRead(1);
            vs(k) = voltages(1); k=k+1;
            for i = 2:length(wvRead)
                dwv = wvRead(i)-wvRead(i-1);
                dV = voltages(i) - voltages(i-1);
                if dwv
                    wvs(k)= wvRead(i);
                    vs(k) = voltages(i);
                    k=k+1;
                end
            end
            
            spectrum = algorithms.Spectrum(obj.laserObj, obj.daqObj, [wvs.*1e-9; vs]);
            save([sprintf('NBSpectrum_%4.3f-%4.3fnm', 1e9.*resonance.WavelengthMax, 1e9.*resonance.WavelengthMin)...
                datestr(now, 'yy-mm-dd_HH-MM-SS') '_' sensorName '.mat'], 'spectrum');
            %for lambda step laser gives us laser min and max and use linspace
            % use kevins code and put it in a vector  (dummy algorithm.m)
            
            WavelengthScanDuration = mean(timeCumulative);
            save('defaultSettings.mat', 'WavelengthScanDuration', '-append');

            load handel.mat;
            sound(y, Fs);

            % smooth out the spectrum
%             smt=3;
%             vRes=smooth(vResRaw, smt)';
            % Keeping only the part of the spectrum with Monotonically rising/falling edges
            %  [indCropA, indCropB, indBot] = algorithms.MakeMonotonous (lambdaRes, vRes); 
        end
        
   end
end
