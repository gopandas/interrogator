clear all; close all;

load ('Workspace1.mat');
smt=5;
vRes=smooth(vResRaw, smt);
lambdaResTrimmed = lambdaRes(indCropA:indCropB);
vResTrimmed = vRes(indCropA:indCropB);

h0= figure('PaperUnits', 'inches');pos = get (h0, 'PaperPosition');w = 3.5;set (h0, 'PaperPosition', [pos(1) pos(2) w pos(4)/pos(3)*w] );
plot (1e9.*lambdaResTrimmed, 1e3.*vResTrimmed)
hold on;
plot (1e9.*lambdaRes(indCropA:indCropB), 1e3.*vResRaw(indCropA:indCropB),'r')


xlabel('Wavelength [nm]', 'FontSize', 12);ylabel('V [mV]', 'FontSize', 12);
saveas(h0, 'spectrum.eps','epsc2');saveas(h0, 'spectrum.png');


%% CALIBRATION
        %load the calibration file
            filename = uigetfile('*.*', 'Select Calibration File');
            switch filename(end-3:end)
                case '.isf'
                    data = isfread (filename); 
                    tCal=data.x; vCal=data.y; clear data;
                case '.csv'
                    M = csvread(filename);
                    tCal = M(:,1); vCal=M(:,2); clear M;
                case '.trc'
                    M=ReadLeCroyBinaryWaveform(filename);
                    tCal = M.x; vCal=M.y; clear M;
            end

            
    %Plot the Calibration measurement with automatic order magnitude labelling
            h0= figure('PaperUnits', 'inches');pos = get (h0, 'PaperPosition');
            w = 3.5;set (h0, 'PaperPosition', [pos(1) pos(2) w pos(4)/pos(3)*w] );
            plotLabeld(tCal, vCal)
            
            

    %processing the calibration measurement
        %Finding Peak to peak of the calibration voltage value
        [vCalSCOSPP VcalMid] = findPeakToPeak(tCal, vCal, 100);
        %Mapping to wavelength shifts
        LambdaCal = findCorrespondingX (vCal, lambdaResTrimmed, vResTrimmed);
        %Finding peak to peak of the wavelength shift
        LambdaCalPP = findPeakToPeak(tCal, LambdaCal, 100); 
     % Calibration Ratio
        v0Cal = 6.36e3;
        calibration_ratio_lin = v0Cal/vCalSCOSPP;
        calibration_ratio_nonlin = v0Cal/LambdaCalPP;
        
        
%% MEASUREMENT
    %Load Measurement file
            filename = uigetfile('*.*', 'Select Measurement File');
            switch filename(end-3:end)
                case '.isf'
                    data = isfread (filename); 
                    t=data.x; v=data.y;
                case '.csv'
                    M = csvread(filename);
                    t = M(:,1); v=M(:,2);
                case '.trc'
                    M=ReadLeCroyBinaryWaveform(filename);
                    t = M.x; v=M.y; clear M;
            end


                h0= figure('PaperUnits', 'inches');pos = get (h0, 'PaperPosition');
                w = 3.5;set (h0, 'PaperPosition', [pos(1) pos(2) w pos(4)/pos(3)*w] );
                plotLabeld(t, v)


    %Linear calibration
    vLin = (v - mean(v(1:500))).*calibration_ratio_lin;
 %  plot(1e3.*t, 1e-3.*vLin);
   smoothed=  smooth(vLin,25);
    h0= figure('PaperUnits', 'inches');pos = get (h0, 'PaperPosition');w = 3.5;set (h0, 'PaperPosition', [pos(1) pos(2) w pos(4)/pos(3)*w] );
   %  plot(1e3.*t, 1e-3.*vLin);
    
  % hold on
  plot(1e3.*t, 1e-3.*smoothed);
% xlim([999.,1003.]);
  grid on
    xlabel('Time [ms]', 'FontSize', 12);ylabel('Voltage [kV]', 'FontSize', 12);
    title({'Linear'});
    saveas(h0, 'Linear.eps','epsc2');saveas(h0, 'Linear.png');
    saveas(h0, 'Linear.fig');

    %Nonlinear calibration  
    lambdaShiftMeasurement= findCorrespondingX (v, lambdaResTrimmed, vResTrimmed);
%     lambdaShifts = lambdaShiftMeasurement - lambdaMaxSens;
    lambdaShifts = lambdaShiftMeasurement - ...
        mean(lambdaShiftMeasurement(1:round(length(lambdaShiftMeasurement)/50)));
    vNonlin = lambdaShifts.*calibration_ratio_nonlin; 
    h0= figure('PaperUnits', 'inches');pos = get (h0, 'PaperPosition');w = 3.5;set (h0, 'PaperPosition', [pos(1) pos(2) w pos(4)/pos(3)*w] );
    plot(1e3.*t, 1e9.*lambdaShifts);
    smoothed2= smooth(vNonlin,25);
    
 %  plot(1e3.*t, 1e-3.*vNonlin);
 %  hold on
    plot(1e3.*t, 1e-3.*smoothed2);
  %  xlim([999.,1003.]);
    grid on
  %  xlim([99.5,100.5]);
    xlabel('Time [ms]', 'FontSize', 12);ylabel('Voltage [kV]', 'FontSize', 12);
    title({'Nonlinear'});
     saveas(h0, 'nonLinear.eps','epsc2');saveas(h0, 'nonLinear.png');
     saveas(h0, 'nonLinear.fig');
    save([filename(1:end-4) '.mat'], 'vLin', 'vNonlin', 't');
