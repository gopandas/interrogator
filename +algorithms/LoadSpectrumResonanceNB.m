function [lambdaInt, vInt] = LoadSpectrumResonanceNB(resonance, lambdaStep, laserSettings, daqSessionObj)
    % Loads a resonance value by either capturing a new spectrum or loading
    % the last spectrum
    
   % filename = [sprintf('SpectrumResonanceAt%4.3fn', 1e9.*resonance.Center) '.mat'];
    
    function [lambdaGridNB, VSpectrumNB] = getspectrum()
%         if exist(filename, 'file') == 2
%             choice = questdlg('Would you like to load the last spectrum used, saved one or create a new one?',...
%                 'Spectrum Selection',...
%                 'Load Previous', 'Load Saved', 'Create New', 'Load Previous');
%             switch choice
%                 case 'Load Previous'
%                     vars = load(filename);
%                     lambdaGridNB = vars.lambdaGridNB;
%                     VSpectrumNB = vars.VSpectrumNB;
%                     return
%                 case 'Load Saved'
%                     filename1 = uigetfile;
%                     vars = load(filename1);
%                     lambdaGridNB = vars.lambdaGridNB;
%                     VSpectrumNB = vars.VSpectrumNB;
%                     return
%             end
%             else
%             choice = questdlg('Would you like to load a saved spectrum or create a new one?',...
%                 'Spectrum Selection',...
%                 'Load Saved', 'Create New', 'Load Saved');
%             switch choice
%                 case 'Load Previous'
%                     vars = load(filename);
%                     lambdaGridNB = vars.lambdaGridNB;
%                     VSpectrumNB = vars.VSpectrumNB;
%                     return
%                 case 'Load Saved'
%                     filename1 = uigetfile;
%                     vars = load(filename1);
%                     lambdaGridNB = vars.lambdaGridNB;
%                     VSpectrumNB = vars.VSpectrumNB;
%                     return
%             end
%         end


    % Single Resonance Scan
        %Scan Parameters
            lambdaGridNB = resonance.Left:lambdaStep:resonance.Right;

        %Time the spectrum capture function execution
            t1=clock; t1= t1([4 5 6]);
        VSpectrumNB = algorithms.CaptureSpectrum (lambdaGridNB, laserSettings, daqSessionObj); 
            t2=clock; t2= t2([4 5 6]); 
            dt = t2-t1;
            if dt(2)<0; dt(2)=60+dt(2); dt(1)=dt(1)-1; end; 
            if dt(3)<0, dt(3)=60+dt(3); end;
            disp(sprintf('Time elapsed %02.0f:%02.0f:%02.0f (hrs:min:sec)', dt));
      
% 
%         
%         h0= figure('PaperUnits', 'inches');pos = get (h0, 'PaperPosition');
%         w = 3.5;set (h0, 'PaperPosition', [pos(1) pos(2) w pos(4)/pos(3)*w] );
%         plot(1e9.*lambdaGridNB, 1e3.*VSpectrumNB);
%         xlabel('Wavelength [nm]', 'FontSize', 12);ylabel('V [mV]', 'FontSize', 12);
%         axis tight; grid on
%         proxy = 1e3.*VSpectrumNB; margin1=0.1; margin2=0.1; 
%         ylim([min(proxy)-margin1.*(max(proxy) - min(proxy)) max(proxy)+margin2.*(max(proxy) - min(proxy))]);
% 
%         saveas(h0, [sprintf('SpectrumResonanceAt%4.3fnm', 1e9.*resonance.Center) '.png']);
%         save ([sprintf('SpectrumResonanceAt%4.3fnm', 1e9.*resonance.Center) '.mat'], 'lambdaGridNB', 'VSpectrumNB');
%     
% 
%                 
% 
%         save(filename, 'lambdaGridNB', 'VSpectrumNB');
    end

    [lambdaInt, vInt] = getspectrum();

end


