function resonance = FindResonance (lambdaInt, VInt)
    % Creates a user dialog to select portions of resonances. Arguments are
    % the lambda spectrum and the voltage spectrum, all with matched
    % indices.
    
    VInt = 1e3.*VInt;
    resonances = [];
    threeDB = max(VInt) * 0.5; % 3dB threshold
    [highs, iHigh] = findpeaks(VInt );
    [lows, iLow] = findpeaks(-VInt, 'MinPeakHeight', -threeDB);
    
    % find the following pattern: A max followed by a min no larger than
    % the 10% of the max followed by another max within 10% of the first
    % max
    percentageWidening = 0.1;
    for i = iLow
        leftMaxes = iHigh(iHigh < i);
        rightMaxes = iHigh(iHigh > i);
        if (length(leftMaxes) < 1 || length(rightMaxes) < 1)
            continue
        end
        leftMax = leftMaxes(end);
        rightMax = rightMaxes(1);
        width = lambdaInt(rightMax) - lambdaInt(leftMax);
        set = [lambdaInt(leftMax)-percentageWidening*width, lambdaInt(i),... 
            lambdaInt(rightMax)+percentageWidening*width];
        resonances = [resonances set'];
    end
    
    resonance = struct([]);
    top = max(VInt);
    bottom = min(VInt);
    f = figure;
    plot(lambdaInt, VInt);
    names = {};
    for r = resonances
        rectangle('Position', [r(1) bottom r(3)-r(1) top-bottom], 'Curvature', [0.5, 0.5])
        name = num2eng(r(2));
        text(r(1), bottom, name, 'VerticalAlignment', 'top');
        names{end+1} = name;
    end
    
    if isempty(names)
        msgbox('No resonances were found');
    else
        [selection, ok] = listdlg('PromptString', 'Select a resonance:',...
            'SelectionMode', 'single',...
            'ListString', names)
        if ok
            resonance = struct('Left', resonances(1, selection),...
                'Center', resonances(2, selection),...
                'Right', resonances(3, selection));
        end
    end
end
