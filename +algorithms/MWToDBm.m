function PdBm = MWToDBm (PmW)
PdBm = 10.*log10(PmW);
