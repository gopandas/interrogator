function VSpectrum = CaptureSpectrum (lambdaGrid, laserSettings, daqSessionObj)
% 
% global max_time
%     %Turn on the laser if the laser is not already on
%         disp('testing Output state');
%         if ~CBMX_query_port_state(laserSettings.serialObject,laserSettings.Port); 
%             disp('Laser was off, turning on...');
%             CBMX_set_port_state(laserSettings.serialObject,laserSettings.Port,1);
%             pause(1);
%             wait_tuning(laserSettings.serialObject,laserSettings.Port,max_time);
%         else disp('Laser already on...');
%         end
        
    %Convert wavelength grid to frequency grid
        fGrid = algorithms.LambdaToF(lambdaGrid);
        
    %Mapping the frequency grid to Fine Tuning slots
        [fGridSlotted, fGridSlottedFineTuning, fFineTuningSlotsGridCenters]... 
            = algorithms.SlotTheGridForFineTuning (fGrid, laserSettings);
        if abs(max(max(fGridSlottedFineTuning)))>12e9; warning('Fine Tuning grid exceeds limits'); end
    %Tuning and measuring voltage loop
        h_w = waitbar(0, 'wait...');
        nn=1;
        VSpectrum = zeros(size(fGrid));
        T=~all(isnan(fGridSlottedFineTuning),1);
        T((T==0))=[];
        if length(T)==1 %Only Rough Tuning 
            for kk=1:size(fGridSlotted, 1)
                waitbar(kk/size(fGridSlotted, 1), h_w); drawnow;
                %set frequency
                %fGridSlotted(kk), the middle frequencies
                %    CBMX_set_port_frequ(laserSettings.serialObject,laserSettings.Port,1e-12.*fGridSlotted(kk));
                  algorithms.SetFreq(laserSetting.serialObj,fGridSlotted(kk));  %laser
                    pause(1);algorithms.wait_tuning(laserSettings.serialObject,laserSettings.Port,max_time);
                %read the voltage from DAQ 
                    data = daqSessionObj.startForeground;
                    VSpectrum(nn)=mean(data);nn=nn+1;
                fprintf('%i/%i:Wavelength: %4.3f nm\n', kk, size(fGridSlotted, 1), 1e9.*FToLambda(fGridSlotted(kk)));
            end
        else %Rough and Fine Tuning
            disp('Capturing spectrum with fine tuning');
            for kk=1:size(fGridSlotted, 2)
                waitbar(kk/size(fGridSlotted, 2), h_w); drawnow;
                if ~isnan(fGridSlotted(1,kk))
                    %set frequency to the center frequency of the optical mode
                        CBMX_set_port_config(laserSettings.serialObject,laserSettings.Port,... 
                            1e-12.*fGridSlotted(kk),round(1e-9.*fGridSlottedFineTuning(kk,1),3),...
                            laserSettings.Properties.conf.read(3),laserSettings.Properties.conf.read(4),...
                            laserSettings.Properties.conf.read(6));
                        fprintf('%.0f of %.0f : Wavelength: %4.3f nm\n', nn,... 
                            size(fGrid,2), 1e9.*algorithms.FToLambda(fGridSlotted(kk)+fGridSlottedFineTuning(kk,1)));
                        wait_tuning(laserSettings.serialObject,laserSettings.Port,max_time);
                        pause(2);
                    %read the voltage from DAQ 
                        data = daqSessionObj.startForeground;
                        VSpectrum(nn)=mean(data); nn=nn+1;
                    %fine-tune to all frequencies within the same mode
                    M=fGridSlottedFineTuning(kk,:);
                    for mm=2:length(M)
                        if ~isnan(M(mm))
                            %fine tune to mm-th frequency within the same optical mode
                            %how fr left or right with in a mode you are
                                
                            %CBMX_set_port_FTF(laserSettings.serialObject,laserSettings.Port,round(1e-9.*fGridSlottedFineTuning(kk,mm),3));
                             algorithms.SetFineFreq(obj,fGridSlottedFineTuning(kk,mm),3);  
                                fprintf('%.0f of %0.f : Wavelength: %4.3f nm\n', nn,... 
                                    size(fGrid,2), 1e9.*algorithms.FToLambda(fGridSlotted(kk)+fGridSlottedFineTuning(kk,mm)));
                                algorithms.wait_tuning(laserSettings.serialObject,laserSettings.Port,max_time);
                            %read the voltage from DAQ 
                                data = daqSessionObj.startForeground;
                                VSpectrum(nn)=mean(data); nn=nn+1;
                        elseif ~all(isnan(M(mm:end)))
                                warning('Fine Tuning Grid Error - not all tailing NaNs');
                        end
                    end
                end
            end
        end
        close (h_w);


