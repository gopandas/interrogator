function [] =  LaserOFF (ser_obj, Port)


    %turning off the laser
        CBMX_set_port_state(ser_obj,Port,0); % switch port off
        pause(0.5);
    % closing the laser port
        fclose(ser_obj);
