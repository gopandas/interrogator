function [indCropA, indCropB, indBot] = MakeMonotonous (lambdaRes, vRes)
%Returns indices between which the spectrum is monotonous
        %Find the bottom
            [botPks indBot] = findpeaks(-vRes, 'SortStr','descend','NPeaks',1);
            lambdaBottom = lambdaRes(indBot);
        %Trim the Right edge
        for kk=(indBot+floor(length(vRes)/50)):(length(vRes)-1)
            if vRes(kk+1)-vRes(kk)<0
               indCropB=kk; break
            end
        end
        %Trim the Left edge
        for kk=(indBot-floor(length(vRes)/50)):-1:1
            if vRes(kk+1)-vRes(kk)>0
               indCropA=kk; break
            end
        end
        vResTrimmed = vRes(indCropA:indCropB);
        lambdaResTrimmed = lambdaRes(indCropA:indCropB);
        h0= figure('PaperUnits', 'inches');pos = get (h0, 'PaperPosition');
        w = 3.5;set (h0, 'PaperPosition', [pos(1) pos(2) w pos(4)/pos(3)*w] );
        plot(1e9.*lambdaResTrimmed, 1e3.*vResTrimmed);
        grid on
        xlabel('Wavelength [nm]', 'FontSize', 12);ylabel('V [mV]', 'FontSize', 12);
        title('Monotonous part of the spectrum');