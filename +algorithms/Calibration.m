classdef Calibration 
    %CALIBRATION object
    
    properties (SetAccess = private)
        CalibrationFactorV
        % number that when multiplied by the measured sensor voltage [mV] to give the
        % measured voltage V [V]
        CalibrationFactorE
        % number that when multiplied by the measured sensor voltage [mV] to give the
        % measured electric field E [V/m]
        SensorObj
        % Sensor object used to get the calibration
        tCalibration
        vCalibration
        % t and V of the calibration measurement
        smallSignalGain
        % this value tells how much gain is added to the small signal
        % compared to the signal used to capture the spectrum
        operatingV % used to calculate the DC coupled data
        % DC_coupled value of the vcal used for mapping
        ACDCFlag % is the calibration signal AC coupled, 1 for AC, 0 for DC
        vPP % peak to peak calibration voltage
        vOffset % DC offset in calibration voltage
        lambdaPP % peak to peak mapped wavelength
        lambdaOffset % DC offset in mapped wavelength
        
    end
            
    methods
        function obj = Calibration(calV, calE, sensorObj, t, v, sGain, vDC, ...
                acdcFlag, vPP, v0, lambdaPP, lambda0)
            obj.CalibrationFactorV = calV;
            obj.CalibrationFactorE = calE;
            obj.SensorObj = sensorObj;
            obj.tCalibration = t;
            obj.vCalibration = v;
            obj.smallSignalGain = sGain;
            obj.operatingV = vDC;
            obj.vPP = vPP;
            obj.vOffset = v0;
            obj.lambdaPP = lambdaPP;
            obj.lambdaOffset = lambda0;
            obj.ACDCFlag = acdcFlag;
        end     
    end
    
end
