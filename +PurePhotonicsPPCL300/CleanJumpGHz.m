function fResponse = CleanJumpGHz(obj, f)

% N. Stan
% CleanJumpGHz load the GHz part of the next frequency, with x in units of 0.1GHz 
% 0.1*f - GHz part of the frequency (unsigned short)

if nargin == 1 
    WR = 0; f = 0;
elseif nargin == 2
    WR = 1;
end

commandRegister = 'EB'; %Register address for the command
data1 = commandRegister;
data23 = dec2hex(uint16(f), 4);
data2 = data23(1:2); % first data byte in hex
data3 = data23(3:4); % second data byte in hex


LstRsp = 0; % Bit set to logic 0 when the checksum is consistent.
% Bit set to logic 1 forces module to resend last valid packet. 
% Used when the checksum is inconsistent.

Bits26_25 = [0 0]; % set to zero by default

CE = 1; Status = 1;
while CE || Status

    data0_b = [[0 0 0 0] LstRsp Bits26_25 WR];
    data0 = binaryVectorToHex(data0_b);

    % Calculating BIP4 checksum
    BIP4 = PurePhotonicsPPCL300.BIP4 (data0, data1, data2, data3);
    data0(1) = binaryVectorToHex(BIP4);

    sendLine = uint8(hex2dec([data0; data1; data2; data3]));
    fwrite(obj.ser_obj,sendLine);


    %% Obtain response from the module
    pause(0.25);
    rxdata_dec = fread(obj.ser_obj,obj.ser_obj.BytesAvailable);
    rxdata_hex = dec2hex(rxdata_dec);
if size(rxdata_hex)~=[4 2]
    rxdata_hex
end
    data = [rxdata_hex(3,1:2) rxdata_hex(4,1:2)];
    % convert response to binary 
    rxdata_bin = hexToBinaryVector(dec2hex(rxdata_dec), 8);

    %% Analyze response

    %% Command specific bits
    % Channel number
    fResponse = hex2dec(data);


    %% Command independent bits
    % Communication Error
    CE = rxdata_bin(1, 5);
    if CE 
        fprintf ('\nCleanJumpGHz - Communication Error'); 
        LstRsp = 1;  continue;
    end

    % Pending Operation Flags
    pendingOperation = binaryVectorToHex(rxdata_bin(3, 1:8));
    % A series of eight flag bits indicating which operations, 
    % if any, are still pending. Each operation that becomes pending
    % is assigned one of these four bit positions. 
    % The module can be periodically polled (by reading the NOP register)
    % to determine which operations have completed. 
    % A value of 0x0 indicates that there are no currently pending operations.

    % Status Bits
    Status = binaryVectorToDecimal(rxdata_bin(1, 7:8));
    % 0x00 - OK flag, Normal return status
    % 0x01 - XE flag, (execution error)
    % 0x02 - AEA flag, (Automatic extended addressing result being returned or ready to write)
    % 0x03 - CP flag, Command not complete, pending
    if Status == 1
        fprintf('\nCleanJumpGHz - Execution Error - retrying...'); continue;
    end
end

