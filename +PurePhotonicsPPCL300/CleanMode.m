function modeResponse = CleanMode(obj, mode)

% N. Stan
% Register 0x90: switch between the different low-noise modes, 
% with 0 the standard dither-mode, 1 the no-dither mode and 2 the whisper-mode.

% The standard operating mode for the laser is the dither-mode. 
% In this mode all the control loops are running and creating noise 
% (especially in the 1-10000Hz range). By disabling these control- loops, 
% either completely or partially, a lower noise behavior can be achieved.
% In the no-dither mode, the FM dither is disabled. Most other control-loops remain enabled. 
% This results in the removal of the 888Hz tone (and its overtones),
% but does not address the noise below 100Hz. Note that this mode is not available 
% on the most recent versions of the firmware (8.0.9 and 8.6.0) as we now recommend 
% the whisper mode for all applications.
% In the whisper mode, essentially all control loops are disabled, to the extent possible. 
% This significantly reduced the AM and FM noise in the below 100Hz range. 
% If in any way possible, we recommend to once in a while switch back to 
% the dither mode for the laser to relock. Such a switch-back could be done 
% in less than 10 seconds.

if nargin == 1 
    WR = 0; mode = 0;
elseif nargin == 2
    WR = 1;
    assert (mode>=0&&mode<=2, 'Bad parameter passed to CleanMode')
end

commandRegister = '90'; %Register address for the command
data1 = commandRegister;
data23 = dec2hex(mode, 4);
data2 = data23(1:2); % first data byte in hex
data3 = data23(3:4); % second data byte in hex

LstRsp = 0; % Bit set to logic 0 when the checksum is consistent.
% Bit set to logic 1 forces module to resend last valid packet. 
% Used when the checksum is inconsistent.

Bits26_25 = [0 0]; % set to zero by default
CE = 1;
while CE
    data0_b = [[0 0 0 0] LstRsp Bits26_25 WR];
    data0 = binaryVectorToHex(data0_b);

    % Calculating BIP4 checksum
    BIP4 = PurePhotonicsPPCL300.BIP4 (data0, data1, data2, data3);
    data0(1) = binaryVectorToHex(BIP4);

    sendLine = uint8(hex2dec([data0; data1; data2; data3]));
    fwrite(obj.ser_obj,sendLine);


    %% Obtain response from the module
    pause(0.25);
    rxdata_dec = fread(obj.ser_obj,obj.ser_obj.BytesAvailable);
    rxdata_hex = dec2hex(rxdata_dec);
    data = [rxdata_hex(3,1:2) rxdata_hex(4,1:2)];
    % convert response to binary 
    rxdata_bin = hexToBinaryVector(dec2hex(rxdata_dec), 8);

    %% Analyze response

    %% Command specific bits

    % Mode
    modeResponse = hex2dec(data);



    %% Command independent bits

    % Communication Error
    CE = rxdata_bin(1, 5);
    if CE 
        fprintf ('\nCleanMode - Communication Error'); 
        LstRsp = 1; 
    end
end
% Status Bits
Status = binaryVectorToDecimal(rxdata_bin(1, 7:8));
% 0x00 - OK flag, Normal return status
% 0x01 - XE flag, (execution error)
% 0x02 - AEA flag, (Automatic extended addressing result being returned or ready to write)
% 0x03 - CP flag, Command not complete, pending


%% maybe add wait for pending operation to finish code