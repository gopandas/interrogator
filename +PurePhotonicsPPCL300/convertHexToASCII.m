function response = convertHexToASCII(wordHex)

% converts the hex values given 
% into an array of ascii characters and displays them

response = char(hex2dec(wordHex));
if size(response, 1)>size(response, 2)
    response = response';
end

disp(response');