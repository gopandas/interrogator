function [f0, fFTF] = matchToGridFTF (f, fMin, fMax, dfGrid, fFTFLim, INCDEC)
% matches the target frequency f to the frequency grid of the laser (fMin:dfGrid:fMax)
% making the FTF minimum if frequencies increment (INCDEC=1) or 
% making the FTF maximum if frequencies decrement (INCDEC=0) 


if f~=fMin && f~=fMax;
    fGrid=fMin:dfGrid:fMax;
    if INCDEC
        ind=find(fGrid<=f+fFTFLim, 1, 'last' );
    else
        ind=find(fGrid>=f-fFTFLim, 1 );
    end
    f0 = fGrid(ind);
    fFTF=f-f0;
else
    f0=f; fFTF=0;
end