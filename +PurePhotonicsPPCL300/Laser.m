classdef Laser <interfaces.ILaser
    %LASER Summary of this class goes here
    %   Detailed explanation goes here

    properties (Access=public)
        ser_obj=false
    end
    
    
    properties (Access=private)
        com_port
    end
    
    properties (Transient, SetAccess = protected)
        timer_handle = false
    end
    
    properties (Constant)
        max_time=60; %max time to wait after tuning [s]
        ptime=5; % pause time used after setting a value for power
        
        %Notes on USB com ports: if there are communication errors, change
        %the com port number to a random number to avoid system conflicts,
        %connect the laser directly to the mother board, don't connect
        %through a USB hub or front panel. Windows com port settings:
        %Baud rate: 112500,
        %data bits read and write to minimum: 64 bits,
        %latency minimum: 1 ms
        Port=[1 1 1]; % do not use wildcards here
        wvError=0.1; % [nm]
        fError=0.001;
        pwrError=0.5e-3;
    end
    
    methods
        function obj = Laser(port)
            %laser outputs [1527.60488 1567.13256] when queried
            obj.WavelengthMin = 1528e-9;
            obj.WavelengthMax = 1565e-9;
            obj.WavelengthStep = 1e-12; %1 pm
            %in dBm, the laser gives [6 16] dBm when queried
            %FTF limits setting actual: -12.000GHz to 12.000GHz
            obj.FTFlim = 30; % in GHz
            obj.Grid = 30; % in GHz
            obj.Powers = 1e-3.*(ceil(10^(6/10)):1e-1:round(10*10^(16/10))*0.1); %in W
            obj.com_port = port;
        end
    end
    
    methods (Access=protected)
        %these can be called only by this instance and its children(classes
        %that inherit from Laser object which inherits from ILaser)
        function Initialize(obj) %public methods cause other people will call these
            tic;
            fprintf('\nInitializing Pure Photonics PPCL300 ...');
            bdRate = 9600;
            obj.ser_obj=serial(obj.com_port,'BaudRate', bdRate);
            fopen(obj.ser_obj);
            
            % Setting Laser parameters
            % FTFlim
%                 obj.FTFlim = 1e-3*PurePhotonicsPPCL300.FTFR(obj); % in GHz
                obj.FTFlim = 5; % Bypassed the maximum FTF limit because of 
                % laser power nonlinearity at higher values 
                fprintf('\nFTF Limit = %3.2f GHz', obj.FTFlim);
            % Grid spacing
                obj.Grid = 1e-1*PurePhotonicsPPCL300.Grid(obj); % in GHz
                fprintf('\nGrid spacing = %3.2f GHz', obj.Grid); 
                lGridResponse = 1e-1*PurePhotonicsPPCL300.LGrid(obj);
                fprintf('\nMinimum Grid spacing = %3.2f GHz', lGridResponse); 
                if obj.Grid~=lGridResponse
                    PurePhotonicsPPCL300.ResEna(obj, 1, 0); %make sure laser out is off
                    PurePhotonicsPPCL300.Grid(obj, lGridResponse*10);
                    obj.Grid = 1e-1*PurePhotonicsPPCL300.Grid(obj); % in GHz
                    fprintf('\nGrid spacing changed to = %3.2f GHz', obj.Grid); 
                end
            % WavelengthMin
                f1 = PurePhotonicsPPCL300.LFH1(obj);
                f2 = PurePhotonicsPPCL300.LFH2(obj);
                f = f1*1e3 + f2/10;
                obj.WavelengthMin = algorithms.FToLambda(1e9*(f-2*obj.FTFlim)); 
                % (f-2*obj.FTFlim) is to create a margin around the extreme
                % values so that fine tuning doesn't step outside the range
                fprintf('\nMin Wavelength = %4.3f nm', 1e9*obj.WavelengthMin);
            % WavelengthMax
                f1 = PurePhotonicsPPCL300.LFL1(obj);
                f2 = PurePhotonicsPPCL300.LFL2(obj);
                f = f1*1e3 + f2/10;
                obj.WavelengthMax = algorithms.FToLambda(1e9*(f+2e-3*obj.FTFlim));
                % (f+2*obj.FTFlim) is to create a margin around the extreme
                % values so that fine tuning doesn't step outside the range
                fprintf('\nMax Wavelength = %4.3f nm', 1e9*obj.WavelengthMax);
            % Powers
                pLim(1)=PurePhotonicsPPCL300.OPSL(obj)/100;% in dBm
                pLim(2)=PurePhotonicsPPCL300.OPSH(obj)/100;% in dBm
                obj.Powers = ...
                    1e-3.*(ceil(10^(double(pLim(1))/10)):1e-1:round(10*10^(double(pLim(2))/10))*0.1); %in W

            % query laser until module is ready to be enabled
            fprintf('\nWaiting for module to become enable-ready ');
            MRDY = false; CE = false;
            while ~MRDY&&~CE 
                [MRDY, ~, CE, ~, ~] = ...
                    PurePhotonicsPPCL300.NOP(obj);
                pause(0.1);
                fprintf('.');
            end
            fprintf('\nTurning the Laser ON');
            
            % setting the laser to channel one
            PurePhotonicsPPCL300.Channel(obj, 1);
            
            % turning the laser ON
            PurePhotonicsPPCL300.ResEna(obj, 1, 1)
            
            fprintf('Laser ON');
            
%             % Turning laser LowNoise state off in case it was on
%             CoBriteDX1.CBMX_set_port_state(obj.ser_obj,obj.Port,1);
%             CoBriteDX1.wait_tuning(obj.ser_obj,obj.Port,obj.max_time);
%             CoBriteDX1.CBMX_set_port_dither(obj.ser_obj,obj.Port,1);
%             pause(0.5);
%             assert (logical(CoBriteDX1.CBMX_query_port_dither(obj.ser_obj,obj.Port)),...
%                 'Error: Dither on');
%             toc;
        end
        
        function ShutDown(obj)
            %switch the port off
            tic;
            fprintf('\nTurning the Laser OFF');
            PurePhotonicsPPCL300.ResEna(obj, 1, 0);
            fclose(obj.ser_obj); %close the laser port
            delete(obj.ser_obj); %delete serial object
            toc;
        end
        
        function wvRead = SetWavelength(obj, wavelength)
            fprintf('\nSetting Wavelength ...');
            
            % calculating frequencies
            % Target Frequency
            wavelengthNM = wavelength * 1e9;
            fTarget = 1e-9*algorithms.LambdaToF(wavelength); %f [GHz]
%             frough = f - obj.FTFlim;
            fprintf('\nTarget: lambda = %4.3f nm and f = %2.3f GHz',... 
                1e9*wavelength, fTarget);
            
            % Current Frequency
            f01 = PurePhotonicsPPCL300.LF1(obj);
            f02 = 1e-1*PurePhotonicsPPCL300.LF2(obj);
            f0 = f01*1e3 + f02;
            fprintf('\nCurrent LF: lambda = %4.3f nm and f = %2.3f GHz',... 
                algorithms.FToLambda(f0), f0);

            F = 1e-3*PurePhotonicsPPCL300.FTF(obj); % FTF in GHz
            f01fcf = PurePhotonicsPPCL300.FCF1(obj);
            f02fcf = 1e-1*PurePhotonicsPPCL300.FCF2(obj);
            fOpticalMode = f01fcf*1e3 + f02fcf;
            f0fcf = fOpticalMode + F;
            fprintf('\nCurrent FCF: lambda = %4.3f nm and f = %2.3f GHz (FTF: %2.3f GHz)',... 
                algorithms.FToLambda(f0fcf), fOpticalMode, F);

            f0fcfRounded = round(f0fcf, 1); 
            fprintf('\nCurrent FCF (rounded): lambda = %4.3f nm and f = %2.3f GHz',... 
                algorithms.FToLambda(f0fcfRounded), f0fcfRounded);

%             f0rough = f01*1e3 + f02;
%             f0 = f0rough + F;
%             deltafFTF = f - f0;
%             deltaf = frough - f0rough;
%             deltafGrid = ceil(deltaf/obj.Grid)*obj.Grid;
%             
%             fOffset = deltaf - deltafGrid;
%             assert (abs(fOffset)<=obj.FTFlim, 'Calculated FTF exceeds limits');
%             froughGrid = f0rough + deltafGrid; %new central frequency of the optical mode
%             fFTF = f - froughGrid; % this is the calculated ftf to be as big as possible 
%             fprintf('\ndelta f = %6.3f GHz, matched to the grid = %6.3f GHz, offset df = %6.3fGHz', ...
%                 deltaf, deltafGrid, fOffset);
%             
%             fprintf('\nInitial: lambda = %4.3f nm and f = %2.3f GHz (FTF = %6.3f GHz)',... 
%                 algorithms.FToLambda(f0), f0, F);
%             fprintf('\ndelta f = %6.3f GHz', deltaf);
          
%             %CleanJump frequency tuning
%                 f0new2 = 1e4*mod(f*1e-3,1); %0.1GHz part
%                 f0new1 = 1e-4*(f*10 - f0new2); %THz part
%                 PurePhotonicsPPCL300.CleanJumpGHz(obj, f0new2);
%                 PurePhotonicsPPCL300.CleanJumpTHz(obj, f0new1);
%                 PurePhotonicsPPCL300.CleanJumpEnable(obj);
            
            deltaf = fTarget - fOpticalMode;
            fprintf('\ndlambda = %4.3f nm and df = %2.3f GHz',... 
                algorithms.FToLambda(fTarget)-algorithms.FToLambda(fOpticalMode),...
                deltaf);
            % setting the frequency
            if deltaf==0; 
                fprintf('\nWavelength unchanged ...'); return; 
            elseif (abs(deltaf)<=obj.FTFlim)
                fprintf('\nFine tuning ...');
                PurePhotonicsPPCL300.FTF(obj, deltaf*1e3);
            elseif (abs(deltaf)>obj.FTFlim) 
                fprintf('\nCoarse Tuning ...');
                [f0Target, fFTFTarget] = ...
                    PurePhotonicsPPCL300.matchToGridFTF (fTarget, ...
                    algorithms.LambdaToF(1e9*obj.WavelengthMax), ... 
                    algorithms.LambdaToF(1e9*obj.WavelengthMin), ...
                    obj.Grid, obj.FTFlim, 0);
                
%                 fTarget = f;
%                 frough = fTarget-obj.FTFlim;
%                 deltafRough = frough-f0rough;
%                 deltafFTFGrid = ceil(deltafRough/obj.Grid)*obj.Grid;
%                 fTargetGrid = f0 + deltafFTFGrid;
%                 
%                 fFTFOffset = fTarget - fTargetGrid;
%                 assert (fFTFOffset<=obj.FTFlim, 'Calculated FTF exceeds limits');
                
                f0new2 = 1e4*mod(f0Target*1e-3,1);
                f0new1 = 1e-4*(f0Target*10 - f0new2);
                
                PurePhotonicsPPCL300.ResEna(obj, 1, 0); %laser OFF
                PurePhotonicsPPCL300.FTF(obj, 1e3.*(fFTFTarget));
                PurePhotonicsPPCL300.FCF1(obj, f0new1);
                PurePhotonicsPPCL300.FCF2(obj, f0new2);
                PurePhotonicsPPCL300.ResEna(obj, 1, 1); %laser ON
            end
            
            % Verifying the frequency
            f01 = PurePhotonicsPPCL300.LF1(obj);
            f02 = 1e-1*PurePhotonicsPPCL300.LF2(obj);
            f0 = f01*1e3 + f02;
            fprintf('\nCurrent LF: lambda = %4.3f nm and f = %2.3f GHz',... 
                algorithms.FToLambda(f0), f0);

            F = 1e-3*PurePhotonicsPPCL300.FTF(obj); % FTF in GHz
            f01fcf = PurePhotonicsPPCL300.FCF1(obj);
            f02fcf = 1e-1*PurePhotonicsPPCL300.FCF2(obj);
            fOpticalMode = f01fcf*1e3 + f02fcf;
            f0fcf = fOpticalMode + F;
            lambda0fcf = algorithms.FToLambda(f0fcf);
            fprintf('\nCurrent FCF: lambda = %4.3f nm and f = %2.3f GHz',... 
                algorithms.FToLambda(f0fcf), f0fcf);
            wvRead = lambda0fcf;
            f0fcfRounded = round(f0fcf, 1); 
            fprintf('\nCurrent FCF (rounded): lambda = %4.3f nm and f = %2.3f GHz',... 
                algorithms.FToLambda(f0fcfRounded), f0fcfRounded);
            
            fprintf('\nWavelength error: %3.3f pm', 1e3*abs(wavelengthNM-lambda0fcf));
            while abs(wavelengthNM-lambda0fcf)>obj.wvError
                fprintf('Port %d-%d-%d: Error! Wavelength error exceeded',obj.Port);
                obj.SetWavelength(wavelength);
            end
        end
        
        function wavelength = GetWavelength(obj)
            f01 = PurePhotonicsPPCL300.LF1(obj);
            f02 = PurePhotonicsPPCL300.LF2(obj);
            f0 = f01*1e3 + f02/10;
            df0 = PurePhotonicsPPCL300.FTF(obj);
            wavelength = algorithms.FToLambda(1e9*f0);
            fprintf('\nCurrent f: %2.3f GHz (FTF = %2.3f GHz)', f0, df0);
            fprintf('\nCurrent wavelength is %4.3f nm', 1e9*wavelength);
            
            f01 = PurePhotonicsPPCL300.LF1(obj);
            f02 = PurePhotonicsPPCL300.LF2(obj);
            f0 = f01*1e3 + f02/10;
            df0 = PurePhotonicsPPCL300.FTF(obj);
            wavelength = algorithms.FToLambda(1e9*f0);
            fprintf('\nCurrent f: %2.3f GHz (FTF = %2.3f GHz)', f0, df0);
            fprintf('\nCurrent wavelength is %4.3f nm', 1e9*wavelength);
        end
        
        function SetPower(obj, power)
            fprintf('\nSetting Power ...');
            power_mW = 1e3*power;
            powerDBm = 10*log10(power_mW);
            
            lim = [obj.Powers(1) obj.Powers(end)];
            assert(powerDBm>lim(1)||powerDBm<lim(2), 'Requested Power out of bounds');
            PurePhotonicsPPCL300.ResEna(obj, 1, 0); %laser software OFF
            PurePhotonicsPPCL300.PWR(obj, 100*powerDBm);
            PurePhotonicsPPCL300.ResEna(obj, 1, 1); %laser software ON
            pwrRead=1e-2*PurePhotonicsPPCL300.PWR(obj);
            fprintf('\nPower setting error = %2.6f mW', abs(10^(powerDBm/10)-10^(pwrRead/10)));
            assert(abs(10^(powerDBm/10)-10^(pwrRead/10))<1e3*obj.pwrError||pwrRead==-100, ...
                'Port %d-%d-%d: Error! Power setting/reading not working',obj.Port);
            fprintf('\nPower set to %2.3f mW ', 10^(pwrRead/10));
        end
        
        function power = GetPower(obj)
            powerResponse = PurePhotonicsPPCL300.PWR(obj);
            powerdBm = powerResponse/100;
            power = 1e-3*10^(powerdBm/10);
            fprintf('\nCurrent power is %4.3f mW', 1e3*power);
        end

        function ok = SetLowNoise(obj, sw)
            %turn dither off
            if sw==true
                fprintf('\nTurning Low Noise Mode on...');
                x = inputdlg('Timer setup: set Low-noise mode timeout [mins]:', 'Timeout', 1, {'5'});
                if ~isempty(x); 
                    obj.timer_handle = GUI.TimerGUI(str2double(x{:}));
                end
            else
                fprintf('\nTurning Low Noise Mode off...');
            end
            
            PurePhotonicsPPCL300.CleanMode(obj, 2*sw);
            modeRead = PurePhotonicsPPCL300.CleanMode(obj);
            assert(modeRead==2*sw,...
                'Dither status setting/reading error');
            ok = true;
            if sw==true
                fprintf('ON');
            else
                fprintf('OFF');
                if isvalid(obj.timer_handle)
                    delete(obj.timer_handle);
                end
            end
        end
    end
    
end
