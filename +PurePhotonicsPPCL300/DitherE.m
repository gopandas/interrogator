function [WFResponse, DDEResponse, CE, Status] = DitherE(obj, WF, DDE)

% N. Stan
% Dither Enable
% The dither registers provide a way to configure dither performance of the units.
% WF - Waveform : 00 - Sinusoidal; 01 - Triangular
% DDE - Digital DIther Enable : 0 - not enabled; 1 - enabled (confiugred through DitherF and DitherA).

if nargin == 1 
    WR = 0; WF = 0; DDE = 0;
elseif nargin == 3
    WR = 1;
end

commandRegister = '59'; %Register address for the command
data1 = commandRegister;
%conversion of power into a signed hex, 16 bits long
data2 = '00'; % first data byte in hex
data3 = binaryVectorToHex([0 0 decimalToBinaryVector(WF, 2) 0 0 DDE 0]); % second data byte in hex

LstRsp = 0; % Bit set to logic 0 when the checksum is consistent.
% Bit set to logic 1 forces module to resend last valid packet. 
% Used when the checksum is inconsistent.

Bits26_25 = [0 0]; % set to zero by default
CE = 1;
while CE
    data0_b = [[0 0 0 0] LstRsp Bits26_25 WR];
    data0 = binaryVectorToHex(data0_b);

    % Calculating BIP4 checksum
    BIP4 = PurePhotonicsPPCL300.BIP4 (data0, data1, data2, data3);
    data0(1) = binaryVectorToHex(BIP4);

    sendLine = uint8(hex2dec([data0; data1; data2; data3]));
    fwrite(obj.ser_obj,sendLine);


    %% Obtain response from the module
    pause(0.25);
    rxdata_dec = fread(obj.ser_obj,obj.ser_obj.BytesAvailable);
    rxdata_hex = dec2hex(rxdata_dec);
    data = [rxdata_hex(3,1:2) rxdata_hex(4,1:2)]
    % convert response to binary 
    rxdata_bin = hexToBinaryVector(dec2hex(rxdata_dec), 8);

    %% Analyze response

    %% Command specific bits

    % Channel number
    fResponse = hex2dec(data);

    WFResponse = binaryVectorToDecimal(rxdata_bin(3:4));
    DDEResponse = binaryVectorToDecimal(rxdata_bin(7));

    %% Command independent bits

    % Communication Error
    CE = rxdata_bin(1, 5);
    if CE 
        fprintf ('\nDitherE - Communication Error'); 
        LstRsp = 1; 
    end
end

% Status Bits
Status = binaryVectorToDecimal(rxdata_bin(1, 7:8));
% 0x00 - OK flag, Normal return status
% 0x01 - XE flag, (execution error)
% 0x02 - AEA flag, (Automatic extended addressing result being returned or ready to write)
% 0x03 - CP flag, Command not complete, pending

%% this needs to be changed, the status will most of the time be 3, because it's queried very soon after issuing the command


%% Final wait for the command to execute
if WR
    fprintf('\nDither ...');
    PurePhotonicsPPCL300.wait_operation(obj)
    switch DDE
        case true; fprintf('ON!\n');
        case false; fprintf('OFF!\n');
    end        
end

