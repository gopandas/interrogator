function [MRDY, errorField, pendingOperation, CE, Status] = NOP(obj)

% Nikola Stan stan.nikola@gmail.com
% The NOP register provides a way to access the module?s status, 
% returning pending operation status, and the current value of the error field. 
% This register may be read upon receiving an execution error for 
% an immediately preceding command. 
% It can also be polled to determine the status of pending operations.


commandRegister = '00'; %Register address for the NOP command
data1 = commandRegister;
data2 = '00'; % first data byte in hex
data3 = '00'; % second data byte in hex

LstRsp = 0; % Bit set to logic 0 when the checksum is consistent.
% Bit set to logic 1 forces module to resend last valid packet. 
% Used when the checksum is inconsistent.
Bits26_25 = [0 0]; % set to zero by default
WR = 0; % Read 0, Write 1
CE = 1;
while CE
    data0_b = [[0 0 0 0] LstRsp Bits26_25 WR];
    data0 = binaryVectorToHex(data0_b);

    % Calculating BIP4 checksum
    BIP4 = PurePhotonicsPPCL300.BIP4 (data0, data1, data2, data3);
    data0(1) = binaryVectorToHex(BIP4);

    % sending the formatted command to the laser 
    sendLine = uint8(hex2dec([data0; data1; data2; data3]));
    fwrite(obj.ser_obj,sendLine);
    LstRsp = 0;
    %% Obtain response from the module
    pause(0.25);
    if obj.ser_obj.BytesAvailable == 0 
        disp('zero Bytes Available'); LstRsp = 1; continue;
    end
    rxdata_dec = fread(obj.ser_obj,obj.ser_obj.BytesAvailable);

    % convert response to binary 
    rxdata_bin = hexToBinaryVector(dec2hex(rxdata_dec), 8);

    if size(rxdata_bin)~=[4 8]
        rxdata_bin
    end

    %% Analyze response


    %% Command independent bits

    % Communication Error
    CE = rxdata_bin(1, 5);
    if CE 
        fprintf ('\nNOP - Communication Error'); 
        LstRsp = 1; continue;
    end
    
    %% Command specific bits

    % Pending Operation Flags
    pendingOperation = binaryVectorToHex(rxdata_bin(3, 1:8));
    % A series of eight flag bits indicating which operations, 
    % if any, are still pending. Each operation that becomes pending
    % is assigned one of these four bit positions. 
    % The module can be periodically polled (by reading the NOP register)
    % to determine which operations have completed. 
    % A value of 0x0 indicates that there are no currently pending operations.


    % Module Ready Bit
    try 
        MRDY = rxdata_bin(4, 4);
    catch
        disp('');
    end
    % MRDY - Module Ready
    % When �1� indicates that the module is ready for its output 
    % to be enabled
    % When �0� indicates that the module is not ready for its output 
    % to be enabled.

    % Error Field Bits
    errorField = binaryVectorToDecimal(rxdata_bin(4, 5:8));
    % 0x00 OK - Ok, no errors
    % 0x01 RNI - The addressed register is not implemented
    % 0x02 RNW - Register not write-able; register cannot be written (read only)
    % 0x03 RVE - Register value range error; writing register contents causes value range error; contents unchanged
    % 0x04 CIP - Command ignored due to pending operation
    % 0x05 CII - Command ignored while module is initializing, warming up, or contains an invalid configuration.
    % 0x06 ERE - Extended address range error (address invalid)
    % 0x07 ERO - Extended address is read only
    % 0x08 EXF - Execution general failure
    % 0x09 CIE - Command ignored while module�s optical output is enabled (carrying traffic)
    % 0x0A IVC - Invalid configuration, command ignored
    % 0x0B-0x0E -- Reserved for future expansion
    % 0x0F VSE Vendor specific error (see vendor specific documentation for more information)

end

% Status Bits
Status = binaryVectorToDecimal(rxdata_bin(1, 7:8));
% 0x00 - OK flag, Normal return status
% 0x01 - XE flag, (execution error)
% 0x02 - AEA flag, (Automatic extended addressing result being returned or ready to write)
% 0x03 - CP flag, Command not complete, pending
