function channelResponse = Channel(obj, ch)

% N. Stan
% Channel sets the module�s channel.

if nargin == 1 
    WR = 0; ch = 0;
elseif nargin == 2
    WR = 1;
end

commandRegister = '30'; %Register address for the command
data1 = commandRegister;
data23 = dec2hex(ch, 4);
data2 = data23(1:2); % first data byte in hex
data3 = data23(3:4); % second data byte in hex

LstRsp = 0; % Bit set to logic 0 when the checksum is consistent.
% Bit set to logic 1 forces module to resend last valid packet. 
% Used when the checksum is inconsistent.

Bits26_25 = [0 0]; % set to zero by default
CE = 1;
while CE
    data0_b = [[0 0 0 0] LstRsp Bits26_25 WR];
    data0 = binaryVectorToHex(data0_b);

    % Calculating BIP4 checksum
    BIP4 = PurePhotonicsPPCL300.BIP4 (data0, data1, data2, data3);
    data0(1) = binaryVectorToHex(BIP4);

    sendLine = uint8(hex2dec([data0; data1; data2; data3]));
    fwrite(obj.ser_obj,sendLine);


    %% Obtain response from the module
    pause(0.25);
    rxdata_dec = fread(obj.ser_obj,obj.ser_obj.BytesAvailable);
    rxdata_hex = dec2hex(rxdata_dec);
    data = [rxdata_hex(3,1:2) rxdata_hex(4,1:2)]
    % convert response to binary 
    rxdata_bin = hexToBinaryVector(dec2hex(rxdata_dec), 8);

    %% Analyze response

    %% Command specific bits

    % Channel number
    channelResponse = hex2dec(data);



    %% Command independent bits

    % Communication Error
    CE = rxdata_bin(1, 5);
    if CE 
        fprintf ('\nChannel - Communication Error'); 
        LstRsp = 1; 
    end
end
% Status Bits
Status = binaryVectorToDecimal(rxdata_bin(1, 7:8));
% 0x00 - OK flag, Normal return status
% 0x01 - XE flag, (execution error)
% 0x02 - AEA flag, (Automatic extended addressing result being returned or ready to write)
% 0x03 - CP flag, Command not complete, pending

%% this needs to be changed, the status will most of the time be 3, because it's queried very soon after issuing the command


%% Final wait for the command to execute
fprintf('\nSetting the channel ...');
PurePhotonicsPPCL300.wait_operation(obj);
fprintf('SET!\n');


