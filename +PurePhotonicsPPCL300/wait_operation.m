function wait_operation(obj)
% D. Stahl 12.11.11
% waits until laser is settled or max_time [s] is elapsed

timer_start=now;
completeFlag = false;
pending0 = 0;
while ~completeFlag&&(((now-timer_start)*24*3600)<=obj.max_time)
    [~, ~, pending, CE, ~] = ...
        PurePhotonicsPPCL300.NOP(obj);
    if (pending0 ~= hex2dec(pending))||CE; 
        fprintf('.');
        pending0 = hex2dec(pending);
    end
    fprintf('.');
    if ~(hex2dec(pending)); completeFlag=true; end
    pause(0.25);
end
fprintf('\nTime elapsed: %3.2f s out of %3.2f s', ((now-timer_start)*24*3600), obj.max_time);

assert(((now-timer_start)*24*3600)<=obj.max_time ...
    || ((now-timer_start)*24*3600)>100*obj.max_time, ...
    ('Max waiting time exceeded!'));

