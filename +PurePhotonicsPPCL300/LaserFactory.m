classdef LaserFactory<interfaces.ILaserFactory
    %   LaserFactory Creates laser objects for PurePhotonicsPPCL300 laser
    %   Detailed explanation goes here
    
    properties (Constant)
        Name = 'PurePhotonicsPPCL300';
    end
    
    methods 
        function laser = Create(~)
            delete(instrfind); %first clean up before doing anything
            ports = PurePhotonicsPPCL300.getAvailableComPort();
            [idx, ok] = listdlg('PromptString', 'Select COM Port',...
                'SelectionMode', 'single', 'ListSize', [100 120],...
                'ListString', ports);
            if ~ok
                idx = 1;
                laser = [];
                return;
            end
            laser = PurePhotonicsPPCL300.Laser(ports(idx));
        end
    end
    
end
