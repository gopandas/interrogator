function [ filename ] = findfile( filter )
%FINDFILE Finds a file with some extension

persistent lastpath;

if lastpath~=0
    spec = [lastpath filter];
else
    spec = [];
end

if isempty(spec)
    [name, folder, ~] = uigetfile(filter);
else
    [name, folder, ~] = uigetfile(spec);
end


lastpath = folder;

filename = [folder name];

end

