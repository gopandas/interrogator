classdef ResultsWindow < handle
    %ResultsWindow opens a new window to display results

    properties (Constant)
         enlargedSubplotPosition = [185, 35, 400, 300];
    end
    
    properties (Transient, AbortSet)
        Units % Units for this panel
        Position % On parent in Units
        Parent % Figure, uitab, or uipanel
    end
    
    
    properties (SetObservable)
        Spectrum % ISpectrum object used for calibration
        Sensor % Sensor object to use
        Calibration % Calibration data object 
        Results % Results data object
        referenceLevelShift % adjustments to reference level via GUI
        referenceLevelShiftIncremental % increments of the shift
        leftEdgeSelection
        plotResultsWindowFlag = 0; % Flag that signals the time to plot results 
        Busy = false;
    end
    
    properties (Access = private)
%         infoPanel
        figureHandle % Figure Handle for the window
        
        zoomObj
        dcmObj
        brushObj
        
        dualEdgeCalibrationButton
        calibrationPreviewButton
        checkingCalibrationTurnedOff
        dualEdgingTurnedOff
        saveButton
        
        dataTipButton
        zoomButton
        zoomOutButton
        brushButton
        edgeButton
        revertButton

        hSubplots % Array of handles to 4 subplots
        hSubplotsAxes % Array of handles to data of 4 subplots
        calAxisHandle
        calPlotHandle
        subplotSelected
        subplotAxesProperties
        yMultiplier
        xMultiplier
        previousPosition

        ResultsNewRefLevel % Results data object with updated ref level     
        subplotTitles % all the titles of the subplots
    end
    
    events
        WindowChanged
        RequestRecalibration
    end
    
    methods
        function obj = ResultsWindow(varargin)
            parseInputs(obj,varargin{:});
            addlistener(obj, 'WindowChanged', @obj.updateWindow);
            addlistener(obj, 'referenceLevelShift', 'PostSet', @obj.applyReferenceLevelShift);
            addlistener(obj, 'RequestRecalibration', @obj.onRequestRecalibration);
        end

        function obj = plotResultsWindow(obj)
            obj.subplotSelected = 0;
            obj.previousPosition = {[160, 240, 200, 135], [420, 240, 200, 135], ...
                [160, 35, 200, 135], [420, 35, 200, 135], [170, 50, 400, 300]};
            obj.referenceLevelShift = [0 0 0 0];
            obj.referenceLevelShiftIncremental = [0 0 0 0];
            obj.leftEdgeSelection = [];
            obj.ResultsNewRefLevel.t = obj.Results.t;
            obj.ResultsNewRefLevel.v = obj.Results.v;
            obj.ResultsNewRefLevel.vLin = obj.Results.vLin;
            obj.ResultsNewRefLevel.vNonLin = obj.Results.vNonLin;
            obj.ResultsNewRefLevel.eLin = obj.Results.eLin;
            obj.ResultsNewRefLevel.eNonLin = obj.Results.eNonLin;
            
            obj.subplotTitles = {'Linear Calibration - V', ...
                'Nonlinear Calibration - V', 'Linear Calibration - E', ...
                'Nonlinear Calibration - E'};

            obj.buildWindow();
        end

    end

    
    methods (Access = private)
        
        function buildWindow(obj)
            WIDTH = 640;
            HEIGHT = 400;
            windowSize = [obj.Position(1), obj.Position(2), WIDTH, HEIGHT];
            
            if isempty(obj.figureHandle)||~isvalid(obj.figureHandle)
                f = figure('Parent', obj.Parent, 'Visible', 'on', ... 
                'Units', obj.Units, 'Position', windowSize, 'Resize', 'off', ...
                'MenuBar', 'none', 'IntegerHandle', 'off', 'Name', 'Results');
                obj.figureHandle = f;
            end
            
            % Getting the data and general units
            
%             obj.infoPanel = uipanel('Parent', obj.figureHandle,...
%                 'Title', 'Info Panel',...
%                 'Units', obj.Units,...
%                 'Position', [5 210 100 185]);

            obj.calibrationPreviewButton = uicontrol('Parent', obj.figureHandle,...
                'Style', 'togglebutton',...
                'String', 'Calibration Preview',...
                'Units', 'pixels',...
                'Position', [5, 95, 100, 30],...
                'Callback', @obj.onCalibrationPreviewButtonClicked);

            obj.dualEdgeCalibrationButton = uicontrol('Parent', obj.figureHandle,...
                'Style', 'togglebutton',...
                'String', '<html><div style="text-align:center">Dual Edge<br>Calibration',...
                'Units', 'pixels',...
                'Position', [5, 50, 100, 40],...
                'Callback', @obj.onDualEdgeCalibrationButtonClicked);

            obj.saveButton = uicontrol('Parent', obj.figureHandle,...
                'Style', 'pushbutton',...
                'String', '<html><div style="text-align:center">Save Changed<br>Data',...
                'Units', 'pixels',...
                'Position', [5, 5, 100, 40],...
                'Callback', @obj.onsaveButtonClicked);
            
            % Plot the subplots
            obj.hSubplots(1) = subplot(2, 2, 1);
            set(obj.hSubplots(1), 'Units', 'pixels',... 
                'Position', obj.previousPosition{1});
            [x, y, xUnitSuffix, yUnitSuffix] = obj.getSubPlotData (1);
            [obj.hSubplotsAxes(1), obj.xMultiplier(1), obj.yMultiplier(1)] = ...
                obj.plotLabeldResults(x, y, xUnitSuffix, yUnitSuffix);
            set(obj.hSubplots(1), 'ButtonDownFcn', @obj.onSubplotClicked);
            title(obj.subplotTitles{1}, 'FontSize', 9);
            
            obj.hSubplots(2) = subplot(2, 2, 2);
            set(obj.hSubplots(2), 'Units', 'pixels',... 
                'Position', obj.previousPosition{2});
            [x, y, xUnitSuffix, yUnitSuffix] = obj.getSubPlotData (2);
            [obj.hSubplotsAxes(2), obj.xMultiplier(2), obj.yMultiplier(2)] = ...
                obj.plotLabeldResults(x, y, xUnitSuffix, yUnitSuffix);
            set(obj.hSubplots(2), 'ButtonDownFcn', @obj.onSubplotClicked);
            title(obj.subplotTitles{2}, 'FontSize', 9);
            
            obj.hSubplots(3) = subplot(2, 2, 3);
            set(obj.hSubplots(3), 'Units', 'pixels',... 
                'Position', obj.previousPosition{3});
            [x, y, xUnitSuffix, yUnitSuffix] = obj.getSubPlotData (3);
            [obj.hSubplotsAxes(3), obj.xMultiplier(3), obj.yMultiplier(3)] = ...
                obj.plotLabeldResults(x, y, xUnitSuffix, yUnitSuffix);
            set(obj.hSubplots(3), 'ButtonDownFcn', @obj.onSubplotClicked);
            title(obj.subplotTitles{3}, 'FontSize', 9);
            
            obj.hSubplots(4) = subplot(2, 2, 4);
            set(obj.hSubplots(4), 'Units', 'pixels',... 
                'Position', obj.previousPosition{4});
            [x, y, xUnitSuffix, yUnitSuffix] = obj.getSubPlotData (4);
            [obj.hSubplotsAxes(4), obj.xMultiplier(4), obj.yMultiplier(4)] = ...
                obj.plotLabeldResults(x, y, xUnitSuffix, yUnitSuffix);
            set(obj.hSubplots(4), 'ButtonDownFcn', @obj.onSubplotClicked);
            title(obj.subplotTitles{4}, 'FontSize', 9);
            
            notify(obj, 'WindowChanged');
        end
        
        
        function updateWindow(obj,  ~, ~)
            % Performs steps for updating the window
            if isobject(obj.figureHandle)
                if obj.calibrationPreviewButton.Value
                    obj.dualEdgeCalibrationButton.Enable = 'off';                    
                    obj.saveButton.Enable = 'off';
                else
                    if obj.checkingCalibrationTurnedOff
                        obj.dualEdgeCalibrationButton.Enable = 'on';                    
                        obj.saveButton.Enable = 'on';
                        obj.checkingCalibrationTurnedOff = false;
                    end
                end
                
                if obj.dualEdgeCalibrationButton.Value
                    obj.calibrationPreviewButton.Enable = 'off';                    
                    obj.saveButton.Enable = 'off';
                else
                    if obj.dualEdgingTurnedOff
                        obj.calibrationPreviewButton.Enable = 'on';                    
                        obj.saveButton.Enable = 'on';
                        obj.dualEdgingTurnedOff = false;
                    end
                end
                if obj.subplotSelected>=1&&obj.subplotSelected<=4 ...
                    &&~obj.dualEdgeCalibrationButton.Value
                    obj.calibrationPreviewButton.Enable = 'off';
                    obj.dualEdgeCalibrationButton.Enable = 'off';
                elseif ~obj.subplotSelected
                    obj.calibrationPreviewButton.Enable = 'on';
                    obj.dualEdgeCalibrationButton.Enable = 'on';
                elseif obj.subplotSelected==5
                    obj.calibrationPreviewButton.Enable = 'on';
                    obj.dualEdgeCalibrationButton.Enable = 'off';
                end
            end
            obj.Busy = false;
        end
        
        function onSubplotClicked(obj, event, ~)
            if ~obj.subplotSelected
                obj.subplotSelected = obj.clickedSubplot(event);
                if ~obj.subplotSelected; return; end
                makeEnlarged = [0 0 0 0];
                makeEnlarged(obj.subplotSelected) = 1;
                for i = 1:4
                    if makeEnlarged(i)
                        % Enlarge the subplot
                        obj.previousPosition{i} = get(obj.hSubplots(i), 'Position');
                        set(obj.hSubplots(i), 'Position',  obj.enlargedSubplotPosition);
                    else
                        % Make invisible/unclickable
                        set(obj.hSubplots(i), 'HandleVisibility', 'off', 'Visible', 'off');
                        set(obj.hSubplotsAxes(i), 'HandleVisibility', 'off', 'Visible', 'off');
                    end
                end
                obj.ToolbarOn();
            else
                set(obj.hSubplots(obj.subplotSelected),...
                    'Position', obj.previousPosition{obj.subplotSelected});
                for i = 1:4
                    % Make all visible/clickable again
                    set(obj.hSubplots(i), 'HandleVisibility', 'on', 'Visible', 'on');
                    set(obj.hSubplotsAxes(i), 'HandleVisibility', 'on', 'Visible', 'on');
                end
                obj.ToolbarOff();
                obj.subplotSelected = 0;
            end
            notify(obj, 'WindowChanged');
        end
                
        function number = clickedSubplot(obj, event)
           switch event.Type
               case 'axes'
                   number = find(strcmp(event.Title.String, obj.subplotTitles)==1);
               case 'figure'
                   number = 0;
           end
        end
        
        function onCalibrationPreviewButtonClicked(obj, ~, ~)
            if obj.calibrationPreviewButton.Value
                obj.subplotSelected = 5;
                for i = 1:4
                        % Make invisible/unclickable
                        set(obj.hSubplots(i), 'HandleVisibility', 'off',...
                            'Visible', 'off', 'Position', obj.previousPosition{i});
                        set(obj.hSubplotsAxes(i), 'HandleVisibility', 'off', ...
                        'Visible', 'off');
                end
                obj.calAxisHandle = axes('Parent', obj.figureHandle,...
                    'Units', 'pixels',... 
                'Position', obj.previousPosition{5});
                obj.hSubplotsAxes(5) = obj.calAxisHandle;
                [x, y, xUnitSuffix, yUnitSuffix] = obj.getSubPlotData (5);
                [obj.calPlotHandle, obj.xMultiplier(5), obj.yMultiplier(5)] = ...
                    GUI.plotLabeldCal(x, y, xUnitSuffix, yUnitSuffix, obj.Calibration);
                obj.hSubplots(5) = get(obj.calAxisHandle, 'Parent');
                obj.checkingCalibrationTurnedOff = false;
                obj.ToolbarOn();
            else
                for i = 1:4
                    % Make all visible/clickable again
                    set(obj.hSubplots(i), 'HandleVisibility', 'on', 'Visible', 'on');
                    set(obj.hSubplotsAxes(i), 'HandleVisibility', 'on', 'Visible', 'on');
                    set(obj.hSubplots(i), 'ButtonDownFcn', @obj.onSubplotClicked);
                    set(obj.hSubplots(i), 'Position', obj.previousPosition{i});
                end
                obj.subplotSelected = 0;
                cla(obj.calAxisHandle);
                delete(allchild(obj.calAxisHandle))
                delete(obj.calAxisHandle)
                obj.checkingCalibrationTurnedOff = true;
                obj.toolButtonsOff([1 0 1 1]);
                obj.ToolbarOff();
            end
            notify(obj, 'WindowChanged');
        end
        
        function onDualEdgeCalibrationButtonClicked(obj, ~, ~)
            if obj.dualEdgeCalibrationButton.Value
                makeEnlarged = [1 0 0 0];
                obj.subplotSelected = 1;
                for i = 1:4
                    if makeEnlarged(i)
                        % Enlarge the subplot
                        obj.previousPosition{obj.subplotSelected} =...
                            get(obj.hSubplots(i), 'Position');
                        set(obj.hSubplots(i),... 
                            'Position', obj.enlargedSubplotPosition, ...
                            'ButtonDownFcn', '');
                        set(obj.hSubplots(i), 'HandleVisibility', 'on', 'Visible', 'on');
                        set(obj.hSubplotsAxes(i), 'HandleVisibility', 'on', 'Visible', 'on');                        
                    else
                        % Make invisible/unclickable
                        set(obj.hSubplots(i), 'HandleVisibility', 'off', 'Visible', 'off');
                        set(obj.hSubplotsAxes(i), 'HandleVisibility', 'off', 'Visible', 'off');
                    end
                end
                obj.dualEdgingTurnedOff = false;
                obj.ToolbarOn();
            else
                obj.toolButtonsOff([1 0 1 1]);
                obj.ToolbarOff();
                obj.dualEdgingTurnedOff = true;
                set(obj.hSubplots(1), 'Position', obj.previousPosition{1});
                for i = 1:4
                    % Make all visible/clickable again
                    set(obj.hSubplots(i), 'HandleVisibility', 'on', 'Visible', 'on');
                    set(obj.hSubplotsAxes(i), 'HandleVisibility', 'on', 'Visible', 'on');
                    set(obj.hSubplots(i), 'Position', obj.previousPosition{i});
                end
                set(obj.hSubplots(obj.subplotSelected), 'ButtonDownFcn', @obj.onSubplotClicked);
                obj.subplotSelected = 0;
                if ~isempty(obj.leftEdgeSelection)
                    notify(obj, 'RequestRecalibration');
                end
            end
            
            notify(obj, 'WindowChanged');    
        end

        function ToolbarOn (obj)
            zoomIconImgData = imread('./+GUI/zoom_icon.png');
            obj.zoomButton = uicontrol('Parent', obj.figureHandle,...
                'Style', 'togglebutton',...
                'CData', zoomIconImgData,...
                'Units', 'pixels',...
                'Position', [170, 365, 30, 30],...
                'Callback', @obj.onZoomButtonClicked);
            
            zoomoutIconImgData = imread('./+GUI/zoomout_icon.png');
            obj.zoomOutButton = uicontrol('Parent', obj.figureHandle,...
                'Style', 'pushbutton',...
                'CData', zoomoutIconImgData,...
                'Units', 'pixels',...
                'TooltipString', 'Zoom-out to see all data',...
                'Position', [210, 365, 30, 30],...
                'Callback', @obj.onZoomOutButtonClicked);

            dataTipIconImgData = imread('./+GUI/dataTip_icon.png');
            obj.dataTipButton = uicontrol('Parent', obj.figureHandle,...
                'Style', 'togglebutton',...
                'CData', dataTipIconImgData,...
                'Units', 'pixels',...
                'Position', [250, 365, 30, 30],...
                'Callback', @obj.onDataTipButtonClicked);
            
            if ~obj.calibrationPreviewButton.Value
                if obj.dualEdgeCalibrationButton.Value %Dual Edge Calibration
                    brushIconImgData = imread('./+GUI/edge_lr_icon.png');
                    obj.edgeButton = uicontrol('Parent', obj.figureHandle,...
                        'Style', 'togglebutton',...
                        'CData', brushIconImgData,...
                        'Units', 'pixels',...
                        'Position', [290, 365, 30, 30],...
                        'Callback', @obj.onEdgeButtonClicked);
                else %Reference Level Setting
                    brushIconImgData = imread('./+GUI/brush_icon.png');
                    obj.brushButton = uicontrol('Parent', obj.figureHandle,...
                        'Style', 'togglebutton',...
                        'CData', brushIconImgData,...
                        'Units', 'pixels',...
                        'Position', [290, 365, 30, 30],...
                        'Callback', @obj.onBrushButtonClicked);

                    dataRevertIconImgData = imread('./+GUI/revert_icon.png');
                    obj.revertButton = uicontrol('Parent', obj.figureHandle,...
                        'Style', 'pushbutton',...
                        'CData', dataRevertIconImgData,...
                        'Units', 'pixels',...
                        'TooltipString', 'Revert the changes made to the reference level',...
                        'Position', [330, 365, 30, 30],...
                        'Callback', @obj.onRevertButtonClicked);
                end
            end

            
            notify(obj, 'WindowChanged');
        end
        
        function ToolbarOff (obj)
            if isobject (obj.zoomButton); delete(obj.zoomButton); obj.zoomButton = []; end
            if isobject (obj.zoomOutButton); delete(obj.zoomOutButton); obj.zoomOutButton = []; end
            if isobject (obj.brushButton); delete(obj.brushButton); obj.brushButton = []; end
            if isobject (obj.edgeButton); delete(obj.edgeButton); obj.edgeButton = []; end
            if isobject (obj.dataTipButton); delete(obj.dataTipButton); obj.dataTipButton = []; end
            if isobject (obj.revertButton); delete(obj.revertButton); obj.revertButton = []; end
            delete(findall(gca, 'Type', 'hggroup', 'HandleVisibility','off'));
            obj.zoomObj = [];
            obj.dcmObj = [];
            delete(obj.brushObj);
            obj.brushObj = [];
        end
        
        function onZoomButtonClicked(obj, ~, ~)
            if obj.zoomButton.Value
                % then turn on zoom
                obj.zoomObj = zoom; 
                set (obj.zoomObj, 'Enable', 'on');
                set(obj.hSubplots(obj.subplotSelected), 'HitTest', 'on');
                % turn off other tools
                obj.toolButtonsOff([0 0 1 1 0]);
            else
                obj.toolButtonsOff([1 0 1 1 0]);
            end
        end

        function onZoomOutButtonClicked(obj, ~, ~)
            obj.toolButtonsOff([1 0 1 1 0]);
            axis tight;
            [~, y, ~, ~] = obj.getSubPlotData (obj.subplotSelected);
            proxy = y./obj.yMultiplier(obj.subplotSelected); 
            margin1=0.1; margin2=0.1; 
            ylim([min(proxy)-margin1.*(max(proxy) - min(proxy)) ...
                max(proxy)+margin2.*(max(proxy) - min(proxy))]);
        end
        
        function onDataTipButtonClicked(obj, ~, ~)
            if obj.dataTipButton.Value
                % turn off other tools buttons
                obj.toolButtonsOff([1 0 0 1 0]);
                % then turn on datacursormode
                obj.dcmObj = datacursormode; 
                set (obj.dcmObj, 'Enable', 'on',...
                    'UpdateFcn', @obj.DataTipFunction, 'UIContextMenu', []);
                if obj.subplotSelected %disable clickability of the underlying graph
                    set(obj.hSubplots(obj.subplotSelected), 'HitTest', 'off');
                end
            else
                obj.toolButtonsOff([1 0 1 1 0]);
                set(obj.hSubplots(obj.subplotSelected), 'HitTest', 'on');
            end
        end
        
        function onBrushButtonClicked(obj, ~, ~)
            if obj.brushButton.Value
                % turn off other tools buttons
                obj.toolButtonsOff([1 0 1 0 0]);
                % then turn on brush mode
                obj.brushObj = brush;
                set(obj.brushObj, 'Enable', 'on',...
                    'ActionPostCallback', @obj.postBrushReferenceLevelFcn1);
            else
                obj.toolButtonsOff([1 0 1 1 0]);
            end
        end
        
        function onEdgeButtonClicked(obj, ~, ~)
            if obj.edgeButton.Value
                % turn off other tools buttons
                obj.toolButtonsOff([1 0 1 0 0]);
                % turn on brush mode
                obj.brushObj = brush;
                set(obj.brushObj, 'Enable', 'on',...
                    'ActionPostCallback', @obj.postBrushReferenceLevelFcn2);
            else
                obj.toolButtonsOff([1 0 1 1 0]);
            end
        end

        function onRevertButtonClicked(obj, ~, ~)
            obj.toolButtonsOff([1 0 1 1 0]);
            obj.referenceLevelShift = [0 0 0 0];
            axis tight;
            [~, y, ~, ~] = obj.getSubPlotData (obj.subplotSelected);
            proxy = y./obj.yMultiplier(obj.subplotSelected); 
            margin1=0.1; margin2=0.1; 
            ylim([min(proxy)-margin1.*(max(proxy) - min(proxy)) ...
                max(proxy)+margin2.*(max(proxy) - min(proxy))]);
        end
        
        function toolButtonsOff(obj, n)
            % n = 1 Zoom button
            % n = 2 DataTip Button
            % n = 3 Selection Button
            for i = 1:length(n)
                if(n(i))
                    [buttonObj, modeObj] = obj.getToolButtonObj(i);
                    if ~isempty(buttonObj);
                        if isvalid(buttonObj)
                            buttonObj.Value = 0;
                        end
                    end
                    if ~isempty(modeObj);
                        if isvalid(modeObj)
                            modeObj.Enable = 'off';
                        end
                    end
                end
            end
        end
        
        function [buttonObj, modeObj] = getToolButtonObj(obj, n)
            % n = 1 Zoom button
            % n = 2 DataTip Button
            % n = 3 Selection Button
            switch n
                case 1
                    buttonObj = obj.zoomButton;
                    modeObj = obj.zoomObj;
                case 2
                    buttonObj = obj.zoomOutButton;
                    modeObj = [];
                case 3
                    buttonObj = obj.dataTipButton;
                    modeObj = obj.dcmObj;
                case 4
                    if obj.dualEdgeCalibrationButton.Value
                        buttonObj = obj.edgeButton;
                        modeObj = obj.brushObj;
                    else
                        buttonObj = obj.brushButton;
                        modeObj = obj.brushObj;
                    end
                case 5
                    buttonObj = obj.revertButton;
                    modeObj = [];
                otherwise
                    buttonObj = [];
                    modeObj = [];
            end
        end
        
        
        function applyReferenceLevelShift(obj, ~, ~)
            obj.Busy = true;            
            switch obj.subplotSelected
                case 1
                    obj.ResultsNewRefLevel.vLin = ...
                        obj.Results.vLin - obj.referenceLevelShift(1);
                case 2
                    obj.ResultsNewRefLevel.vNonLin = ...
                        obj.Results.vNonLin - obj.referenceLevelShift(2);
                case 3
                    obj.ResultsNewRefLevel.eLin = ...
                        obj.Results.eLin - obj.referenceLevelShift(3);
                case 4
                    obj.ResultsNewRefLevel.eNonLin = ...
                        obj.Results.eNonLin - obj.referenceLevelShift(4);
                otherwise
                    return;
            end
            axes(obj.hSubplots(obj.subplotSelected));
            currentAxis = gca;
            previousXLim = currentAxis.XLim; 
            previousYLim = currentAxis.YLim;
            previousYMultiplier = obj.yMultiplier(obj.subplotSelected);
            previousXMultiplier = obj.xMultiplier(obj.subplotSelected);
            [x, y, xUnitSuffix, yUnitSuffix] = obj.getSubPlotData (obj.subplotSelected);
            [obj.hSubplotsAxes(obj.subplotSelected), ...
                obj.xMultiplier(obj.subplotSelected), obj.yMultiplier(obj.subplotSelected)] = ...
                obj.plotLabeldResults(x, y, xUnitSuffix, yUnitSuffix);
            set(gca, 'XLim', previousXLim...
                .*(previousXMultiplier/obj.xMultiplier(obj.subplotSelected))); 
            set(gca, 'YLim', (previousYLim ...
                - obj.referenceLevelShiftIncremental(obj.subplotSelected)/previousYMultiplier)...
                .*(previousYMultiplier/obj.yMultiplier(obj.subplotSelected)));
            set(obj.hSubplots(obj.subplotSelected), 'ButtonDownFcn', @obj.onSubplotClicked);
            title(obj.subplotTitles{obj.subplotSelected}, 'FontSize', 9);
            obj.Busy = false;
        end

        
        function onsaveButtonClicked(obj, ~, ~)
            fileName = obj.Results.filename;
            t = obj.ResultsNewRefLevel.t;  %#ok<NASGU>
            v = obj.ResultsNewRefLevel.v; %#ok<NASGU>
            vLin = obj.ResultsNewRefLevel.vLin; %#ok<NASGU>
            vNonLin = obj.ResultsNewRefLevel.vNonLin; %#ok<NASGU>
            eLin = obj.ResultsNewRefLevel.eLin; %#ok<NASGU>
            eNonLin = obj.ResultsNewRefLevel.eNonLin; %#ok<NASGU>
            newFolder = '.\dataFiles\';
            if ~exist(newFolder, 'dir'); mkdir(newFolder); end
            save([newFolder fileName datestr(now, 'yy-mm-dd_HH-MM-SS') '_calibrated.mat'],...
                't', 'eLin', 'vLin', 'eNonLin', 'vNonLin');
        end

        function output_txt = DataTipFunction(~, dataTip, event_obj)
        % Display the position of the data cursor
        % obj          Currently not used (empty)
        % event_obj    Handle to event object
        % output_txt   Data cursor text string (string or cell array of strings).

            pos = get(event_obj,'Position');
            output_txt = {['t: ',num2str(pos(1),4)],...
                ['V: ',num2str(pos(2),4)]};
            dataTip.FontSize = 9;
        end

        function postBrushReferenceLevelFcn1(obj, ~, event_data)
            brushdata = event_data.Axes.Children.BrushHandles.Children(1).VertexData;
            event_data.Axes.Children.BrushHandles.Children(1).FaceColorData = ...
                uint8(255.*[1;0;0;0.3]);
            
            if ~isempty(brushdata)
                obj.referenceLevelShiftIncremental(obj.subplotSelected) = ...
                    mean(brushdata(2, :))*obj.yMultiplier(obj.subplotSelected);
                obj.referenceLevelShift(obj.subplotSelected) = ...
                    obj.referenceLevelShift(obj.subplotSelected) + ...
                    obj.referenceLevelShiftIncremental(obj.subplotSelected);
            else
                return;
            end
        end
    
        function postBrushReferenceLevelFcn2(obj, ~, event_data)
            obj.leftEdgeSelection = ...
                event_data.Axes.Children.BrushHandles.Children(1).VertexData;
            event_data.Axes.Children.BrushHandles.Children(1).FaceColorData = ...
                uint8(255.*[1;0;0;0.3]);
        end
        
        function onRequestRecalibration(obj, ~, ~)
            obj.Busy = true;
            % Find selected indices 
             indices(1)=find(obj.ResultsNewRefLevel.t>=...
                    obj.leftEdgeSelection(1,1).*obj.xMultiplier(1), 1 );
                indices(2)=find(obj.ResultsNewRefLevel.t<=...
                    obj.leftEdgeSelection(1,end).*obj.xMultiplier(1), 1, 'last' );
            ind = indices(1):1:indices(2); 
            
            % form a 1-vector with zeros at selected points
            rightEdgeMapping = ones(length(obj.ResultsNewRefLevel.t),1); 
            rightEdgeMapping(ind) = 0;
            
            % Re-calibration
            obj.Results = obj.Spectrum.ProcessData(...
                obj.Results.t, obj.Results.v, ...
                obj.Results.filename, obj.Sensor, rightEdgeMapping); 
            
            obj.plotResultsWindowFlag = 1; % signal to replot the results window
            obj.Busy = false;
        end
        
        function [axisHandle, xMult, yMult] = plotLabeldResults(~, x, y, xUnitSuffix, yUnitSuffix)
            
            % General magnitudes setup
            yUnitPrefix = {'p', 'n', 'u', 'm', '', 'k', 'M', 'G', 'T'};
            xUnitPrefix = {'p', 'n', 'u', 'm', ''};
            ymultiplier = [1e-12 1e-9 1e-6 1e-3 1 1e3 1e6 1e9 1e12]; 
            xmultiplier = [1e-12 1e-9 1e-6 1e-3 1];
            
            % Determining which units to use
            xAvg = max (abs(x)); 
            xUnitIndex = find(xAvg<=1e3.*xmultiplier, 1);
            xMult = xmultiplier(xUnitIndex);
            xUnit = strcat(xUnitPrefix(xUnitIndex), xUnitSuffix);

            yAvg = max (abs(y)); 
            yUnitIndex = find(yAvg<=1e3.*ymultiplier, 1);
            yMult = ymultiplier(yUnitIndex); 
            yUnit = strcat(yUnitPrefix(yUnitIndex), yUnitSuffix);
            
            % Plotting 
            axisHandle = plot (x./xMult, y./yMult);
            grid on
            axis tight;
            proxy = y./yMult; margin1=0.1; margin2=0.1; 
            if (min(proxy)~=max(proxy))
                ylim([min(proxy)-margin1.*(max(proxy) - min(proxy)) ...
                    max(proxy)+margin2.*(max(proxy) - min(proxy))]);
            end
            xlabel(['Time [' xUnit{1} ']'], 'FontSize', 9);
            ylabel(['Voltage [' yUnit{1} ']'], 'FontSize', 9);
            set (gca, 'FontSize', 7);
        end

        function [x, y, xUnitSuffix, yUnitSuffix] = getSubPlotData (obj, nSubPlot)
            x = obj.ResultsNewRefLevel.t; 
            switch nSubPlot
                case 1
                    y = obj.ResultsNewRefLevel.vLin; 
                    yUnitSuffix = 'V'; 
                case 2
                    y = obj.ResultsNewRefLevel.vNonLin;
                    yUnitSuffix = 'V'; 
                case 3
                    y = obj.ResultsNewRefLevel.eLin;
                    yUnitSuffix = 'V/m';
                case 4
                    y = obj.ResultsNewRefLevel.eNonLin;
                    yUnitSuffix = 'V/m'; 
                case 5
                    x = obj.Calibration.tCalibration;
                    y = obj.Calibration.vCalibration;        
                    yUnitSuffix = 'V'; 
            end
            xUnitSuffix = 's'; 
        end
        
        function parseInputs(obj, varargin)
            % Parses inputs
            
            parser = inputParser;
            parser.FunctionName = 'ColorPanel';
            
            addParameter(parser, 'Parent', get(groot, 'CurrentFigure'))
            addParameter(parser, 'Position', get(groot, 'DefaultUicontrolPosition'))
            addParameter(parser, 'Units', get(groot, 'DefaultUIcontrolUnits'))
            addParameter(parser, 'Spectrum', [])
            
            parse(parser, varargin{:});
            
            if isempty(parser.Results.Parent)
                parent = gcf; % Use current figure for parent if none specified
            else
                parent = parser.Results.Parent;
            end
            
            obj.Parent = parent;
            obj.Units = parser.Results.Units;
            obj.Position = parser.Results.Position;
            obj.Spectrum = parser.Results.Spectrum;
        end
        
    end
    
end