classdef SensorPanel < handle
    %SENSORPANEL Panel for selecting and displaying the sensor info
    %   UI Control for selecting a sensor
    
    properties (Constant)
        sensorFileName = 'sensors.txt';
        % name of the text file where a list of sensors can be provided
        % in the format name, eShiftRate on each line, for example:
        % SCOS1, 50
        % SCOS2, 45
        % SCOS3, 350
    end

    
    properties (Transient, AbortSet)
        Units % Units for this panel
        Position % On parent in Units
        Parent % Figure, uitab, or uipanel
    end
    
    properties (Transient, SetObservable)
        Sensor % Sensor object to use
        sensors % sensors list read from the file
        Busy = false;
    end
    
    properties (Transient, Access = private, SetObservable)
        panel
        sensorNamePopup
        sensorNameEdit
        sensorEShiftEdit
        NameEditContentLength = 0;
        EShiftEditContentLength = 0;
        setButton
        deleteButton
    end
    
    events
        PanelChanged
        SensorPopupChanged
        NonEmptyFields
        EmptyFields
    end
    
    methods
        function obj = SensorPanel(varargin)
            
            parseInputs(obj,varargin{:});
            
            obj.readSensorsFromFile(); % read list of sensors from sensors.txt
            obj.Sensor = obj.sensors(1); % set the current sensor to user custom          
            
            obj.buildPanel();
            
            addlistener(obj.panel, 'ObjectBeingDestroyed', @(~,~)obj.delete);
            addlistener(obj, 'PanelChanged', @obj.updatePanel);
            addlistener(obj, 'SensorPopupChanged', @obj.onSensorNamePopupSelectionChanged);
            addlistener(obj, 'Sensor', 'PostSet', @obj.updatePanel);
            addlistener(obj, 'NonEmptyFields', @obj.enableSetButton);
            addlistener(obj, 'EmptyFields', @obj.disableSetButton);


            
            notify(obj, 'PanelChanged'); % make sure everything is up to date
        end
        
        function set.Parent(obj, newParent)
            % Validate/Set/Update
            assert(ishghandle(newParent),'New Parent is expected to be a valid handle to a figure, uipanel, or uitab')
            assert(ismember(newParent.Type,{'figure','uipanel','uitab'}),'New Parent is expected to be a valid handle to a figure, uipanel, or uitab')
            obj.Parent = newParent;
            
            % Notify
            notify(obj,'PanelChanged');
        end
        
        function set.Sensor(obj, sensor)
            obj.Sensor = sensor;            
        end
        
    end
    
    
    methods (Access = private)
        
        function buildPanel(obj)
            % Builds our panel
            
            obj.panel = uipanel('Parent', obj.Parent,...
                'Title', 'Sensor',...
                'Units', obj.Units,...
                'Position', obj.Position);
            
            names = arrayfun(@(f) f.Name, obj.sensors, 'un', 0);
            
            obj.sensorNamePopup = uicontrol('Parent', obj.panel,...
                'Style', 'popup',...
                'String', names,...
                'Units', 'pixels',...
                'TooltipString', 'Choose a sensor',...
                'Position', [20, 50, 260, 30], ...
                'Callback', @obj.onSensorNamePopupSelectionChanged);
            uicontrol('Parent', obj.panel,...
                'Style', 'text',...
                'String', 'Name:',...
                'HorizontalAlignment', 'right',...
                'Units', 'pixels',...
                'Position', [20, 35, 45, 20], ...
                'ButtonDownFcn', @obj.onSensorPanelDoubleClick);
            obj.sensorNameEdit = uicontrol('Parent', obj.panel,...
                'Style', 'edit',...
                'Units', 'pixels',...
                'HorizontalAlignment', 'left',...
                'Position', [65, 35, 85, 20],...
                'KeyPressFcn', @obj.keyPressedName,...
                'TooltipString', 'Enter the name of the new sensor');
            uicontrol('Parent', obj.panel,...
                'Style', 'text',...
                'String', 'E-shift:',...
                'HorizontalAlignment', 'right',...
                'Units', 'pixels',...
                'Position', [150, 35, 85, 20], ...
                'ButtonDownFcn', @obj.onSensorPanelDoubleClick);
            obj.sensorEShiftEdit = uicontrol('Parent', obj.panel,...
                'Style', 'edit',...
                'Units', 'pixels',...
                'HorizontalAlignment', 'left',...
                'Position', [235, 35, 45, 20],...
                'KeyPressFcn', @obj.keyPressedEShift,...
                'TooltipString', 'Enter the E-shift value in pm/(MV/m)');
            obj.setButton = uicontrol('Parent', obj.panel,...
                'Style', 'pushbutton',...
                'String', 'Set',...
                'Units', 'pixels',...
                'TooltipString', 'Use the entered information as a new sensor',...
                'Position', [150, 5, 130, 30],...
                'Callback', @obj.onSetClicked);
            obj.deleteButton = uicontrol('Parent', obj.panel,...
                'Style', 'pushbutton',...
                'String', 'Delete',...
                'Units', 'pixels',...
                'TooltipString', 'Delete the selected sensor',...
                'Position', [20, 5, 130, 30],...
                'Callback', @obj.onDeleteClicked);
        end
        
        function onSensorPanelDoubleClick(obj, ~, ~, ~)
            persistent chk
            if isempty (chk)
                chk = 1;
                pause (0.5); 
                %this delay distinguishes between single and double clicks
                if chk == 1
                    chk = []; return;
                end
            else
                chk = [];
                obj.refreshSensorList(0);
            end
        end
        
        function onSetClicked(obj, ~, ~, ~)
            name = obj.sensorNameEdit.String;
            eShift = str2double(obj.sensorEShiftEdit.String);
            if isnan(eShift) 
                warndlg('Only numeric values accepted for E-shift');
                return;
            end
            sensor = interfaces.Sensor(name, eShift);
            
            noMatch = true;
            for i=1:length(obj.sensors)
                noMatch = noMatch&&(~strcmp(name, obj.sensors(i).Name));
            end
            if noMatch % new sensor name is not a duplicate
                obj.appendSensorToFile(sensor);
                obj.refreshSensorList(1);
            else
                warndlg('Unallowed sensor name. Either a duplicate or illegal name.');
            end
        end
        
        function onDeleteClicked(obj, ~, ~, ~)
            obj.sensors(obj.sensorNamePopup.Value) = [];
            obj.sensorNamePopup.Value = 1;
            notify(obj, 'SensorPopupChanged');
            
            obj.overwriteSensorFileWithCurrentSensors();
            obj.refreshSensorList(0);
        end
        
        function onSensorNamePopupSelectionChanged(obj, ~, ~, ~)
            obj.Sensor = obj.sensors(obj.sensorNamePopup.Value);
        end
        
        function updatePanel(obj, ~, ~)
            % Performs steps for updating the panel
            
            obj.panel.Position = obj.Position;
            obj.panel.Units = obj.Units;
            obj.panel.Parent = obj.Parent;
            
            if obj.sensorNamePopup.Value==1
                % value one is always the dummy sensor reserved for adding
                % new sensor entry
                obj.sensorNameEdit.String = '';
                obj.sensorEShiftEdit.String = '';
                obj.sensorNameEdit.Enable = 'on';
                obj.sensorEShiftEdit.Enable = 'on';
                obj.deleteButton.Enable = 'off';
                obj.disableSetButton(obj);
            else
                obj.sensorNameEdit.String = obj.Sensor.Name;
                obj.sensorEShiftEdit.String = num2str(obj.Sensor.EShift);
                obj.sensorNameEdit.Enable = 'off';
                obj.sensorEShiftEdit.Enable = 'off';
                obj.deleteButton.Enable = 'on';
                obj.setButton.Enable = 'off';
            end
            obj.Busy = false;            
        end
        
        function readSensorsFromFile(obj, ~)
            fID = fopen(obj.sensorFileName, 'rt');
            data = textscan(fID, '%s %s', 'Delimiter', ',');
            if ~feof(fID) 
                disp('error reading sensor file');
            end
            fclose(fID);
            s(1)=interfaces.Sensor('Add New Sensor', 0); 
            % first sensor is always user customizable
            for i = 1:size(data{1}, 1)
                s(i+1)=interfaces.Sensor(data{1,1}{i}, str2num(data{1, 2}{i}));                
            end
            obj.sensors = s;
        end
        
        function appendSensorToFile(obj, sensor)
            fID = fopen(obj.sensorFileName, 'at');
%             fseek (fID, 0, 'eof');
            fprintf(fID, '\n%s, %s', sensor.Name, num2str(sensor.EShift));
            fclose(fID);
        end
        
        function overwriteSensorFileWithCurrentSensors(obj, ~)
            fID = fopen(obj.sensorFileName, 'wt');
            for i=2:length(obj.sensors)
                fprintf(fID, '%s, %s', obj.sensors(i).Name,... 
                    num2str(obj.sensors(i).EShift));
                if i<length(obj.sensors)
                    fprintf(fID, '\n');
                end
            end
            fclose(fID);            
        end

        function refreshSensorList(obj, selection)
                obj.readSensorsFromFile();
            
                names = arrayfun(@(f) f.Name, obj.sensors, 'un', 0);
                obj.sensorNamePopup.String = names;
                switch selection
                    case 1
                        % Set is clicked => Select the newly added sensor, last in the list
                        obj.sensorNamePopup.Value = length(names);
                    case 0
                        if obj.sensorNamePopup.Value>length(names);
                            obj.sensorNamePopup.Value = 1;
                        end
                end
                notify(obj, 'SensorPopupChanged'); 
                % update sensor to match popup selection
        end

        
        function keyPressedName(obj, ~, E)
            obj.EShiftEditContentLength = length(obj.sensorEShiftEdit.String);
            
            if strcmp(E.Key, 'backspace')||strcmp(E.Key, 'delete')
                obj.NameEditContentLength = obj.NameEditContentLength - 1;
            else
                if isempty(E.Character)||strcmp(E.Key, 'leftarrow')||strcmp(E.Key, 'rightarrow')...
                    ||strcmp(E.Key, 'downarrow')||strcmp(E.Key, 'uparrow');
                else
                    obj.NameEditContentLength = obj.NameEditContentLength + 1;
                end
            end
            if obj.NameEditContentLength<0; obj.NameEditContentLength=0; end

            obj.updateSetButton();
        end
         
        function keyPressedEShift(obj, ~, E)
            obj.NameEditContentLength = length(obj.sensorNameEdit.String); 
            
            if strcmp(E.Key, 'backspace')||strcmp(E.Key, 'delete')
                obj.EShiftEditContentLength = obj.EShiftEditContentLength - 1;
            else
                if isempty(E.Character)||strcmp(E.Key, 'leftarrow')||strcmp(E.Key, 'rightarrow')...
                    ||strcmp(E.Key, 'downarrow')||strcmp(E.Key, 'uparrow');
                else
                    obj.EShiftEditContentLength = obj.EShiftEditContentLength + 1;
                end
            end
            if obj.EShiftEditContentLength<0; obj.EShiftEditContentLength=0; end

            obj.updateSetButton();
            if obj.EShiftEditContentLength<0; obj.EShiftEditContentLength=0; end
        end

        function updateSetButton(obj, ~, ~, ~)
            if obj.NameEditContentLength==0||obj.EShiftEditContentLength==0 ...
                &&(strcmp(obj.setButton.Enable, 'on'));
                    notify(obj, 'EmptyFields');
                    return;
            end

            if (obj.NameEditContentLength>0&&obj.EShiftEditContentLength>0) ...
                &&(strcmp(obj.setButton.Enable, 'off'));
                    notify(obj, 'NonEmptyFields');
                    return;
            end
        end
        
        function enableSetButton(obj, ~, ~, ~)
                obj.setButton.Enable = 'on';
        end
        
        function disableSetButton(obj, ~, ~, ~)
                obj.setButton.Enable = 'off';
        end

        function parseInputs(obj, varargin)
            % Parses inputs
            
            parser = inputParser;
            parser.FunctionName = 'ColorPanel';
            
            addParameter(parser, 'Parent', get(groot, 'CurrentFigure'))
            addParameter(parser, 'Position', get(groot, 'DefaultUicontrolPosition'))
            addParameter(parser, 'Units', get(groot, 'DefaultUIcontrolUnits'))
            
            parse(parser, varargin{:});
            
            if isempty(parser.Results.Parent)
                parent = gcf; % Use current figure for parent if none specified
            else
                parent = parser.Results.Parent;
            end
            
            obj.Parent = parent;
            obj.Units = parser.Results.Units;
            obj.Position = parser.Results.Position;
        end
    end
    
end

