function axisHandle = plotLabeldLeftRightEdge(t, v)

            lambda = t; 
            Vunit = {'nV', 'uV', 'mV', 'V', 'kV'}; Tunit = {'ns', 'us', 'ms', 's'};
            Vmultiplier = [1e9 1e6 1e3 1 1e-3]; Tmultiplier = [1e9 1e6 1e3 1];
            Vavg = max (abs(v)); Tavg = max (abs(t)); 
            if Vavg<1e-6
                VUnitSwitch = 1;
            elseif Vavg<1e-3
                VUnitSwitch = 2;
                elseif Vavg<1
                VUnitSwitch = 3;
                elseif Vavg<1e3
                VUnitSwitch = 4;
                elseif Vavg>=1e3
                VUnitSwitch = 5;
            else
                VUnitSwitch = 4;
                end

            if Tavg<1e-6
                TUnitSwitch = 1;
            elseif Tavg<1e-3
                TUnitSwitch = 2;
            elseif Tavg<1
                TUnitSwitch = 3;
            elseif Tavg<1e3
                TUnitSwitch = 4;
            else
                TUnitSwitch = 4;
            end


            axisHandle = plot (Tmultiplier(TUnitSwitch).*t, Vmultiplier(VUnitSwitch).*v);
            grid on
            axis tight;
            proxy = Vmultiplier(VUnitSwitch).*v; margin1=0.1; margin2=0.1; 
            ylim([min(proxy)-margin1.*(max(proxy) - min(proxy)) max(proxy)+margin2.*(max(proxy) - min(proxy))]);
            xlabel(['Time [' Tunit{TUnitSwitch} ']'], 'FontSize', 9);
            ylabel(['Voltage [' Vunit{VUnitSwitch} ']'], 'FontSize', 9);
            set (gca, 'FontSize', 7);
            

end
