function [axisHandle, xMult, yMult] = plotLabeldCal(x, y, xUnitSuffix, yUnitSuffix, calObj)

            % General magnitudes setup
            yUnitPrefix = {'p', 'n', 'u', 'm', '', 'k', 'M', 'G', 'T'};
            xUnitPrefix = {'p', 'n', 'u', 'm', ''};
            ymultiplier = [1e-12 1e-9 1e-6 1e-3 1 1e3 1e6 1e9 1e12]; 
            xmultiplier = [1e-12 1e-9 1e-6 1e-3 1];
            
            % Determining which units to use
            xAvg = max (abs(x)); 
            xUnitIndex = find(xAvg<=1e3.*xmultiplier, 1);
            xMult = xmultiplier(xUnitIndex);
            xUnit = strcat(xUnitPrefix(xUnitIndex), xUnitSuffix);

            yAvg = max (abs(y)); 
            yUnitIndex = find(yAvg<=1e3.*ymultiplier, 1);
            yMult = ymultiplier(yUnitIndex); 
            yUnit = strcat(yUnitPrefix(yUnitIndex), yUnitSuffix);
            
            % Plotting 
                        
            %Plot the calibration data
            axisHandle = plot (x./xMult, y./yMult);
            grid on
            axis tight;
            proxy = y./yMult; margin1=0.3; margin2=0.3; 
            ylim([min(proxy)-margin1.*(max(proxy) - min(proxy)) ...
                max(proxy)+margin2.*(max(proxy) - min(proxy))]);
            xlabel(['Time [' xUnit{1} ']'], 'FontSize', 9);
            ylabel(['Voltage [' yUnit{1} ']'], 'FontSize', 9);
            set (gca, 'FontSize', 7);
            
            % Draw Lines
            line ([x(1) x(end)]./xMult, ... 
                (calObj.vOffset+calObj.vPP/2).*[1 1]./yMult,...
                'Color', 'red', 'LineStyle', '--', 'LineWidth', 2);

            line ([x(1) x(end)]./xMult, ... 
                (calObj.vOffset-calObj.vPP/2).*[1 1]./yMult,...
                'Color', 'red', 'LineStyle', '--', 'LineWidth', 2);

            if calObj.ACDCFlag 
                text((x(end)-0.25*(x(end)-x(1)))./xMult, ...
                    (calObj.vOffset + 1.9*calObj.vPP/2)./yMult, ...
                    'AC Coupling', 'FontSize', 10, 'Color', 'black', 'Margin', 1);
            else
                line ([x(1) x(end)]./xMult, ... 
                    calObj.vOffset.*[1 1]./yMult,...
                    'Color', 'black', 'LineStyle', '-', 'LineWidth', 2);
                text((x(end)-0.2*(x(end)-x(1)))./xMult, ...
                    (calObj.vOffset + 1.75*calObj.vPP/2)./yMult, ...
                    'DC Coupling', 'FontSize', 10, 'Color', 'black', 'Margin', 1);
%                 line ([x(1) x(end)]./xMult, ... 
%                     calObj.operatingV.*[1 1]./yMult,...
%                     'Color', 'black', 'LineStyle', '-', 'LineWidth', 2);
%                 text((x(end)-0.25*(x(end)-x(1)))./xMult, ...
%                     (calObj.operatingV + 1.9*calObj.vPP/2)./yMult, ...
%                     'Operating V', 'FontSize', 10, 'Color', 'black', 'Margin', 1);
            end
            title('Calibration Preview', 'FontSize', 9);

            
end
