function fh = TimerGUI(x)
% Demonstrate how to have a running clock in a GUI, and timer use.
% Creates a small little GUI which displays the correct time and is updated
% every second according to the system clock.
% Returns the object S which contains all the relevant elements

x0=x*60;
xSec = mod(x0,60);
xMin = floor(x0/60);

S.fh = figure('units','normalized',...
              'position',[0.5 0.5 0.1 0.05],...
              'menubar','none',...
              'name','Low-noise timeout',...
              'numbertitle','off',...
              'resize','off');
S.tx = uicontrol('style','text',...
                 'unit','pix',...
                 'position',[35 10 130 30],...
                 'string', sprintf('%02i:%02i', xMin, xSec),...
                 'backgroundc',get(S.fh,'color'),...
                 'fontsize',18,...
                 'fontweight','bold',...
                 'foregroundcolor',[.9 .1 .1]);
% x0 = str2double(datestr(now,'HH')*60*60 + datestr(now,'MM')*60 + datestr(now,'SS')); % So we can update every minute.             

tmr = timer('Name','Reminder',...
            'Period',1,...  % Update the time every second.
            'TasksToExecute',inf,...  % number of times to update
            'ExecutionMode','fixedSpacing',...
            'TimerFcn',{@updater}); 
start(tmr);  % Start the timer object.
set(S.fh,'deletefcn',{@deleter})  % Kill timer if fig is closed.
fh=S.fh;

    function [] = updater(varargin)
    % timerfcn for the timer.  If figure is deleted, so is timer.
         % I use a try-catch here because timers are finicky in my
         % experience.
         try
             x0 = x0 - 1;
             if x0>=0
                 xSec = mod(x0,60);
                 xMin = floor(x0/60);
                 set(S.tx,'string', sprintf('%02i:%02i', xMin, xSec))
                 if ~x0
                     X = load('gong');  % At the timeout, sound a gong.
                     sound(X.y,X.Fs*2.5)  
                     [y,Fs] = audioread('StopLN.mp3');
                     sound(y,Fs);
                 end
                 clear X
             else
                 if mod(x0,2)
                     set(S.fh, 'Color', 'red');
                 else
                    set(S.fh, 'Color', 'white');
                 end
             end
         catch
             delete(S.fh) % Close it all down.
         end
    end

    function [] = deleter(varargin)
    % If figure is deleted, so is timer.
         stop(tmr);
         delete(tmr);
    end
end

             
