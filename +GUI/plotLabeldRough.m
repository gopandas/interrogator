function [] = plotLabeldRough(rp)

            if isempty(rp.RoughSpectrum)
                cla; return
            else
                lambda = rp.RoughSpectrum(1, :); 
                v = rp.RoughSpectrum(2, :);
            end
            VUnit = {'nV', 'uV', 'mV', 'V', 'kV'}; LambdaUnit = {'nm', 'nm', 'mm', 'm'};
            VMultiplier = [1e9 1e6 1e3 1 1e-3]; LambdaMultiplier = [1e9 1e9 1e3 1];
            Vavg = max (abs(v)); LambdaAvg = max (abs(lambda)); 
            if Vavg<1e-6
                VUnitSwitch = 1;
            elseif Vavg<1e-3
                VUnitSwitch = 2;
                elseif Vavg<1
                VUnitSwitch = 3;
                elseif Vavg<1e3
                VUnitSwitch = 4;
                elseif Vavg>=1e3
                VUnitSwitch = 5;
            else
                VUnitSwitch = 4;
                end

            if LambdaAvg<1e-6
                LambdaUnitSwitch = 1;
            elseif LambdaAvg<1e-3
                LambdaUnitSwitch = 2;
            elseif LambdaAvg<1
                LambdaUnitSwitch = 3;
            elseif LambdaAvg<1e3
                LambdaUnitSwitch = 4;
            else
                LambdaUnitSwitch = 4;
            end

            plot (LambdaMultiplier(LambdaUnitSwitch).*lambda, VMultiplier(VUnitSwitch).*v);
            grid on; axis tight;
            proxy = VMultiplier(VUnitSwitch).*v; margin1=0.1; margin2=0.1; 
            ylim([min(proxy)-margin1.*(max(proxy) - min(proxy)) max(proxy)+margin2.*(max(proxy) - min(proxy))]);
            xlabel(['Wavelength [' LambdaUnit{LambdaUnitSwitch} ']'], 'FontSize', 9);
            ylabel(['Voltage [' VUnit{VUnitSwitch} ']'], 'FontSize', 9);
