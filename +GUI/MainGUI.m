function MainGUI()

    f = figure('Visible', 'on', 'Units', 'pixels',...
        'MenuBar', 'none', 'IntegerHandle', 'off',...
        'Name', 'Interrogator App', 'Windowstyle', 'normal',... 
        'DockControls', 'off');
    
    WIDTH = 800;
    HEIGHT = 600;
    
    % Set window size and position
    f.Resize = 'off';
    screensize = get(groot, 'Screensize');
    windowSize = [(screensize(3) - WIDTH) / 2, (screensize(4) - HEIGHT) / 2, WIDTH, HEIGHT];
    f.Position = windowSize;
    
    lp = GUI.LaserPanel({OclaroTL5000.LaserFactory, PurePhotonicsPPCL300.LaserFactory, CoBriteDX1.LaserFactory},...
        'Parent', f,...
        'Position', [25 360 300 130]);
    dp = GUI.DAQPanel({NIDaqUSB6002.NIDaqUSBFactory},...
        'Parent', f,...
        'Position', [25 270 300 90]);
    sp = GUI.SensorPanel('Parent', f,...
        'Position', [25 490 300 100]);
    rp = GUI.ResonancePanel('Parent', f,...
        'Position', [25 90 300 180]);
    cp = GUI.CalibrationPanel('Parent', f,...
        'Position', [25 20 300 70]);
    rw = GUI.ResultsWindow('Parent', get(f, 'Parent'),...
        'Position', get(f, 'Position'));
    bb = GUI.BusyBar('Parent', f,...
        'Position', [25 5 100 15]);
    
    h1 = subplot(3, 2, 2);
    set(h1, 'Units', 'pixels',... 
        'Position', [400, 500, 350, 50]);
    title('Rough Spectrum');
    
    h2 = subplot(3, 2, 4);
    set(h2, 'Units', 'pixels',... 
        'Position', [400, 70, 350, 350]);
    title('Fine Spectrum');
    dcm = datacursormode(f);
    datacursormode on;
    set(dcm, 'updatefcn', @updateFunctionFineSpectrum);
    dcm.UIContextMenu = [];
    
    function output_txt = updateFunctionFineSpectrum(obj,event_obj)
        % Display the position of the data cursor
        % obj          Currently not used (empty)
        % event_obj    Handle to event object
        % output_txt   Data cursor text string (string or cell array of strings).
        hFig = ancestor(event_obj.Target,'figure');
        
        pos = get(event_obj,'Position');
        output_txt = {['\lambda: ',num2str(pos(1),7)],...
            ['V: ',num2str(pos(2),4)]};
        obj.TipHandle.Interpreter = 'tex';
        obj.TipHandle.FontSize = 7;
    end
    
    function setDAQ(~, ~)
        oldPointer = startWatchCursor(f);
        rp.DAQ = dp.DAQ;
        lp.DAQ = dp.DAQ;
        endWatchCursor(f, oldPointer);
    end

    function setLaser(~, ~)
        oldPointer = startWatchCursor(f);
        % passes Laser object to resonance panel once it is set in laser
        % panel
        rp.Laser = lp.Laser;
        
        % if fine Spectrum is loaded, refreshes it to show the red line for
        % the laser
        if ~isempty(rp.Spectrum); setSpectrum(); end;
        endWatchCursor(f, oldPointer);
    end

    function setSensor(~, ~)
        oldPointer = startWatchCursor(f);
        cp.Sensor = sp.Sensor;
        rw.Sensor = sp.Sensor;
        rp.Sensor = sp.Sensor;
        endWatchCursor(f, oldPointer);
    end

    function setRoughSpectrum(~, ~)
        oldPointer = startWatchCursor(f);
        axes(h1);
        GUI.plotLabeldRough(rp);
        title('Rough Spectrum');
        endWatchCursor(f, oldPointer);
    end

    function refreshSpectrumPlot()
        oldPointer = startWatchCursor(f);
        axes(h2);
        if ~isempty(rp.Spectrum)
            GUI.plotLabeldNM(rp);
            title(['Fine Spectrum: '... 
                GUI.num2eng1e3(rp.Spectrum.LambdaMin) 'm - '... 
                GUI.num2eng1e3(rp.Spectrum.LambdaMax) 'm']);
        else
            cla;
            title('Fine Spectrum');
        end
        endWatchCursor(f, oldPointer);
    end

    function setSpectrum(~, ~)
        oldPointer = startWatchCursor(f);
        if ~isempty(lp.Laser)&&~isempty(rp.Spectrum)
            rp.Spectrum.OperatingLambda = lp.Laser.Wavelength;
        end
        cp.Spectrum = rp.Spectrum;
        rw.Spectrum = rp.Spectrum;
        refreshSpectrumPlot();
        endWatchCursor(f, oldPointer);
    end

    function setResults(~, ~)
        oldPointer = startWatchCursor(f);
        rw.Results = cp.Results;
        endWatchCursor(f, oldPointer);
    end

    function setCalibration(~, ~)
        oldPointer = startWatchCursor(f);
        rw.Calibration = cp.Calibration;
        endWatchCursor(f, oldPointer);
    end

    function plotResultsWindow(~, ~)
        oldPointer = startWatchCursor(f);
        if cp.plotResultsWindowFlag
            cp.plotResultsWindowFlag = 0;
            rw.plotResultsWindow;
        end
        if rw.plotResultsWindowFlag
            rw.plotResultsWindowFlag = 0;
            rw.plotResultsWindow;
        end
        endWatchCursor(f, oldPointer);
    end

    function oldPointer = startWatchCursor(fHandle)
        oldPointer = get(fHandle, 'Pointer');
        set(fHandle, 'Pointer', 'watch');
    end
       
    function endWatchCursor(fHandle, oldPointer)
        set(fHandle, 'Pointer', oldPointer);
    end

    function setBusyBar(~, ~)
        bb.Busy = lp.Busy || dp.Busy|| sp.Busy|| rp.Busy|| cp.Busy || rw.Busy;
    end

    addlistener(dp, 'DAQ', 'PostSet', @setDAQ);
    addlistener(lp, 'Laser', 'PostSet', @setLaser);
    addlistener(sp, 'Sensor', 'PostSet', @setSensor);
    addlistener(rp, 'RoughSpectrum', 'PostSet', @setRoughSpectrum);
    addlistener(rp, 'Spectrum', 'PostSet', @setSpectrum);
    addlistener(cp, 'Results', 'PostSet', @setResults);
    addlistener(cp, 'Calibration', 'PostSet', @setCalibration);
    addlistener(cp, 'plotResultsWindowFlag', 'PostSet', @plotResultsWindow);
    addlistener(rw, 'plotResultsWindowFlag', 'PostSet', @plotResultsWindow);

% Busy state listeners
    addlistener(lp, 'Busy', 'PostSet', @setBusyBar);
    addlistener(dp, 'Busy', 'PostSet', @setBusyBar);
    addlistener(sp, 'Busy', 'PostSet', @setBusyBar);
    addlistener(rp, 'Busy', 'PostSet', @setBusyBar);
    addlistener(cp, 'Busy', 'PostSet', @setBusyBar);
    addlistener(rw, 'Busy', 'PostSet', @setBusyBar);
    
    % Show the GUI
    f.Visible = 'on';

end
