classdef DAQPanel < handle
    %DAQPANEL Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Transient, AbortSet)
        Units % Units for this panel
        Position % On parent in Units
        Parent % Figure, uitab, or uipanel
    end
    
    properties (Transient, SetAccess = private, SetObservable)
        DAQ % DAQ object selected
        Busy = false;
    end
    
    properties (Transient, Access = private)
        panel
        factories
        namePopup
        daqStatusText
        initButton
        shutdownButton
    end
    
    properties (Transient, Access = private)
        hvoltage % Current voltage handle
        hstate % Current state handle
    end
    
    properties (Transient, Dependent)
        DAQState
    end

    events
        PanelChanged
        DAQChanged
    end
    
    methods
        function obj = DAQPanel(daqFactories, varargin)
            
            obj.DAQ = [];
            obj.factories = daqFactories;
            parseInputs(obj,varargin{:});
            
            obj.buildPanel();
            
            addlistener(obj.panel, 'ObjectBeingDestroyed', @(~,~)obj.delete);
            addlistener(obj, 'PanelChanged', @obj.updatePanel);
            addlistener(obj, 'DAQChanged', @obj.updateDAQ);
            obj.Busy = false;            
        end
        
        function set.Parent(obj, newParent)
            % Validate/Set/Update
            assert(ishghandle(newParent),'New Parent is expected to be a valid handle to a figure, uipanel, or uitab')
            assert(ismember(newParent.Type,{'figure','uipanel','uitab'}),'New Parent is expected to be a valid handle to a figure, uipanel, or uitab')
            obj.Parent = newParent;
            
            % Notify
            notify(obj,'PanelChanged');
        end
        
        function set.DAQ(obj, daq)
            obj.DAQ = daq;
            notify(obj,'DAQChanged');
            notify(obj,'PanelChanged');
        end
        
        function s = get.DAQState(obj)
            if isempty(obj.DAQ)
                s = 'No DAQ selected';
            else
                s = sprintf('%s, Voltage: %sV',...
                    char(obj.DAQ.State),...
                    GUI.num2eng(round(obj.DAQ.Voltage,4)));
            end
        end
    end
    
    methods (Access = private)
        
        function buildPanel(obj)
            % Builds our main panel
            
            obj.panel = uipanel('Parent', obj.Parent,...
                'Title', 'DAQ',...
                'Units', obj.Units,...
                'Position', obj.Position);
            
            names = cellfun(@(f) f.Name, obj.factories, 'un', 0);
            
            obj.namePopup = uicontrol('Parent', obj.panel,...
                'Style', 'popup',...
                'String', names,...
                'Units', 'pixels',...
                'Position', [20, 50, 260, 20]);
            obj.initButton = uicontrol('Parent', obj.panel,...
                'Style', 'pushbutton',...
                'String', 'Initialize',...
                'Units', 'pixels',...
                'Position', [20, 15, 130, 30],...
                'Callback', @obj.onInitClicked);
            obj.shutdownButton = uicontrol('Parent', obj.panel,...
                'Style', 'pushbutton',...
                'String', 'Shutdown',...
                'Units', 'pixels',...
                'Enable', 'off',...
                'Position', [150, 15, 130, 30],...
                'Callback', @obj.onShutdownClicked);
            obj.daqStatusText = uicontrol('Parent', obj.panel,...
                'Style', 'text',...
                'String', sprintf('%s', obj.DAQState),...
                'Enable', 'inactive', ...
                'TooltipString', 'Mouse click to refresh voltage',...
                'Units', 'pixels',...
                'HorizontalAlignment', 'center',...                
                'Position', [20, 0, 260, 15],...
                'ButtonDownFcn', @obj.onStatusTextClicked);
        end
        
        function updatePanel(obj, ~, ~)
            % Called to update the panel
            if isvalid(obj)
                    obj.panel.Position = obj.Position;
                    obj.panel.Units = obj.Units;
                    obj.panel.Parent = obj.Parent;

                    obj.daqStatusText.String = sprintf('DAQ: %s', char(obj.DAQState));

                if isobject(obj.DAQ)
                    obj.namePopup.Enable = 'off';
                    obj.initButton.Enable = 'off';
                    obj.shutdownButton.Enable = 'on';
                else
                    obj.namePopup.Enable = 'on';
                    obj.initButton.Enable = 'on';
                    obj.shutdownButton.Enable = 'off';
                end
            end

            obj.Busy = false;            
        end
        
        function updateDAQ(obj, ~, ~)
            % Performs steps for updating the DAQ
            if ~isempty(obj.hvoltage)
                delete(obj.hvoltage)
            end
            if ~isempty(obj.DAQ)
                obj.hvoltage = addlistener(obj.DAQ, 'Voltage', 'PostSet', @obj.updatePanel);
                obj.hstate = addlistener(obj.DAQ, 'State', 'PostSet', @obj.updatePanel);
            end            
        end
        
        function onInitClicked(obj, ~, ~, ~)
            obj.Busy = true;            
            oldPointer = obj.startWatchCursor();
            idx = obj.namePopup.Value;
            factory = obj.factories{idx};
            try
                daq = factory.Create();
                assert(daq.RequestVoltage == 1, 'Unable to read voltage');
                obj.DAQ = daq;
            catch ME
                msgbox(ME.message, 'Error', 'modal');
                warning(getReport(ME));
            end
            obj.endWatchCursor(oldPointer);
            obj.Busy = false;            
        end
        
        function onShutdownClicked(obj, ~, ~, ~)
            obj.Busy = true;            
            oldPointer = obj.startWatchCursor();
            if ~isempty(obj.DAQ)
                try
                    obj.DAQ.RequestShutdown;
                catch ME
                    msgbox(ME.message, 'Error', 'modal');
                    warning(getReport(message));
                end
                obj.DAQ = [];
            end
            obj.endWatchCursor(oldPointer);
            obj.Busy = false;            
        end

        function onStatusTextClicked(obj, ~, ~, ~)
            obj.Busy = true;            
            oldPointer = obj.startWatchCursor();
            if ~isempty(obj.DAQ)
                try
                    assert(obj.DAQ.RequestVoltage == 1, 'Unable to read voltage');
                catch ME
                    msgbox(ME.message, 'Error', 'modal');
                    warning(getReport(ME));
                end
            end
            obj.endWatchCursor(oldPointer);
            obj.Busy = false;            
        end
        
        function oldPointer = startWatchCursor(obj)
            fHandle = obj.Parent;
            oldPointer = get(fHandle, 'Pointer');
            set(fHandle, 'Pointer', 'watch');
        end

        function endWatchCursor(obj, oldPointer)
            fHandle = obj.Parent;
            set(fHandle, 'Pointer', oldPointer);
        end
        
        function parseInputs(obj, varargin)
            % Parses inputs
            
            parser = inputParser;
            parser.FunctionName = 'ColorPanel';
            
            addParameter(parser, 'Parent', get(groot, 'CurrentFigure'))
            addParameter(parser, 'Position', get(groot, 'DefaultUicontrolPosition'))
            addParameter(parser, 'Units', get(groot, 'DefaultUIcontrolUnits'))
            
            parse(parser, varargin{:});
            
            if isempty(parser.Results.Parent)
                parent = gcf; % Use current figure for parent if none specified
            else
                parent = parser.Results.Parent;
            end
            
            obj.Parent = parent;
            obj.Units = parser.Results.Units;
            obj.Position = parser.Results.Position;
        end
    end
    
end

