classdef BusyBar < handle
    %BusyBar is a text bar at the bottom that displays when the app is busy 
    %and user shouldn't touch anything
    
    properties (Constant)
        busyTextStringBusy = 'Busy'
        busyTextStringReady = 'Ready'
    end
    
    properties (Transient, AbortSet)
        Units % Units for this panel
        Position % On parent in Units
        Parent % Figure, uitab, or uipanel
    end
    
    properties (Transient, SetObservable)
        Busy % Boolean value that indicates when to display Busy text
    end
    
    properties (Access = private)
        busyText
    end
    
    methods
        function obj = BusyBar(varargin)
            parseInputs(obj,varargin{:});

            % Build the resonance panel
            obj.buildPanel();
            
            addlistener(obj.busyText, 'ObjectBeingDestroyed', @(~,~)obj.delete);
            addlistener(obj, 'Busy', 'PostSet', @obj.updatePanel);

        end
    end
    
    methods (Access = private)
        
        function buildPanel(obj)
            obj.busyText = uicontrol('Parent', obj.Parent,...
                'Style', 'text',...
                'String', obj.busyTextStringReady,...
                'HorizontalAlignment', 'left',...
                'Units', 'pixels',...
                'Position', [25, 3, 100, 15]);
        end
        
        
        function updatePanel(obj, ~, ~)
            % Called up update the panel
            if obj.Busy
                obj.busyText.String = obj.busyTextStringBusy;
            else
                obj.busyText.String = obj.busyTextStringReady;
            end                
        end

        function parseInputs(obj, varargin)
            % Parses inputs
            
            parser = inputParser;
            parser.FunctionName = 'ColorPanel';
            
            addParameter(parser, 'Parent', get(groot, 'CurrentFigure'))
            addParameter(parser, 'Position', get(groot, 'DefaultUicontrolPosition'))
            addParameter(parser, 'Units', get(groot, 'DefaultUIcontrolUnits'))
            
            parse(parser, varargin{:});
            
            parent = gcf; 
            
            obj.Parent = parent;
            obj.Units = parser.Results.Units;
            obj.Position = parser.Results.Position;
        end
    end
end