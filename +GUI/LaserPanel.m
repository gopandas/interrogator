classdef LaserPanel < handle
    %LASERPANEL Panel for displaying the laser
    %   UI Control for controlling a laser
    %   Based on http://www.mathworks.com/matlabcentral/answers/uploaded_files/22289/ColorPanel.m
    
    properties (Transient, AbortSet)
        Units % Units for this panel
        Position % On parent in Units
        Parent % Figure, uitab, or uipanel
    end
    
    properties (Transient, SetObservable)
        Laser % Laser object
        DAQ % DAQ object
        Busy = false;
    end
    
    properties (Transient, Access = private)
        panel
        factories
        namePopup
        powerEdit
        wavelengthEdit
        initButton
        shutdownButton
        autoButton
        lnButton
        hwavelength % Current wavelength handle
        hpower % Current power handle
        hstate % Current state handle
    end
    
    properties (Transient, Dependent)
        LaserState
    end
    
    events
        PanelChanged
        LaserChanged
        DaqChanged
    end
    
    methods
        function obj = LaserPanel(laserFactories, varargin)
            
            obj.Laser = [];
            obj.factories = laserFactories;
            parseInputs(obj,varargin{:});
            
            obj.buildPanel();
            
            addlistener(obj.panel, 'ObjectBeingDestroyed', @(~,~)obj.delete);
            addlistener(obj, 'PanelChanged', @obj.updatePanel);
            addlistener(obj, 'LaserChanged', @obj.updateLaser);
            notify(obj,'PanelChanged');

        end
        
        function set.Parent(obj, newParent)
            % Validate/Set/Update
            assert(ishghandle(newParent),'New Parent is expected to be a valid handle to a figure, uipanel, or uitab')
            assert(ismember(newParent.Type,{'figure','uipanel','uitab'}),'New Parent is expected to be a valid handle to a figure, uipanel, or uitab')
            obj.Parent = newParent;
            
            % Notify
            notify(obj,'PanelChanged');
        end
        
        function set.Laser(obj, laser)
            obj.Laser = laser;
            notify(obj,'LaserChanged');
            notify(obj,'PanelChanged');
        end

        function set.DAQ(obj, DAQ)
            obj.DAQ = DAQ;
            notify(obj,'PanelChanged');
        end
        
    end
    
    methods (Access = private)
        
        function buildPanel(obj)
            % Builds our panel
            
            obj.panel = uipanel('Parent', obj.Parent,...
                'Title', 'Laser',...
                'Units', obj.Units,...
                'Position', obj.Position);
            
            names = cellfun(@(f) f.Name, obj.factories, 'un', 0);
            
            obj.namePopup = uicontrol('Parent', obj.panel,...
                'Style', 'popup',...
                'String', names,...
                'Units', 'pixels',...
                'TooltipString', 'Choose a laser',...
                'Position', [20, 95, 260, 20]);
            obj.initButton = uicontrol('Parent', obj.panel,...
                'Style', 'pushbutton',...
                'String', 'Initialize',...
                'Units', 'pixels',...
                'TooltipString', 'Turn on the selected laser',...
                'Position', [20, 60, 130, 30],...
                'Callback', @obj.onInitializeClicked);
            obj.shutdownButton = uicontrol('Parent', obj.panel,...
                'Style', 'pushbutton',...
                'String', 'Shutdown',...
                'Units', 'pixels',...
                'TooltipString', 'Shut down the selected laser',...
                'Position', [150, 60, 130, 30],...
                'Enable', 'off',...
                'Callback', @obj.onShutdownClicked);
            uicontrol('Parent', obj.panel,...
                'Style', 'text',...
                'String', 'Power:',...
                'HorizontalAlignment', 'right',...
                'Units', 'pixels',...
                'Position', [20, 30, 70, 20]);
            obj.powerEdit = uicontrol('Parent', obj.panel,...
                'Style', 'edit',...
                'Enable', 'off',...
                'Units', 'pixels',...
                'HorizontalAlignment', 'left',...
                'Position', [90, 30, 60, 20],...
                'Callback', @obj.onPowerEntered);
            uicontrol('Parent', obj.panel,...
                'Style', 'text',...
                'String', '[mW]',...
                'HorizontalAlignment', 'left',...
                'Units', 'pixels',...
                'Position', [150, 30, 30, 20]);
            
            uicontrol('Parent', obj.panel,...
                'Style', 'text',...
                'String', 'Wavelength:',...
                'HorizontalAlignment', 'right',...
                'Units', 'pixels',...
                'Position', [20, 5, 70, 20]);
            obj.wavelengthEdit = uicontrol('Parent', obj.panel,...
                'Style', 'edit',...
                'Enable', 'off',...
                'Units', 'pixels',...
                'HorizontalAlignment', 'left',...
                'Position', [90, 5, 60, 20],...
                'Callback', @obj.onWavelengthEntered);
            uicontrol('Parent', obj.panel,...
                'Style', 'text',...
                'String', '[nm]',...
                'HorizontalAlignment', 'left',...
                'Units', 'pixels',...
                'Position', [150, 5, 30, 20]);

            obj.autoButton = uicontrol('Parent', obj.panel,...
                'Style', 'pushbutton',...
                'String', 'Auto Power',...
                'Enable', 'off',...
                'Units', 'pixels',...
                'HorizontalAlignment', 'right',...
                'TooltipString', 'Adjust laser power to make resonance top at 1V',...
                'Position', [205, 30, 75, 20],...
                'Callback', @obj.onAutoClicked);
            
            obj.lnButton = uicontrol('Parent', obj.panel,...
                'Style', 'radiobutton',...
                'String', 'Low Noise',...
                'Enable', 'off',...
                'Units', 'pixels',...
                'HorizontalAlignment', 'right',...
                'TooltipString', 'Select to turn on the low-noise mode',...
                'Position', [205, 5, 80, 15],...
                'Callback', @obj.onLNClicked);
        end
        
        function onInitializeClicked(obj, ~, ~, ~)
            obj.Busy = true;            
            oldPointer = obj.startWatchCursor();

            idx = obj.namePopup.Value;
            factory = obj.factories{idx};
            try
                laser = factory.Create();
                if isempty(laser); return; end
                assert(laser.UpdatePower == 1, 'Unable to set initial laser power');
                assert(laser.UpdateWavelength == 1, 'Unable to read wavelength');
                obj.Laser = laser;
            catch ME
                msgbox(ME.message, 'Error', 'modal');
                warning(getReport(ME));
            end
            obj.endWatchCursor(oldPointer);
            obj.Busy = false;            
        end
        
        function onShutdownClicked(obj, ~, ~, ~)
            obj.Busy = true;            
            oldPointer = obj.startWatchCursor();
            if ~isempty(obj.Laser)
                try
                    obj.Laser.RequestShutdown();
                catch ME
                    msgbox(ME.message, 'Error', 'modal');
                    warning(getReport(ME));
                end
                obj.Laser = [];
            end
            obj.endWatchCursor(oldPointer);
            obj.Busy = false;            
        end
        
        function onPowerEntered(obj, ~, ~, ~)
            obj.Busy = true;            
            oldPointer = obj.startWatchCursor();
            if ~isempty(obj.Laser)
                p=1e-3*str2double(obj.powerEdit.String);
                assert(isnumeric(p), 'Must enter a number');
                if ~obj.Laser.RequestPower(p)
                    msgbox('Unable to set laser power', 'Error', 'modal');
                    warning('Unable to set laser power');
                end
            end
            obj.endWatchCursor(oldPointer);
            obj.Busy = false;            
        end

        function onAutoClicked(obj, ~, ~, ~)
            obj.Busy = true;            
            oldPointer = obj.startWatchCursor();
            if ~isempty(obj.Laser)&&~isempty(obj.DAQ) %if laser and DAQ are initialized
                autoWavelength = obj.Laser.Wavelength; % save the current wavelength 
                
                wavelengthSpan = obj.Laser.WavelengthMax - obj.Laser.WavelengthMin;
                wavelengthSelection = obj.Laser.WavelengthMin + wavelengthSpan/2 + [0 3].*1e-9;
                k=0;
                for lambda = wavelengthSelection;
                   assert(obj.Laser.RequestWavelength(lambda), 'Could not set wavelength');
                   k=k+1; [~, voltageSelection(k)] = obj.DAQ.RequestVoltage;
                end
                topIndex = find (voltageSelection==max(voltageSelection));
                newPower = obj.Laser.Power*1/voltageSelection(topIndex);
                if newPower > max(obj.Laser.Powers)
                    newPower = max(obj.Laser.Powers);
                    disp(sprintf('New laser power is %2.3f', newPower));
                end
                if ~obj.Laser.RequestPower(newPower)
                    warning('Unable to set laser power');
                end
                
                assert(obj.Laser.RequestWavelength(autoWavelength), 'Could not set wavelength');
                % set back the previous wavelength 
            end
            obj.endWatchCursor(oldPointer);
            obj.Busy = false;            
        end
        
        function onWavelengthEntered(obj, ~, ~, ~)
            obj.Busy = true;            
            oldPointer = obj.startWatchCursor();
            if ~isempty(obj.Laser)
                w=1e-9*str2double(obj.wavelengthEdit.String);
                if ~isnumeric(w); warndlg('Non-numeric value entered'); end;
                if ~obj.Laser.RequestWavelength(w)
                    msgbox('Unable to set laser wavelength', 'Error', 'modal');
                    warning('Unable to set laser wavelength');
                end
                obj.Laser = obj.Laser;
            end
            obj.endWatchCursor(oldPointer);
            obj.Busy = false;            
        end
        

        function onLNClicked(obj, ~, ~, ~)
            obj.Busy = true;            
            oldPointer = obj.startWatchCursor();
            try
                if obj.lnButton.Value
                    if ~obj.Laser.RequestLNSwitch(1); 
                        obj.lnButton.Value = 0; 
                    end;
                else
                    if ~obj.Laser.RequestLNSwitch(0); 
                        obj.lnButton.Value = 1; 
                    end;
                end
            catch ME
                msgbox(ME.message, 'Error', 'modal');
                warning(getReport(ME));
            end
            obj.endWatchCursor(oldPointer);
            obj.Busy = false;            
        end
        
        function updatePanel(obj, ~, ~)
            % Performs steps for updating the panel
            
            obj.panel.Position = obj.Position;
            obj.panel.Units = obj.Units;
            obj.panel.Parent = obj.Parent;

            
            if isobject(obj.Laser)
                obj.namePopup.Enable = 'off';
                obj.shutdownButton.Enable = 'on';
                obj.initButton.Enable = 'off';
                
                if obj.Laser.State~=interfaces.LaserState.Off
                    if ~strcmp((obj.namePopup.String(obj.namePopup.Value)),'OclaroTL5000')
                        obj.lnButton.Enable = 'on'; 
                    end
                    obj.powerEdit.Enable = 'on';
                    if  isobject(obj.DAQ)
                        obj.autoButton.Enable = 'on';
                    end
                    obj.powerEdit.TooltipString = ...
                        sprintf('Enter value between %2.3f - %2.3f',... 
                        1e3*obj.Laser.Powers(1), 1e3*obj.Laser.Powers(end));
                    obj.powerEdit.String = num2str(1e3*obj.Laser.Power, '%3.3f');
                    obj.wavelengthEdit.Enable = 'on';
                    obj.wavelengthEdit.TooltipString = ...
                        sprintf('Enter value between %4.3f - %4.3f',... 
                        1e9*obj.Laser.WavelengthMin, 1e9*obj.Laser.WavelengthMax);                    
                    obj.wavelengthEdit.String = num2str(1e9*obj.Laser.Wavelength, '%4.3f');
                else
                    obj.powerEdit.Enable = 'off';
                    obj.autoButton.Enable = 'off';
                    obj.wavelengthEdit.Enable = 'off';
                    obj.namePopup.Enable = 'on';
                    obj.shutdownButton.Enable = 'off';
                    obj.lnButton.Enable = 'off';
                    obj.initButton.Enable = 'on';
                    obj.powerEdit.TooltipString = '';
                    obj.wavelengthEdit.TooltipString = '';
                end
                
                if obj.Laser.State==interfaces.LaserState.LN
                    obj.lnButton.Value = 1; 
                else
                    obj.lnButton.Value = 0; 
                end
            end
            obj.Busy = false;                        
        end
        
        function updateLaser(obj, ~, ~)
            % Performs steps for updating the laser
            if ~isempty(obj.hwavelength)
                delete(obj.hwavelength)
            end
            if ~isempty(obj.hpower)
                delete(obj.hpower)
            end
            if ~isempty(obj.Laser)
                obj.hwavelength = addlistener(obj.Laser, 'Wavelength', 'PostSet', @obj.updatePanel);
                obj.hpower = addlistener(obj.Laser, 'Power', 'PostSet', @obj.updatePanel);
                obj.hstate = addlistener(obj.Laser, 'State', 'PostSet', @obj.updatePanel);
            end
        end
        
        function oldPointer = startWatchCursor(obj)
            fHandle = obj.Parent;
            oldPointer = get(fHandle, 'Pointer');
            set(fHandle, 'Pointer', 'watch');
        end

        function endWatchCursor(obj, oldPointer)
            fHandle = obj.Parent;
            set(fHandle, 'Pointer', oldPointer);
        end
        
        function parseInputs(obj, varargin)
            % Parses inputs
            
            parser = inputParser;
            parser.FunctionName = 'ColorPanel';
            
            addParameter(parser, 'Parent', get(groot, 'CurrentFigure'))
            addParameter(parser, 'Position', get(groot, 'DefaultUicontrolPosition'))
            addParameter(parser, 'Units', get(groot, 'DefaultUIcontrolUnits'))
            
            parse(parser, varargin{:});
            
            if isempty(parser.Results.Parent)
                parent = gcf; % Use current figure for parent if none specified
            else
                parent = parser.Results.Parent;
            end
            
            obj.Parent = parent;
            obj.Units = parser.Results.Units;
            obj.Position = parser.Results.Position;
        end
    end
    
end

