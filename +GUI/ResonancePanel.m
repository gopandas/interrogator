classdef ResonancePanel < handle
    %RESONANCEPANEL Panel for displaying resonance and aquisition
    %information
    
    properties (Transient, AbortSet)
        Units % Units for this panel
        Position % On parent in Units
        Parent % Figure, uitab, or uipanel
    end
    
    properties (Transient, SetObservable)
        Laser % Laser object to use
        DAQ % DAQ object to use
        Sensor % Sensor object to use
        Spectrum % ISpectrum object to use for calibration
        Busy = false;
    end
    
    properties (Transient, SetAccess = private, SetObservable)
        RoughSpectrum % Plot data in the format [[lambda, power],...] for main gui
        ScanResolutionUser = 300
        % number in pm that determines the step size of the narrow band
        % scan resolution
        WavelengthStartSelected
        WavelengthEndSelected
        % start and end wavelengths selected in the popup menu
        WavelengthScanDuration = 15
        % Duration of one wavelength tuning in seconds
    end
    
    properties (Access = private, SetObservable)
        panel
        scanButton
        loadRoughButton
        resonancePopup
        resolutionEdit
        estimatedScanDurationText
        resonanceWavelengthsText
        scanFineButton
        loadFineButton
        laserWavelengthEdit
        maxSensitivityWavelengthText
        resonances = []
        setDefaultResonance = 0
    end
    
    events
        PanelChanged
    end
    
    methods
        function obj = ResonancePanel(varargin)
            parseInputs(obj,varargin{:});

            obj.loadWavelengthScanDurationSetting();

            % Build the resonance panel
            obj.buildPanel();
            
            addlistener(obj.panel, 'ObjectBeingDestroyed', @(~,~)obj.delete);
            addlistener(obj, 'PanelChanged', @obj.updatePanel);
            addlistener(obj, 'Laser', 'PostSet', @obj.updatePanel);
            addlistener(obj, 'DAQ', 'PostSet', @obj.updatePanel);
            addlistener(obj, 'Sensor', 'PostSet', @obj.updatePanel);
            notify(obj, 'PanelChanged'); % make sure everything is up to date
        end
    end
    
    methods (Access = private)
        
        function buildPanel(obj)
            % Builds our main panel
            obj.panel = uipanel('Parent', obj.Parent,...
                'Title', 'Resonance',...
                'Units', obj.Units,...
                'Position', obj.Position);


            obj.scanButton = uicontrol('Parent', obj.panel,...
                'Style', 'pushbutton',...
                'String', 'Scan WB Spectrum',...
                'Units', 'pixels',...
                'TooltipString', 'Click to start wideband spectrum scan',...
                'Position', [20, 130, 130, 30],...
                'Callback', @obj.onScanButtonClicked);
            obj.loadRoughButton = uicontrol('Parent', obj.panel,...
                'Style', 'pushbutton',...
                'String', 'Load WB Spectrum',...
                'Enable', 'on',...
                'Units', 'pixels',...
                'TooltipString', 'Click to load previously saved wideband spectrum',...
                'Position', [150, 130, 130, 30],...
                'Callback', @obj.onLoadRoughButtonClicked);
            obj.resonanceWavelengthsText = uicontrol('Parent', obj.panel,...
                'Style', 'text',...
                'String', 'Select Resonance:',...
                'Enable', 'off', ...
                'HorizontalAlignment', 'left',...
                'Units', 'pixels',...
                'Position', [20, 115, 260, 15],...
                'ButtonDownFcn', @obj.onResWavsClicked);
            obj.resonancePopup = uicontrol('Parent', obj.panel,...
                'Style', 'popup',...
                'String', {'No scan performed'},...
                'TooltipString', 'Select a resonance to enable single resonance scan (recommended closest to 1550 nm)',...
                'Units', 'pixels',...
                'Position', [20, 95, 260, 20], ...
                'Callback', @obj.onResonancePopupSelectionChanged);
            
            
            uicontrol('Parent', obj.panel,...
                'Style', 'text',...
                'String', 'Scan resolution [pm]:',...
                'HorizontalAlignment', 'right',...
                'Units', 'pixels',...
                'Position', [20, 70, 210, 20]);
            obj.resolutionEdit = uicontrol('Parent', obj.panel,...
                'Style', 'edit',...
                'Units', 'pixels',...
                'String', num2str(obj.ScanResolutionUser, '%i'),...
                'HorizontalAlignment', 'left',...
                'Position', [235, 70, 45, 20],...
                'Callback', @obj.onResolutionChanged);
            obj.estimatedScanDurationText = uicontrol('Parent', obj.panel,...
                'Style', 'text',...
                'String', 'Estimating scan time...',...
                'HorizontalAlignment', 'right',...
                'Units', 'pixels',...
                'Position', [20, 55, 260, 15]);
            obj.scanFineButton = uicontrol('Parent', obj.panel,...
                'Style', 'pushbutton',...
                'String', 'Scan Single Resonance',...
                'TooltipString', 'Click to start single resonance scan',...
                'Units', 'pixels',...
                'Position', [20, 25, 130, 30],...
                'Callback', @obj.onScanFineButtonClicked);
            obj.loadFineButton = uicontrol('Parent', obj.panel,...
                'Style', 'pushbutton',...
                'String', 'Load Single Resonance',...
                'Enable', 'on',...
                'TooltipString', 'Click to load previously saved single resonance spectrum',...
                'Units', 'pixels',...
                'Position', [150, 25, 130, 30],...
                'Callback', @obj.onLoadFineButtonClicked);
            obj.maxSensitivityWavelengthText = uicontrol('Parent', obj.panel,...
                'Style', 'text',...
                'String', 'Max Sensitivity Wavelength:',...
                'Enable', 'inactive', ...
                'HorizontalAlignment', 'left',...
                'Units', 'pixels',...
                'Position', [20, 5, 260, 15],...
                'ButtonDownFcn', @obj.onMaxSensWavClicked);
        end
        
        
        function updatePanel(obj, ~, ~)
            % Called up update the panel
            obj.panel.Position = obj.Position;
            obj.panel.Units = obj.Units;
            obj.panel.Parent = obj.Parent;
            
            if ~isobject(obj.Laser)&&~isempty(obj.Spectrum)
                obj.Spectrum.OperatingLambda = obj.Spectrum.LambdaMaxSensitivity;
            end

            if isobject(obj.DAQ) && isobject(obj.Laser)
                obj.scanButton.Enable = 'on';
                obj.loadRoughButton.Enable = 'on';
            else
                obj.scanButton.Enable = 'off';
                obj.loadRoughButton.Enable = 'off';
                obj.resonances = [];
                obj.RoughSpectrum = []; % clears rough spectrum when laser is turned off
            end
            
            if ~isempty(obj.resonances)
                obj.resonancePopup.String = cellfun(@(x)... 
                    [GUI.num2eng1e3(x.WavelengthMin) 'm - ' ...
                    GUI.num2eng1e3(x.WavelengthMax) 'm'], ...
                    obj.resonances, 'un', 0);
                obj.resonancePopup.Enable = 'on';
                obj.resonanceWavelengthsText.Enable = 'inactive';
                if obj.setDefaultResonance
                    obj.resonancePopup.Value = obj.setDefaultResonance;
                    obj.setDefaultResonance = 0;
                end
                obj.onResonancePopupSelectionChanged();
                obj.scanFineButton.Enable = 'on';
                obj.resolutionEdit.Enable = 'on';
                obj.resolutionEdit.String = ...
                    num2str(obj.ScanResolutionUser, '%i');
                set(obj.resolutionEdit, 'ToolTip', 'Enter an integer between 1 and 1000');
                obj.estimatedScanDurationText.Visible = 'on';
            else
                obj.resonancePopup.String = {'No scan performed'};
                obj.resonancePopup.Value = 1;
                obj.resonancePopup.Enable = 'off';
                obj.resonanceWavelengthsText.Enable = 'off';
                obj.scanFineButton.Enable = 'off';
                obj.resolutionEdit.Enable = 'off';
                set(obj.resolutionEdit, 'ToolTip', '');
                obj.estimatedScanDurationText.Visible = 'off';
            end
            
            if ~isempty(obj.Spectrum)
                obj.maxSensitivityWavelengthText.String = ...
                    ['Max Sensitivity Wavelength: '... 
                    num2str(obj.Spectrum.LambdaMaxSensitivity* 1e9, '%4.3f') ' nm'];
                    obj.updateSpectrumOperatingPoint();
            else
                obj.maxSensitivityWavelengthText.String = '';
            end
            
            if ~isempty(obj.Spectrum)&&isobject(obj.Laser)
                obj.maxSensitivityWavelengthText.Enable = 'inactive';
            else
                obj.maxSensitivityWavelengthText.Enable = 'off';
            end
            obj.Busy = false;                        
        end
        
        function onScanButtonClicked(obj, ~, ~, ~)
            obj.Busy = true;            
            oldPointer = obj.startWatchCursor();
            if isobject(obj.Laser) && isobject(obj.DAQ)
                algo = algorithms.Algorithm(obj.Laser, obj.DAQ, obj.Sensor);
                obj.resonances = [];
                notify(obj, 'PanelChanged');
                obj.resonances = algo.GetRoughSpectrum();
                if isempty(obj.resonances); return; end;
                obj.RoughSpectrum = cat(1, cell2mat(cellfun(@(x) x.Spectrum, obj.resonances, 'un', 0)));
                obj.setDefaultResonance = obj.findClosestResonanceTo1550;
                notify(obj, 'PanelChanged');
            end
            obj.endWatchCursor(oldPointer);
            obj.Busy = false;            
        end
        
        function onLoadRoughButtonClicked(obj, ~, ~, ~)
            file = GUI.findfile('*.mat');
            if all(file) 
                z = load(file, 'resonances');
                assert(~isempty(z), 'Rough Spectrum file error!');
                obj.resonances = z.resonances;
                obj.RoughSpectrum = cat(1, cell2mat(cellfun(@(x) x.Spectrum, obj.resonances, 'un', 0)));    
                obj.setDefaultResonance = obj.findClosestResonanceTo1550;
                notify(obj, 'PanelChanged');
            end
        end

        function n = findClosestResonanceTo1550(obj)
            for i = 1:length(obj.resonances)
                dLambda(i) = 1e9*obj.resonances{i}.WavelengthMin - 1550;
            end
            n = find((diff(dLambda>=0)==1));
            if isempty(n)
                n=length(dLambda);
            end
        end

        function onScanFineButtonClicked(obj, ~, ~, ~)
            obj.Busy = true;            
            oldPointer = obj.startWatchCursor();
            if ~isempty(obj.resonances)
                obj.Spectrum = [];
                notify(obj, 'PanelChanged');
                resonance = obj.resonances{obj.resonancePopup.Value};
                algo = algorithms.Algorithm(obj.Laser, obj.DAQ, obj.Sensor); % TODO: algo gets to live on this object
                obj.Spectrum = algo.GetFineSpectrum(resonance, obj.ScanResolutionUser); 
                %this triggers plotting of fine spectrum by MainGUI SetSpextrum
                if isempty(obj.Spectrum); return; end;                
                assert (obj.Laser.RequestWavelength(obj.Spectrum.LambdaMaxSensitivity),...
                    'Problem setting wavelength to max sensitivity');
                obj.Spectrum.OperatingLambda = obj.Spectrum.LambdaMaxSensitivity;
                obj.Spectrum = obj.Spectrum; % alternative way to refresh the plot
                notify(obj, 'PanelChanged');
            end
            obj.endWatchCursor(oldPointer);
            obj.Busy = false;            
        end
        
        function onLoadFineButtonClicked(obj, ~, ~, ~)
            file = GUI.findfile('*.mat');
            
            if all(file) 
                fileContents = whos('-file', file); 
                match = false;
                for i=1:length(fileContents)
                    match = match||strcmp (fileContents(i).name, 'spectrum');
                end
                if match
                    x = load(file, 'spectrum');
                else
                    warndlg('No Narrow Band Spectrum Data found in the file');
                    return;
                end
                try
                    obj.Spectrum = x.spectrum; 
                    % Setting Spectrum property triggers the event 
                    % that refreshes fine spectrum plot by MainGUI SetSpectrum
                catch ME
                    if strcmp(ME.identifier, 'MATLAB:nonExistentField');
                        warndlg('No Narrow Band Spectrum Data found in the file');
                        return;
                    end
                end
                obj.updateSpectrumOperatingPoint();                
                notify(obj, 'PanelChanged');
            end
        end

        function onMaxSensWavClicked(obj, ~, ~)
            obj.Busy = true;
            oldPointer = obj.startWatchCursor();
            persistent clickCounter;
            if isempty(clickCounter)
                clickCounter = 1;
                pause(0.3);
                if clickCounter == 1
                    clickCounter = [];
                end
            else
                clickCounter = [];
                newLambdaMaxSensitivity = 1e-9*str2double(inputdlg('Enter New Max Sensitivity Wavelength [nm]:', ...
                    'Input', 1, {sprintf('%4.3f', 1e9*obj.Spectrum.LambdaMaxSensitivity)}));
                obj.Spectrum.LambdaMaxSensitivity = newLambdaMaxSensitivity;
                    obj.Spectrum = obj.Spectrum;
                notify(obj, 'PanelChanged');
            end
            if isobject(obj.Laser)&&~isempty(obj.Spectrum)
                obj.Laser.RequestWavelength(obj.Spectrum.LambdaMaxSensitivity);
                obj.Laser = obj.Laser;
                obj.Spectrum = obj.Spectrum;
            end
            obj.endWatchCursor(oldPointer);
            obj.Busy = false;            
        end        
        
        function onResonancePopupSelectionChanged(obj, ~, ~, ~)
            obj.updateScanDurationString();
            
            notify(obj, 'PanelChanged');
        end
        
        function onResWavsClicked(obj, ~, ~)
            obj.Busy = true;
            oldPointer = obj.startWatchCursor();
            persistent clickCounterRes;
            if isempty(clickCounterRes)
                clickCounterRes = 1;
                pause(0.3);
                if clickCounterRes == 1
                    clickCounterRes = [];
                end
            else
                clickCounterRes = [];
                % double clicked
                prompt = {'Start \lambda:','End \lambda:'};
                dlg_title = 'Edit resonance';
                num_lines = [1 20];
                defaultans = {num2str(1e9*obj.resonances{obj.resonancePopup.Value}.WavelengthMin, '%4.3f'), ...
                    num2str(1e9*obj.resonances{obj.resonancePopup.Value}.WavelengthMax, '%1.3f')};
                options.Interpreter = 'tex';
                answer = inputdlg(prompt,dlg_title,num_lines,defaultans, options);
                if ~isempty(answer)
                    WMin = 1e-9*str2double (answer{1}); WMax = 1e-9*str2double (answer{2});
                    if isnan(WMin)||isnan(WMax)
                        warndlg('Invalid Entry');
                    else
                        if WMin >= WMax
                            warndlg('End must be larger than start'); return;
                        end

                        obj.resonances{obj.resonancePopup.Value}.WavelengthMin = WMin;
                        obj.resonances{obj.resonancePopup.Value}.WavelengthMax = WMax;
    % %                     fine scan with overridden values
    %                     if ~isempty(obj.resonances)
    %                         obj.Spectrum = [];
    %                         notify(obj, 'PanelChanged');
    %                         resonance = obj.resonances{obj.resonancePopup.Value};
    %                         resonance.WavelengthMin = WMin;
    %                         resonance.WavelengthMax = WMax;
    %                         algo = algorithms.Algorithm(obj.Laser, obj.DAQ); % TODO: algo gets to live on this object
    %                         obj.Spectrum = algo.GetFineSpectrum(resonance, obj.ScanResolutionUser); 
    %                         this triggers plotting of fine spectrum by MainGUI SetSpextrum
    %                         if isempty(obj.Spectrum); return; end;                
    %                         assert (obj.Laser.RequestWavelength(obj.Spectrum.LambdaMaxSensitivity),...
    %                             'Problem setting wavelength to max sensitivity');
    %                         obj.Spectrum.OperatingLambda = obj.Spectrum.LambdaMaxSensitivity;
    %                         obj.Spectrum = obj.Spectrum; % alternative way to refresh the plot

    %                     end
                    end
                end
                notify(obj, 'PanelChanged');
            end
            obj.endWatchCursor(oldPointer);
            obj.Busy = false;            
        end        

        
        function onResolutionChanged(obj, ~, ~, ~)
            num = str2double(obj.resolutionEdit.String);
            if (isempty(num) || num<=0 || num>1000)
                obj.resolutionEdit.String = num2str(obj.ScanResolutionUser, '%i');
                warndlg('Resolution value is an integer value in the range 1-1000 pm');
            else
                obj.ScanResolutionUser = ceil(num);
                obj.resolutionEdit.String = num2str(obj.ScanResolutionUser, '%4.0f');
            end
            
            obj.updateScanDurationString();
        end

        function loadWavelengthScanDurationSetting(obj, ~)        
            % Load saved default values for scan duration
            if exist('defaultSettings.mat', 'file')==2
                vars=load('defaultSettings.mat', 'WavelengthScanDuration');
                if isfield(vars, 'WavelengthScanDuration')
                    obj.WavelengthScanDuration = vars.WavelengthScanDuration; 
                end
            end
        end
        
        function updateScanDurationString(obj, ~)
            r = obj.resonances(obj.resonancePopup.Value);
            dataPointsNumberEstimate = ...
                floor(1e9*(r{1}.WavelengthMax - r{1}.WavelengthMin)/(1e-3*obj.ScanResolutionUser)) + 1;
            obj.estimatedScanDurationText.String = sprintf('%.0f data points in ~%.0f minutes',... 
                dataPointsNumberEstimate, ceil(dataPointsNumberEstimate*obj.WavelengthScanDuration/60));
        end
        
        function updateSpectrumOperatingPoint(obj, ~)
            % when new spectrum is loaded or laser has a new wavelength,
            % the operating wavelength and voltage are calculated from the
            % spectrum
            if isempty(obj.Spectrum.LambdaMaxSensitivity)
                [maxR, ~, ~] = algorithms.AnalyzeSpectrum(obj.Spectrum);
                obj.Spectrum.LambdaMaxSensitivity = maxR(1);
            end
            obj.Spectrum.VMaxSensitivity = ...
                algorithms.MapThisLambda(obj.Spectrum.LambdaMaxSensitivity,...
                obj.Spectrum.Spectrum);
            if isobject(obj.Laser)
                obj.Spectrum.OperatingLambda = obj.Laser.Wavelength;
            else
                obj.Spectrum.OperatingLambda = obj.Spectrum.LambdaMaxSensitivity;
            end
            obj.Spectrum.OperatingV = ...
                algorithms.MapThisLambda (obj.Spectrum.OperatingLambda,...
                obj.Spectrum.Spectrum);            
        end
        
        function oldPointer = startWatchCursor(obj)
            fHandle = obj.Parent;
            oldPointer = get(fHandle, 'Pointer');
            set(fHandle, 'Pointer', 'watch');
        end

        function endWatchCursor(obj, oldPointer)
            fHandle = obj.Parent;
            set(fHandle, 'Pointer', oldPointer);
        end
        
        function parseInputs(obj, varargin)
            % Parses inputs
            
            parser = inputParser;
            parser.FunctionName = 'ColorPanel';
            
            addParameter(parser, 'Parent', get(groot, 'CurrentFigure'))
            addParameter(parser, 'Position', get(groot, 'DefaultUicontrolPosition'))
            addParameter(parser, 'Units', get(groot, 'DefaultUIcontrolUnits'))
            addParameter(parser, 'Laser', false)
            
            parse(parser, varargin{:});
            
            if isempty(parser.Results.Parent)
                parent = gcf; % Use current figure for parent if none specified
            else
                parent = parser.Results.Parent;
            end
            
            obj.Parent = parent;
            obj.Units = parser.Results.Units;
            obj.Position = parser.Results.Position;
            obj.Laser = parser.Results.Laser;
        end
    end
end