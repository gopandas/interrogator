classdef CalibrationPanel < handle
    %CALIBRATIONPANEL Panel for handling calibration from a spectrum
    %   Detailed explanation goes here
    
    properties (Transient, AbortSet)
        Units % Units for this panel
        Position % On parent in Units
        Parent % Figure, uitab, or uipanel
    end
    
    properties (Transient, SetObservable)
        Spectrum % ISpectrum object used for calibration
        Sensor % Sensor object to use
        Calibration % Calibration data object 
        Results; % Results data object
        plotResultsWindowFlag = 0; % Flag that signals the time to plot results 
        Busy = false;
    end
    
    properties (Access = private)
        panel
        smallGainButton1
        smallGainButton2
        loadCalibrationButton
        calibrateTraceButton
        calibrationVoltageEdit
        sGain
    end
    
    events
        PanelChanged
    end
    
    methods
        function obj = CalibrationPanel(varargin)
            parseInputs(obj,varargin{:});
            
            obj.sGain = 1;
            
            obj.buildPanel();
            
            addlistener(obj.panel, 'ObjectBeingDestroyed', @(~,~)obj.delete);
            addlistener(obj, 'PanelChanged', @obj.updatePanel);
            addlistener(obj, 'Spectrum', 'PostSet', @obj.updatePanel);
            addlistener(obj, 'Sensor', 'PostSet', @obj.updatePanel);
            addlistener(obj, 'Calibration', 'PostSet', @obj.updatePanel);

            notify(obj, 'PanelChanged'); % make sure everything is up to date
        end
    end
    
    methods (Access = private)
        
        function buildPanel(obj)
            % Builds our main panel
            
            obj.panel = uipanel('Parent', obj.Parent,...
                'Title', 'Calibration',...
                'Units', obj.Units,...
                'Position', obj.Position);
            
            obj.smallGainButton1 = uicontrol('Parent', obj.panel,...
                'Style', 'radiobutton',...
                'String', 'x1',...
                'Enable', 'off',...
                'Value', 1, ...
                'Units', 'pixels',...
                'HorizontalAlignment', 'right',...
                'TooltipString', 'Small signal gain',...
                'Position', [20, 35, 35, 15],...
                'Callback', @obj.onSmallGain1Clicked);
            
            obj.smallGainButton2 = uicontrol('Parent', obj.panel,...
                'Style', 'radiobutton',...
                'String', 'x10',...
                'Enable', 'off',...
                'Units', 'pixels',...
                'HorizontalAlignment', 'right',...
                'TooltipString', 'Small signal gain',...
                'Position', [55, 35, 40, 15],...
                'Callback', @obj.onSmallGain2Clicked);

            uicontrol('Parent', obj.panel,...
                'Style', 'text',...
                'String', 'Peak-to-peak voltage[V]:',...
                'HorizontalAlignment', 'right',...                
                'Units', 'pixels',...
                'TooltipString', ['Enter the peak-to-peak value of the calibration voltage in V. '...
                'Sensor needs to be selected from the Sensor Panel to activate this edit field'],...
                'Position', [110, 35, 120, 15]);
            obj.calibrationVoltageEdit = uicontrol('Parent', obj.panel,...
                'Style', 'edit',...
                'String', '6760',...
                'Units', 'pixels',...
                'HorizontalAlignment', 'left',...                
                'Position', [235, 35, 45, 20],...
                'Callback', @obj.onCalibrationVoltageChange);
            obj.loadCalibrationButton = uicontrol('Parent', obj.panel,...
                'Style', 'pushbutton',...
                'String', 'Load Calibration File',...
                'Units', 'pixels',...
                'Position', [20, 5, 130, 30],...
                'Callback', @obj.onLoadCalibrationClicked);
            obj.calibrateTraceButton = uicontrol('Parent', obj.panel,...
                'Style', 'pushbutton',...
                'String', 'Calibrate Trace File',...
                'Units', 'pixels',...
                'Position', [150, 5, 130, 30],...
                'Callback', @obj.onCalibrateTraceClicked);
        end
        
        function updatePanel(obj, ~, ~)
            % Called up update the panel
            obj.panel.Position = obj.Position;
            obj.panel.Units = obj.Units;
            obj.panel.Parent = obj.Parent;
            
            if ~isempty(obj.Spectrum)&&~(isempty(obj.Sensor))...
                    && obj.Sensor.EShift
                obj.calibrationVoltageEdit.Enable = 'on';
                obj.loadCalibrationButton.Enable = 'on';
                obj.smallGainButton1.Enable = 'on';
                obj.smallGainButton2.Enable = 'on';
                if isobject(obj.Calibration) 
                    obj.Spectrum.Calibration = obj.Calibration;
                    obj.calibrateTraceButton.Enable = 'on';
                end
            else
                obj.calibrationVoltageEdit.Enable = 'off';
                obj.loadCalibrationButton.Enable = 'off';
                obj.calibrateTraceButton.Enable = 'off';
                obj.smallGainButton1.Enable = 'off';
                obj.smallGainButton2.Enable = 'off';
            end
            obj.Busy = false;            
        end
        
        function onCalibrationVoltageChange(obj, ~, ~, ~)
            enteredValue = str2double(obj.calibrationVoltageEdit.String);
            if isnan(enteredValue)
                warndlg('Input must be a number');
                obj.calibrationVoltageEdit.String = '6760';
            end
        end
        
        function onLoadCalibrationClicked(obj, ~, ~, ~)
            oldPointer = obj.startWatchCursor();
            obj.Busy = true;
            set(obj.Parent, 'Pointer', 'watch');
            if ~isempty(obj.Spectrum)
                file = GUI.findfile('*.trc;*.dat;*.txt');
                vPPCalibration = str2double(obj.calibrationVoltageEdit.String);
                if all(file) 
                    %Load the calibration data from the specified file
                    [~, ~, fileExt] = fileparts(file);
                    switch fileExt
                        case '.isf'
                            data = isfread (file); 
                            tCal=data.x; vCal=data.y; clear data;
                        case '.csv'
                            M = csvread(file);
                            tCal = M(:,1); vCal=M(:,2); clear M;
                        case '.trc'
                            M=algorithms.ReadLeCroyBinaryWaveform(file);
                            tCal = M.x; vCal=M.y; clear M;
                    end
                    obj.Calibration = ...
                        obj.Spectrum.Calibrate(tCal, vCal./obj.sGain, obj.sGain, ...
                        vPPCalibration, obj.Sensor);
                end
            end
            obj.endWatchCursor(oldPointer);
            obj.Busy = false;            
        end
        
        function onCalibrateTraceClicked(obj, ~, ~, ~)
            oldPointer = obj.startWatchCursor();
            obj.Busy = true;
            if ~isempty(obj.Spectrum)
                file = GUI.findfile('*.trc;*.dat;*.txt');
                EShift = obj.Sensor.EShift;
                if all(file) 
                    %Load Measurement file
                    switch file(end-3:end)
                        case '.isf'
                            data = isfread (file);
                            t=data.x; v=data.y;
                        case '.csv'
                            M = csvread(file);
                            t = M(:,1); v=M(:,2);
                        case '.trc'
                            M=algorithms.ReadLeCroyBinaryWaveform(file);
                            t = M.x; v=M.y; clear M;
                    end
                    
                    obj.Results = obj.Spectrum.ProcessData(t, v./obj.sGain,...
                        obj.sGain, file, obj.Sensor); 
                    obj.plotResultsWindowFlag = 1;
                end
            end
            obj.endWatchCursor(oldPointer);
            obj.Busy = false;            
        end
        
        function onSmallGain1Clicked (obj, ~, ~)
            if obj.smallGainButton1.Value
                if obj.smallGainButton2.Value
                    obj.smallGainButton2.Value = 0;
                end
                obj.sGain = 1;
            else
                obj.smallGainButton1.Value = 1;
            end
        end
        
        function onSmallGain2Clicked (obj, ~, ~)
            if obj.smallGainButton2.Value
                if obj.smallGainButton1.Value
                    obj.smallGainButton1.Value = 0;
                end
                obj.sGain = 10;
            else
                obj.smallGainButton2.Value = 1;
            end
        end
        
        function oldPointer = startWatchCursor(obj)
            fHandle = obj.Parent;
            oldPointer = get(fHandle, 'Pointer');
            set(fHandle, 'Pointer', 'watch');
        end

        function endWatchCursor(obj, oldPointer)
            fHandle = obj.Parent;
            set(fHandle, 'Pointer', oldPointer);
        end
        
        function parseInputs(obj, varargin)
            % Parses inputs
            
            parser = inputParser;
            parser.FunctionName = 'ColorPanel';
            
            addParameter(parser, 'Parent', get(groot, 'CurrentFigure'))
            addParameter(parser, 'Position', get(groot, 'DefaultUicontrolPosition'))
            addParameter(parser, 'Units', get(groot, 'DefaultUIcontrolUnits'))
            addParameter(parser, 'Spectrum', [])
            
            parse(parser, varargin{:});
            
            if isempty(parser.Results.Parent)
                parent = gcf; % Use current figure for parent if none specified
            else
                parent = parser.Results.Parent;
            end
            
            obj.Parent = parent;
            obj.Units = parser.Results.Units;
            obj.Position = parser.Results.Position;
            obj.Spectrum = parser.Results.Spectrum;
        end
    end
    
end

