function str=num2eng(num);
%NUM2ENG Convert numbers to engineering notation strings.
%   str = num2eng(num) converts the number NUM into a engineering
%   notation string using International System of Units (SI) prefixes.
%
%   Examples:
%        num2eng(23e8) produces the string 
%        '2.3G'
%
%        num2eng(0.0000004) produces the string 
%        '400n'

%   Copyright (c) by Federico Forte
%   Date: 2006/10/20 
if num == 0
    str = '0';
    return
end
suffix_str='yzafpnum kMGTPEZY';
k=1;
while abs(num)>=10^(3*(k-8)) && k<=16,
    k=k+1;
end
if k~=9,
    suff=suffix_str(k);
else
    suff='';
end
str=[num2str(num/10^(3*(k-9))), ' ', suff];
