classdef LaserFactory<interfaces.ILaserFactory
    %   LaserFactory Creates laser objects for PurePhotonicsPPCL300 laser
    %   Detailed explanation goes here
    
    properties (Constant)
        Name = 'OclaroTL5000';
    end
    
    methods 
        function laser = Create(~)
            delete(instrfind); %first clean up before doing anything
            ports = OclaroTL5000.getAvailableComPort();
            [idx, ok] = listdlg('PromptString', 'Select COM Port',...
                'SelectionMode', 'single', 'ListSize', [100 120],...
                'ListString', ports);
            if ~ok
                idx = 1;
                laser = [];
                return;
            end
            laser = OclaroTL5000.Laser(ports(idx));
        end
    end
    
end
