function [flags, CE, Status] = StatusF(obj, clear)

% Nikola Stan stan.nikola@gmail.com
% The StatusF and StatusW commands return the tunable laser status 
% upon a read and provide a way to clear status flags on a write. 
% There are two status registers, one that primarily indicates FATAL 
% conditions (0x20) and the other that primarily indicates WARNING conditions (0x21).

if nargin == 1 
    clear = 0; 
end

commandRegister = '20'; %Register address for the NOP command
data1 = commandRegister;
if clear
    dataBits = 1; WR = 1;
else dataBits = 0; WR = 0;
end
data2 = '00'; % first data byte in hex
data3 = '00'; % second data byte in hex
data23 = dec2hex(uint16(dataBits), 4);
data2 = data23(1:2); % first data byte in hex
data3 = data23(3:4); % second data byte in hex

LstRsp = 0; % Bit set to logic 0 when the checksum is consistent.
% Bit set to logic 1 forces module to resend last valid packet. 
% Used when the checksum is inconsistent.

Bits26_25 = [0 0]; % set to zero by default

CE = 1; Status = 1;
while CE || Status

    data0_b = [[0 0 0 0] LstRsp Bits26_25 WR];
    data0 = binaryVectorToHex(data0_b);

    % Calculating BIP4 checksum
    BIP4 = PurePhotonicsPPCL300.BIP4 (data0, data1, data2, data3);
    data0(1) = binaryVectorToHex(BIP4);

    sendLine = uint8(hex2dec([data0; data1; data2; data3]));
    fwrite(obj.ser_obj,sendLine);


    %% Obtain response from the module
    pause(0.25);
    rxdata_dec = fread(obj.ser_obj,obj.ser_obj.BytesAvailable);
    rxdata_hex = dec2hex(rxdata_dec);
assert(all(size(rxdata_hex)==[4 2]), 'Error! Laser rsponse is the wrong size');

data = [rxdata_hex(3,1:2) rxdata_hex(4,1:2)];
    % convert response to binary 
    rxdata_bin = hexToBinaryVector(dec2hex(rxdata_dec), 8);
    flags = rxdata_bin(3:4, :);
    %% Analyze response

    %% Command specific bits
    % SRQ - Service Request Bit (read �only) (default 0)
    % The SRQ bit is read only. It reflects the state of the module�s SRQ* line. 
    % When the SRQ* line is asserted (low or zero), this bit is set to 1. 
    % The SRQ* line is fully configurable through the SRQ* trigger register 0x28.
    SRQ = rxdata_bin(3, 1);

    % ALM � ALARM Flag bit (read-only) (default 0)
    % The ALM bit is read only. When the ALM* condition is asserted, this bit is set to 1. 
    % The conditions which assert the ALM* condition are fully configurable 
    % through the alarm trigger register (0x2A).
    ALM = rxdata_bin(3, 2);
    
    % FATAL � FATAL alarm bit (read-only) (default 0)
    % The FATAL bit is read only. When the FATAL* condition is asserted, 
    % this bit is set to 1. The conditions which set the FATAL* condition 
    % are fully configurable through the fatal trigger register (0x29).
    FATAL = rxdata_bin(3, 3);

    % DIS � Module�s output is hardware disabled (read-only)
    % The module�s laser output disable bit is read only and represents 
    % the state of the hardware disable pin (DIS*). 
    % When set to one, the module is �hardware� disabled. 
    % When the DIS* pin is set to zero, the SENA bit is also cleared. 
    % Therefore when DIS* is set to one, the module does not re-enable 
    % the output until the SENA is also set. 
    % Any state change in DIS can cause SRQ* to be asserted 
    % if the appropriate SRQ* trigger is set.
    % 1: Module disabled (DIS* line is low) 
    % 0: DIS* line is high
    DIS = rxdata_bin(3, 4);

    % FVSF, WVSF � Vendor Specific Fault (read-only) (default 0)
    % The FVSF bit (0x20) is set to 1 whenever a fatal vendor specific condition 
    % is asserted. The WVSF bit (0x21) is set to 1 whenever 
    % a warning vendor specific condition is asserted. 
    % If either of these bits is set, the vendor will have a register 
    % defined which contains vendor specific fault conditions. 
    % This bit is also asserted when laser aging thresholds are exceeded 
    FVSF = rxdata_bin(3, 5);

    % FFREQ & WFREQ � Frequency Fatal and Warning (read-only) (default 0)
    % The FFREQ bit (0x20) reports that the frequency deviation has exceeded 
    % the frequency fatal threshold (0x24) while WFREQ bit (0x21) reports that 
    % the frequency deviation has exceeded the frequency warning threshold (0x25).
    % When bit 10 is 1, it indicates that the frequency deviation threshold 
    % is being exceeded. When bit 10 is 0, the frequency deviation threshold 
    % is not being exceeded.
    FFREQ = rxdata_bin(3, 6);

    % FTHERM & WTHERM � Thermal Fatal and Warning (read-only) (default 0)
    % The FTHERM bit (0x20) reports that the thermal deviation has exceeded 
    % the thermal fatal threshold (0x26) while WTHERM bit (0x21) reports 
    % that the thermal deviation has exceeded the thermal warning threshold (0x27).
    % When bit 9 is 1, it indicates that the thermal deviation threshold is being exceeded. 
    % When bit 9 is 0, the thermal deviation threshold is not being exceeded.
    FTHERM = rxdata_bin(3, 7);

    % FPWR & WPWR � Power Fatal and Warning (read-only) (default 0)
    % The FPWR bit (0x20) reports that the power deviation has exceeded 
    % the power fatal threshold (0x22) while WPWR bit (0x21) reports that 
    % the power deviation has exceeded the power warning threshold (0x23).
    % When bit 8 is 1, it indicates that the power deviation threshold 
    % is being exceeded. When bit 8 is 0, the power deviation threshold 
    % is not being exceeded.
    FPWR = rxdata_bin(3, 8);

    % XEL � Flags an execution error.
    % A �1� indicates an exceptional condition. Note that execution errors could be
    % generated by a command just given which failed to execute as well as 
    % a command that was currently executing (a pending operation that just complete). 
    % The default RS232 configuration only sets XEL when a pending operation fails. 
    % The XE bit remains set until cleared.
    XEL = rxdata_bin(4, 1);

    % CEL � Flags a communication error.
    % A �1� indicates a communication error. The CE bit remains set until cleared.
    CEL = rxdata_bin(4, 2);
    
    % MRL � Module Restarted (latched) (default 1 � by definition)
    % MRL can be read or set to zero. When it is �1�, it indicates that 
    % the module has been restarted either by power up, by hardware or software reset, 
    % or by a firmware mandated restart. Depending upon the implementation, 
    % this may indicate that the laser�s output signal may be invalid. 
    % Note that the module can be reset through the communication interface 
    % by writing to register 0x32. The bit remains set until cleared.
    MRL = rxdata_bin(4, 3);

    % CRL � Communication Reset (latched) (default 1 � by definition)
    % CRL can be read or set to zero. When it is set, it indicates that 
    % the module has undergone a communication interface reset. 
    % The input buffers were cleared. This can also occur after a manufacturer 
    % specific timeout period has elapsed in the middle of a packet transfer.
    % 38 The bit remains set until cleared.
    CRL = rxdata_bin(4, 4);

    % FVSFL, FFREQL, FTHERML, FPWRL, WVSFL, WFREQL, WTHERML, WPWRL � Latched fatal 
    % and warning indicators (RW) (default 0)
    % These flags are latched versions of bits 11-8 for the fatal and warning 
    % threshold deviations. These bit indicators can be clearfed by writing 
    % a �1� to these bit positions.
    % When any of these bits is 1, it indicates that the corresponding deviation 
    % threshold has been exceeded at sometime in past (since the last clear) 
    % and may still be occurring.
    % When any of these bits are �0�, the corresponding deviation threshold 
    % has not occurred since the last clear.
    FVSFL = rxdata_bin(4, 5);
    FFREQL = rxdata_bin(4, 6);
    FTHERML = rxdata_bin(4, 7);
    FPWRL = rxdata_bin(4, 8);  
    
    %% Command independent bits
    % Communication Error
    CE = rxdata_bin(1, 5);
    if CE 
        fprintf ('\nFCF1 - Communication Error'); 
        LstRsp = 1;  continue;
    end

    % Status Bits
    Status = binaryVectorToDecimal(rxdata_bin(1, 7:8));
    % 0x00 - OK flag, Normal return status
    % 0x01 - XE flag, (execution error)
    % 0x02 - AEA flag, (Automatic extended addressing result being returned or ready to write)
    % 0x03 - CP flag, Command not complete, pending
    if Status == 1
        fprintf('\nFCF1 - Execution Error - retrying...'); continue;
    end
end
