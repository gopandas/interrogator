function fResponse = FTF(obj, f)

% N. Stan
% The FTF register provides fine tune adjustment of the laser�s wavelength
% from the set channel. The adjustment is applied to all channels uniformly.
% Returned value is in MHz, signed short.


if nargin == 1 
    f = 0;
end

fResponse = 0; %Oclaro ITLA doesn't support fine tuning


%
% commandRegister = '62'; %Register address for the command
% data1 = commandRegister;
% %conversion of power into a signed hex, 16 bits long
% data23 = dec2hex(typecast (int16(f), 'uint16'), 4); 
% data2 = data23(1:2); % first data byte in hex
% data3 = data23(3:4); % second data byte in hex
% 
% LstRsp = 0; % Bit set to logic 0 when the checksum is consistent.
% % Bit set to logic 1 forces module to resend last valid packet. 
% % Used when the checksum is inconsistent.
% 
% Bits26_25 = [0 0]; % set to zero by default
% 
% CE = 1; Status = 1;
% while CE || (Status==1)
%     data0_b = [[0 0 0 0] LstRsp Bits26_25 WR];
%     data0 = binaryVectorToHex(data0_b);
% 
%     % Calculating BIP4 checksum
%     BIP4 = PurePhotonicsPPCL300.BIP4 (data0, data1, data2, data3);
%     data0(1) = binaryVectorToHex(BIP4);
% 
%     sendLine = uint8(hex2dec([data0; data1; data2; data3]));
%     fwrite(obj.ser_obj,sendLine);
% 
% 
%     %% Obtain response from the module
%     pause(0.25);
%     rxdata_dec = fread(obj.ser_obj,obj.ser_obj.BytesAvailable);
%     rxdata_hex = dec2hex(rxdata_dec);
% if size(rxdata_hex)~=[4 2]
%     rxdata_hex
% end
%     data = [rxdata_hex(3,1:2) rxdata_hex(4,1:2)];
%     % convert response to binary 
%     rxdata_bin = hexToBinaryVector(dec2hex(rxdata_dec), 8);
% 
%     %% Analyze response
% 
%     %% Command specific bits
% 
%     % Channel number
%     fResponse = double(typecast (uint16(hex2dec(data)), 'int16'));
% 
% 
% 
%     %% Command independent bits
% 
%     % Communication Error
%     CE = rxdata_bin(1, 5);
%     if CE 
%         fprintf ('\nFTF - Communication Error'); 
%         LstRsp = 1; continue;
%     end
% 
%     % Status Bits
%     Status = binaryVectorToDecimal(rxdata_bin(1, 7:8));
%     % 0x00 - OK flag, Normal return status
%     % 0x01 - XE flag, (execution error)
%     % 0x02 - AEA flag, (Automatic extended addressing result being returned or ready to write)
%     % 0x03 - CP flag, Command not complete, pending
% 
%     if Status == 1
%         fprintf('\nFTF - Execution Error - retrying...'); continue;
%     end
% 
% 
%     %% Final wait for the command to execute
%     if WR
%         fprintf('\nSetting FTF ...');
%         PurePhotonicsPPCL300.wait_operation(obj)
%         fprintf('SET!');
%     end
% 
% end
