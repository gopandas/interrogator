classdef Laser <interfaces.ILaser
    %LASER Summary of this class goes here
    %   Detailed explanation goes here

    properties (Access=public)
        ser_obj=false
    end
    
    
    properties (Access=private)
        com_port
    end
    
    properties (Transient, SetAccess = protected)
        timer_handle = false
    end
    
    properties (Constant)
        max_time=600; %max time to wait after tuning [s]
        ptime=5; % pause time used after setting a value for power
        
        %Notes on USB com ports: if there are communication errors, change
        %the com port number to a random number to avoid system conflicts,
        %connect the laser directly to the mother board, don't connect
        %through a USB hub or front panel. Windows com port settings:
        %Baud rate: 112500,
        %data bits read and write to minimum: 64 bits,
        %latency minimum: 1 ms
        Port=[1 1 1]; % do not use wildcards here
        wvError=0.5;
        fError=0.001;
        pwrError=0.5e-3;
    end
    
    methods
        function obj = Laser(port)
            %laser outputs [1527.60488 1567.13256] when queried
            obj.WavelengthMin = 1528e-9;
            obj.WavelengthMax = 1565e-9;
            obj.WavelengthStep = 1e-12; %1 pm
            %in dBm, the laser gives [6 16] dBm when queried
            %FTF limits setting actual: -12.000GHz to 12.000GHz
            obj.FTFlim = 30; % in GHz
            obj.Grid = 30; % in GHz
            obj.Powers = 1e-3.*(ceil(10^(6/10)):1e-1:round(10*10^(16/10))*0.1); %in W
            obj.com_port = port;
        end
    end
    
    methods (Access=protected)
        %these can be called only by this instance and its children(classes
        %that inherit from Laser object which inherits from ILaser)
        function Initialize(obj) %public methods cause other people will call these
            tic;
            fprintf('\nInitializing Oclaro TL5000 ...');
            bdRate = 9600;
            obj.ser_obj=serial(obj.com_port,'BaudRate', bdRate);
            fopen(obj.ser_obj);
            
            % Setting Laser parameters
            % FTFlim
                obj.FTFlim = 0; % in GHz
                fprintf('\nOclaro doesnt support fine tuning: FTF Limit = %3.2f GHz', obj.FTFlim);
            % Grid spacing
                obj.Grid = 1e-1*OclaroTL5000.Grid(obj); % in GHz
                fprintf('\nGrid spacing = %3.2f GHz', obj.Grid); 
            % WavelengthMin
                f1 = OclaroTL5000.LFH1(obj);
                f2 = OclaroTL5000.LFH2(obj);
                f = f1*1e3 + f2/10;
                obj.WavelengthMin = algorithms.FToLambda(1e9*(f-2*obj.FTFlim)); 
                fprintf('\nMin Wavelength = %4.3f nm', 1e9*obj.WavelengthMin);
            % WavelengthMax
                f1 = OclaroTL5000.LFL1(obj);
                f2 = OclaroTL5000.LFL2(obj);
                f = f1*1e3 + f2/10;
                obj.WavelengthMax = algorithms.FToLambda(1e9*(f+2e-3*obj.FTFlim))-7e-9;
                % subtracting from maximum wavelength because Oclaro laser 
                % gives an error for the reported max wv
                fprintf('\nMax Wavelength = %4.3f nm', 1e9*obj.WavelengthMax);
            % Powers
                pLim(1)=OclaroTL5000.OPSL(obj)/100;% in dBm
                pLim(2)=OclaroTL5000.OPSH(obj)/100;% in dBm
                obj.Powers = ...
                    1e-3.*(ceil(10^(double(pLim(1))/10)):1e-1:round(10*10^(double(pLim(2))/10))*0.1); %in W

            % query laser until module is ready to be enabled
            fprintf('\nWaiting for module to become enable-ready ');
            MRDY = false; CE = false;
            while ~MRDY&&~CE 
                [MRDY, ~, CE, ~, ~] = ...
                    OclaroTL5000.NOP(obj);
                pause(0.1);
                fprintf('.');
            end
            fprintf('\nTurning the Laser ON');
            
            % setting the laser to channel one
            OclaroTL5000.Channel(obj, 1);
            
            % turning the laser ON
            OclaroTL5000.ResEna(obj, 1, 1)
            
            fprintf('Laser ON');

%             % Turning laser LowNoise state off in case it was on
%             CoBriteDX1.CBMX_set_port_state(obj.ser_obj,obj.Port,1);
%             CoBriteDX1.wait_tuning(obj.ser_obj,obj.Port,obj.max_time);
%             CoBriteDX1.CBMX_set_port_dither(obj.ser_obj,obj.Port,1);
%             pause(0.5);
%             assert (logical(CoBriteDX1.CBMX_query_port_dither(obj.ser_obj,obj.Port)),...
%                 'Error: Dither on');
%             toc;
        end
        
        function ShutDown(obj)
            %switch the port off
            tic;
            fprintf('\nTurning the Laser OFF');
            OclaroTL5000.ResEna(obj, 1, 0);
            fclose(obj.ser_obj); %close the laser port
            delete(obj.ser_obj); %delete serial object
            toc;
        end
        
        function wvRead = SetWavelength(obj, wavelength)
            fprintf('\nSetting Wavelength ...');
            % calculating frequencies
            wavelengthNM = wavelength * 1e9;
            f = 1e-9*algorithms.LambdaToF(wavelength); %f [GHz]
            fprintf('\nTarget: lambda = %4.3f nm and f = %2.3f GHz',... 
                1e9*wavelength, f);

            f01 = OclaroTL5000.LF1(obj);
            f02 = OclaroTL5000.LF2(obj);
            f0 = f01*1e3 + f02/10;
            deltaf = f-f0;
            deltafGrid = round(deltaf/obj.Grid)*obj.Grid;
            fprintf('\nInitial: lambda = %4.3f nm and f = %2.3f GHz',... 
                algorithms.FToLambda(f0), f0);
            fprintf('\ndelta f = %6.3f GHz, matched to the grid = %6.3f GHz, error df = %6.3fGHz', ...
                deltaf, deltafGrid, deltaf-deltafGrid);
            fGrid = f0 + deltafGrid;
            % setting the frequency
                fprintf('\nTuning wavelength...');
                fGrid2 = 1e4*mod(fGrid*1e-3,1);
                fGrid1 = 1e-4*(fGrid*10 - fGrid2);

                OclaroTL5000.ResEna(obj, 1, 0); %laser OFF
                OclaroTL5000.FCF1(obj, fGrid1);
                OclaroTL5000.FCF2(obj, fGrid2);
                OclaroTL5000.ResEna(obj, 1, 1); %laser ON
                
            
            % Verifying the frequency
            f01 = OclaroTL5000.LF1(obj);
            f02 = OclaroTL5000.LF2(obj);
            f0_post = f01*1e3 + f02/10; % in GHz
            wvRead = algorithms.FToLambda(f0_post);
            fprintf('\nNew: lambda = %4.3f nm and f = %2.3f GHz',... 
                wvRead, f0_post);
            fprintf('\nWavelength error: %3.3f pm', 1e3*abs(wavelengthNM-wvRead));
            while abs(wavelengthNM-wvRead)>obj.wvError
                fprintf('Port %d-%d-%d: Error! Wavelength error exceeded',obj.Port);
                obj.SetWavelength(wavelength);
            end
        end
        
        function wavelength = GetWavelength(obj)
            f01 = OclaroTL5000.LF1(obj);
            f02 = OclaroTL5000.LF2(obj);
            f0 = f01*1e3 + f02/10;
            df0 = OclaroTL5000.FTF(obj);
            wavelength = algorithms.FToLambda(1e9*f0);
            fprintf('\nCurrent f: %2.3f GHz (FTF = %2.3f GHz)', f0, df0);
            fprintf('\nCurrent wavelength is %4.3f nm', 1e9*wavelength);
        end
        
        function SetPower(obj, power)
            fprintf('\nSetting Power ...');
            power_mW = 1e3*power;
            powerDBm = 10*log10(power_mW);
            
            lim = [obj.Powers(1) obj.Powers(end)];
            assert(powerDBm>lim(1)||powerDBm<lim(2), 'Requested Power out of bounds');
            OclaroTL5000.ResEna(obj, 1, 0); %laser software OFF
            OclaroTL5000.PWR(obj, 100*powerDBm);
            OclaroTL5000.ResEna(obj, 1, 1); %laser software ON
            pwrRead=1e-2*OclaroTL5000.PWR(obj);
            fprintf('\nPower setting error = %2.6f mW', abs(10^(powerDBm/10)-10^(pwrRead/10)));
            assert(abs(10^(powerDBm/10)-10^(pwrRead/10))<1e3*obj.pwrError||pwrRead==-100, ...
                'Port %d-%d-%d: Error! Power setting/reading not working',obj.Port);
            fprintf('\nPower set to %2.3f mW ', 10^(pwrRead/10));
        end
        
        function power = GetPower(obj)
            powerResponse = OclaroTL5000.PWR(obj);
            powerdBm = powerResponse/100;
            power = 1e-3*10^(powerdBm/10);
            fprintf('\nCurrent power is %4.3f mW', 1e3*power);
        end

        function ok = SetLowNoise(obj, sw)
            %turn dither off
            if sw==true
                fprintf('\nTurning Low Noise Mode on...');
                x = inputdlg('Timer setup: set Low-noise mode timeout [mins]:', 'Timeout', 1, {'5'});
                if ~isempty(x); 
                    obj.timer_handle = GUI.TimerGUI(str2double(x{:}));
                end
            else
                fprintf('\nTurning Low Noise Mode off...');
            end
            
            OclaroTL5000.CleanMode(obj, 2*sw);
            modeRead = OclaroTL5000.CleanMode(obj);
            assert(modeRead==2*sw,...
                'Dither status setting/reading error');
            ok = true;
            if sw==true
                fprintf('ON');
            else
                fprintf('OFF');
                if isvalid(obj.timer_handle)
                    delete(obj.timer_handle);
                end
            end
        end
    end
    
end
