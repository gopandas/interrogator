function checksum = BIP4 (data0, data1, data2, data3);
% BIP-4 checksum computed over a 32 bit word with the leading 4 bits 
    % pre-pended to the 28 bit packet and set to zero.
    BIP8 = xor(hexToBinaryVector(data3, 8),... 
        hexToBinaryVector(data2, 8));
    BIP8 = xor(BIP8, hexToBinaryVector(data1, 8));
    BIP8 = xor(BIP8, hexToBinaryVector(data0, 8));
    
    checksum = xor(BIP8(1:4), BIP8(5:8));

